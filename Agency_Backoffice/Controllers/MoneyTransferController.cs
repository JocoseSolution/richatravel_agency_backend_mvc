﻿using ModelLibrary;
using System.Web.Mvc;

namespace Agency_Backoffice.Controllers
{
    [RoutePrefix("moneytransfer")]
    public class MoneyTransferController : Controller
    {
        [Route("money-transfer-report")]
        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        public ActionResult MoneyTransferReport()
        {
            MoneyTransfer model = new MoneyTransfer();
            return View(model);
        }
    }
}