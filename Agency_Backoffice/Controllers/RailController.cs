﻿using Agency_Backoffice.Models;
using Agency_Backoffice.Service;
using ClosedXML.Excel;
using ModelLibrary;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Agency_Backoffice.Controllers
{
    public class RailController : Controller
    {
        public ActionResult RailReport(RailModelFilter filter)
        {
            RailModel model = new RailModel();
            try
            {
                LoginSessionDetails lu = new LoginSessionDetails();
                if (AccountService.GetLoginSession(ref lu))
                {                    
                    filter.Loginid = lu.AgentRegisterSession.UID;                   
                }
                model.RailReportList = FlightReportService.GetRailReports(filter);
                model.Totalcount = model.RailReportList.Count;
                TempData["RailFilter"] = filter;
                TempData.Keep();
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return View(model);
        }

        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        public FileResult RailExportToExcel()
        {
            RailModelFilter filter = TempData["RailFilter"] as RailModelFilter;
            TempData.Keep("RailFilter");
            using (XLWorkbook wb = new XLWorkbook())
            {
                wb.Worksheets.Add(FlightReportService.ExportRailTicketReport(filter));
                using (MemoryStream stream = new MemoryStream())
                {
                    wb.SaveAs(stream);
                    return File(stream.ToArray(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "Report_" + DateTime.Now.ToString("dd-MM-yyyy") + ".xlsx");
                }
            }
        }
    }
}