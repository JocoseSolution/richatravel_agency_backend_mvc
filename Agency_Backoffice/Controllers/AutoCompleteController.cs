﻿using Agency_Backoffice.Models;
using Agency_Backoffice.Service;
using System.Linq;
using System.Web.Mvc;

namespace Agency_Backoffice.Controllers
{
    public class AutoCompleteController : Controller
    {
        public JsonResult GetDepArrCity(string param)
        {
            var cityList = AutoCompleteService.CityAutoSearch(param).Take(10).ToList();
            return Json(cityList);
        }

        public JsonResult GetPreferedAirlines(string param)
        {
            var airlinesList = AutoCompleteService.GetPreferedAirlines(param).Take(10).ToList();
            return Json(airlinesList);
        }
        public JsonResult GetAgencyAutoSearch(string param)
        {
            LoginSessionDetails lu = new LoginSessionDetails();
            string Loginid = string.Empty;
            if (AccountService.GetLoginSession(ref lu))
            {
                Loginid = lu.AgentRegisterSession.UID;                
            }
            var cityList = AutoCompleteService.AgencyAutoSearch(param, Loginid).Take(10).ToList();
            return Json(cityList);
        }
    }
}