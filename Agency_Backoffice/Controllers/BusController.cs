﻿using Agency_Backoffice.Models;
using Agency_Backoffice.Models.Common;
using Agency_Backoffice.Service;
using ClosedXML.Excel;
using ModelLibrary;
using System;
using System.Collections.Generic;
using System.IO;
using System.Web.Mvc;

namespace Agency_Backoffice.Controllers
{
    public class BusController : Controller
    {
        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        public ActionResult BusReport(string btnvalue, BusFilter filter)
        {
            BusModel model = new BusModel();
            try
            {
                LoginSessionDetails lu = new LoginSessionDetails();
                if (AccountService.GetLoginSession(ref lu))
                {
                    if (btnvalue == "search")
                    {
                        UpdateModel(model);
                    }
                    filter.FilterLoginId = filter.FilterAgentId = lu.AgentRegisterSession.UID;
                    model = BusService.GetBusReport(filter);

                    TempData["BusReportFilter"] = filter;
                    TempData.Keep();
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return View(model);
        }

        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        public ActionResult BusRefundReport(string btnvalue, BusFilter filter)
        {
            BusRefund model = new BusRefund();
            try
            {
                LoginSessionDetails lu = new LoginSessionDetails();
                if (AccountService.GetLoginSession(ref lu))
                {
                    if (btnvalue == "search")
                    {
                        UpdateModel(filter);
                    }
                    filter.FilterLoginId = filter.FilterAgentId = lu.AgentRegisterSession.UID;
                    model.BusRefundList = BusService.GetBusRefundReport(filter);
                    model.TotalCount = model.BusRefundList.Count;

                    TempData["BusRefundFilter"] = filter;
                    TempData.Keep();
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return View(model);
        }

        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        public ActionResult BusRejectReport(string btnvalue, BusFilter filter)
        {
            BusReject model = new BusReject();
            try
            {
                LoginSessionDetails lu = new LoginSessionDetails();
                if (AccountService.GetLoginSession(ref lu))
                {
                    if (btnvalue == "search")
                    {
                        UpdateModel(filter);
                    }
                    filter.FilterLoginId = filter.FilterAgentId = lu.AgentRegisterSession.UID;
                    model.BusRejectList = BusService.GetBusRejectReport(filter);
                    model.TotalCount = model.BusRejectList.Count;

                    TempData["BusRejectFilter"] = filter;
                    TempData.Keep();
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return View(model);
        }

        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        public ActionResult BusHoldPnrReport(string btnvalue, BusFilter filter)
        {
            BusHoldPnr model = new BusHoldPnr();
            try
            {
                LoginSessionDetails lu = new LoginSessionDetails();
                if (AccountService.GetLoginSession(ref lu))
                {
                    if (btnvalue == "search")
                    {
                        UpdateModel(filter);
                    }
                    filter.FilterLoginId = filter.FilterAgentId = lu.AgentRegisterSession.UID;
                    model.BusHoldPnrList = BusService.GetBusHoldPnrReport(filter);
                    model.TotalCount = model.BusHoldPnrList.Count;
                    model.ExecutiveList = Commoncls.CommonPopulateSelectListItem(BusService.ExecutiveList());

                    TempData["BusHoldPnrFilter"] = filter;
                    TempData.Keep();
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return View(model);
        }
        public JsonResult GetBusSelectedSeatDetails(string orderid, string tinno)
        {
            List<string> result = new List<string>();
            LoginSessionDetails lu = new LoginSessionDetails();
            if (AccountService.GetLoginSession(ref lu))
            {
                result = BusService.GetBusSelectedSeatDetails(orderid, lu.AgentRegisterSession.AgencyId, tinno);
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        #region [Export To Excel]   
        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        public FileResult BusReportExportToExcel()
        {
            BusFilter filter = TempData["BusReportFilter"] as BusFilter;
            TempData.Keep("BusReportFilter");
            using (XLWorkbook wb = new XLWorkbook())
            {
                wb.Worksheets.Add(BusService.ExportBusTicketReport(filter));
                using (MemoryStream stream = new MemoryStream())
                {
                    wb.SaveAs(stream);
                    return File(stream.ToArray(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "Report_" + DateTime.Now.ToString("dd-MM-yyyy") + ".xlsx");
                }
            }
        }

        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        public FileResult BusRefundReportExportToExcel()
        {
            BusFilter filter = TempData["BusRefundFilter"] as BusFilter;
            TempData.Keep("BusRefundFilter");
            using (XLWorkbook wb = new XLWorkbook())
            {
                wb.Worksheets.Add(BusService.BusRefundReportExportToExcel(filter));
                using (MemoryStream stream = new MemoryStream())
                {
                    wb.SaveAs(stream);
                    return File(stream.ToArray(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "Report_" + DateTime.Now.ToString("dd-MM-yyyy") + ".xlsx");
                }
            }
        }

        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        public FileResult BusRejectReportExportToExcel()
        {
            BusFilter filter = TempData["BusRejectFilter"] as BusFilter;
            TempData.Keep("BusRejectFilter");
            using (XLWorkbook wb = new XLWorkbook())
            {
                wb.Worksheets.Add(BusService.BusRejectReportExportToExcel(filter));
                using (MemoryStream stream = new MemoryStream())
                {
                    wb.SaveAs(stream);
                    return File(stream.ToArray(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "Report_" + DateTime.Now.ToString("dd-MM-yyyy") + ".xlsx");
                }
            }
        }

        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        public FileResult BusHoldPnrReportExportToExcel()
        {
            BusFilter filter = TempData["BusHoldPnrFilter"] as BusFilter;
            TempData.Keep("BusHoldPnrFilter");
            using (XLWorkbook wb = new XLWorkbook())
            {
                wb.Worksheets.Add(BusService.BusHoldPnrReportExportToExcel(filter));
                using (MemoryStream stream = new MemoryStream())
                {
                    wb.SaveAs(stream);
                    return File(stream.ToArray(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "Report_" + DateTime.Now.ToString("dd-MM-yyyy") + ".xlsx");
                }
            }
        }
        #endregion
    }
}