﻿using ModelLibrary;
using System.Web.Mvc;

namespace Agency_Backoffice.Controllers
{
    [RoutePrefix("rechange")]
    public class RechargeController : Controller
    {
        [Route("rechange-report")]
        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        public ActionResult RechargeReport()
        {
            Recharge model = new Recharge();
            return View(model);
        }
    }
}