﻿using Agency_Backoffice.Models.Common;
using DataBaseLibrary.BusDB;
using ModelLibrary;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace Agency_Backoffice.Service
{
    internal static class BusService
    {
        #region [Export To Excel]   
        internal static DataTable ExportBusTicketReport(BusFilter filter)
        {
            return BusLibraryHelper.ExportBusTicketReport(filter);
        }
        internal static DataTable BusRefundReportExportToExcel(BusFilter filter)
        {
            return BusLibraryHelper.BusRefundReportExportToExcel(filter);
        }
        internal static DataTable BusRejectReportExportToExcel(BusFilter filter)
        {
            return BusLibraryHelper.BusRejectReportExportToExcel(filter);
        }
        internal static DataTable BusHoldPnrReportExportToExcel(BusFilter filter)
        {
            return BusLibraryHelper.BusHoldPnrReportExportToExcel(filter);
        }
        #endregion
        internal static BusModel GetBusReport(BusFilter filter)
        {
            if (!string.IsNullOrEmpty(filter.FilterFromDate))
            {
                filter.FilterFromDate = Utility.FilterDateFormate(filter.FilterFromDate, "from");
            }
            if (!string.IsNullOrEmpty(filter.FilterToDate))
            {
                filter.FilterToDate = Utility.FilterDateFormate(filter.FilterToDate, "to");
            }
            return BusLibraryHelper.GetBusReport(filter);
        }
        internal static List<BusRefund> GetBusRefundReport(BusFilter filter)
        {
            if (!string.IsNullOrEmpty(filter.FilterFromDate))
            {
                filter.FilterFromDate = Utility.FilterDateFormate(filter.FilterFromDate, "from");
            }
            if (!string.IsNullOrEmpty(filter.FilterToDate))
            {
                filter.FilterToDate = Utility.FilterDateFormate(filter.FilterToDate, "to");
            }
            return BusLibraryHelper.GetBusRefundReport(filter);
        }
        internal static List<BusReject> GetBusRejectReport(BusFilter filter)
        {
            if (!string.IsNullOrEmpty(filter.FilterFromDate))
            {
                filter.FilterFromDate = Utility.FilterDateFormate(filter.FilterFromDate, "from");
            }
            if (!string.IsNullOrEmpty(filter.FilterToDate))
            {
                filter.FilterToDate = Utility.FilterDateFormate(filter.FilterToDate, "to");
            }
            return BusLibraryHelper.GetBusRejectReport(filter);
        }
        internal static List<BusHoldPnr> GetBusHoldPnrReport(BusFilter filter)
        {
            if (!string.IsNullOrEmpty(filter.FilterFromDate))
            {
                filter.FilterFromDate = Utility.FilterDateFormate(filter.FilterFromDate, "from");
            }
            if (!string.IsNullOrEmpty(filter.FilterToDate))
            {
                filter.FilterToDate = Utility.FilterDateFormate(filter.FilterToDate, "to");
            }
            return BusLibraryHelper.GetBusHoldPnrReport(filter);
        }
        public static List<CommonSelectListItem> ExecutiveList()
        {
            return BusLibraryHelper.ExecutiveList();
        }
        internal static List<string> GetBusSelectedSeatDetails(string orderid, string agentid, string tinno = "")
        {
            List<string> result = new List<string>();
            try
            {
                StringBuilder sbResult = new StringBuilder();

                List<BusSelectedSeatDetail> objList = BusLibraryHelper.GetBusSelectedSeatDetails(orderid, agentid, tinno);
                if (objList != null && objList.Count > 0)
                {
                    int loopCount = 1;
                    double totalFare = 0;
                    foreach (var item in objList)
                    {
                        totalFare = +totalFare + Convert.ToDouble(item.TA_NET_FARE);
                    }

                    //BusSingleRoot jsonBus = new BusSingleRoot();
                    //if (!string.IsNullOrEmpty(item.BOOK_RESPONSE) && item.BOOK_RESPONSE.Contains("bookingFee"))
                    //{
                    //    jsonBus = JsonConvert.DeserializeObject<BusSingleRoot>(item.BOOK_RESPONSE);
                    //}

                    //sbResult.Append("<div class='modal-header'>");

                    sbResult.Append("<div class='col-sm-4'><img src='/Content/images/seatseller_logo.jpg' /></div>");
                    sbResult.Append("<div class='col-sm-7' style='text-align:right;'>");
                    sbResult.Append("<ul style='list-style-type: none; text-align: right; line-height: 20px;'>");
                    sbResult.Append("<li><h5 style='margin:0;'>" + objList[0].Agency_Name + "</h5></li>");
                    sbResult.Append("<li>" + objList[0].Address + "</li>");
                    sbResult.Append("<li>" + objList[0].City + "," + objList[0].State + "," + objList[0].Country + "</li>");
                    sbResult.Append("<li>" + objList[0].Phone + "/" + objList[0].Mobile + "</li>");
                    sbResult.Append("</ul></div>");
                    sbResult.Append("<div class='col-sm-1'>");
                    sbResult.Append("<button type='button' class='close' data-dismiss='modal' aria-label='Close' style='margin: -60px -18px 0px 0px; font-size: 40px; color: red;cursor: pointer;'>");
                    sbResult.Append("<span aria-hidden='true'>&times;</span></button></div>");
                    //sbResult.Append("</div>");
                    result.Add(sbResult.ToString());


                    sbResult = new StringBuilder();

                    //sbResult.Append("<div class='modal-body' id='BusBodyContent'>");

                    sbResult.Append("<div class='row form-group'>");
                    sbResult.Append("<div class='col-sm-2' style='border-right: 2px solid #ccc;'>");
                    sbResult.Append("<label>" + objList[0].source + " - " + objList[0].destination + "</label><p>(" + objList[0].journeydate + ")</p></div>");
                    sbResult.Append("<div class='col-sm-2' style='border-right: 2px solid #ccc;'>");
                    sbResult.Append("<label>Bus Operator</label><p>" + objList[0].BUSOPERATOR + "</p></div>");
                    sbResult.Append("<div class='col-sm-2' style='border-right: 2px solid #ccc;'>");
                    sbResult.Append("<label>Order Id</label><p>" + orderid + "</p></div>");
                    sbResult.Append("<div class='col-sm-2' style='border-right: 2px solid #ccc;'>");
                    sbResult.Append("<label>Pnr Number</label><p>" + objList[0].PNR + "</p></div>");
                    sbResult.Append("<div class='col-sm-2' style='border-right: 2px solid #ccc;'>");
                    sbResult.Append("<label>Ticket Number</label><p>" + objList[0].NEWTICKETNO + "</p></div>");
                    sbResult.Append("<div class='col-sm-2'><label>Total Fare</label><p>₹ " + totalFare + "</p></div>");
                    sbResult.Append("</div>");

                    sbResult.Append("<div class='row form-group'>");
                    sbResult.Append("<table class='table table-responsive'>");
                    sbResult.Append("<thead><tr><td>Sr. No.</td><td>Passenger Name</td><td>Seat Number</td><td>Gender</td><td>Contact Number</td><td>Fare</td><td>Status</td></tr></thead>");
                    sbResult.Append("<tbody>");
                    foreach (var item in objList)
                    {
                        sbResult.Append("<tr><td>" + loopCount + ".</td><td>" + item.PAX_TITLE + " " + item.PAXNAME + "</td><td>" + item.SEATNO + "</td><td>" + item.GENDER + "</td><td>" + item.PRIMARY_PAX_PAXMOB + "</td><td>" + item.TA_NET_FARE + "</td><td>" + item.BOOKINGSTATUS + "</td></tr>");
                        loopCount = loopCount + 1;
                    }

                    sbResult.Append("</tbody>");
                    sbResult.Append("</table>");
                    sbResult.Append("</div>");

                    sbResult.Append("<div class='row form-group'></div>");

                    sbResult.Append("<div class='row form-group'>");
                    sbResult.Append("<table class='table table-responsive'>");
                    sbResult.Append("<thead><tr><td>Bus Type</td><td>Reporting&nbsp;Time</td><td>Boarding Point Address</td><td>Landmark</td><td>Address</td></tr></thead>");
                    sbResult.Append("<tbody>");
                    sbResult.Append("<tr>");
                    sbResult.Append("<td><p>" + objList[0].BUSTYPE + "</p></td>");
                    if (!string.IsNullOrEmpty(objList[0].BOARDINGPOINT))
                    {
                        string[] strBordTimeLess = objList[0].BOARDINGPOINT.Split('(');
                        string[] strBordTimeGreat = strBordTimeLess[1].Split(')');

                        sbResult.Append("<td><p>"+ strBordTimeGreat[0] + "</p></td>");
                    }
                    else
                    {
                        sbResult.Append("<td><p>- - -</p></td>");
                    }
                    sbResult.Append("<td><p>" + objList[0].BOARDINGPOINT + "</p></td>");
                    sbResult.Append("<td><p>" + objList[0].BOARDINGPOINT + "</p></td>");
                    sbResult.Append("<td><p>" + objList[0].BOARDINGPOINT + "</p></td>");
                    sbResult.Append("</tr>");
                    sbResult.Append("</tbody>");
                    sbResult.Append("</table></div>");

                    sbResult.Append("<div class='row form-group'><h5><b>Terms &amp; Conditions :</b></h5></div>");

                    sbResult.Append("<div class='col-sm-12'>");
                    sbResult.Append("<p style='margin-bottom: 0.3rem!important;'>1. SeatSeller only a bus ticket agent.It doestn't operate bus services of it's own.</p>");
                    sbResult.Append("<p style='margin-bottom: 0.3rem!important;'>2. In order to provide a comprehensive choice of bus operators departure times and prices to customers.</p>");
                    sbResult.Append("<p style='margin-bottom: 0.3rem!important;'>3. It has tied up with many bus operators.</p>");
                    sbResult.Append("<p style='margin-bottom: 0.3rem!important;'>4. SeatSeller advice to customers is to choice bus operator they are aware of and whose service they are comfortable with.</p>");
                    sbResult.Append("</div>");

                    sbResult.Append("<div class='row form-group'><h5><b>Seat Seller responsible for :</b></h5></div>");

                    sbResult.Append("<div class='row form-group'>");
                    sbResult.Append("<div class='col-sm-8'>");
                    sbResult.Append("<p style='margin-bottom: 0.3rem!important;'>1. Issuing a valid (a ticket that wiil be accepted by the bus operator) for it's network of bus operator.</p>");
                    sbResult.Append("<p style='margin-bottom: 0.3rem!important;'>2. Providing refund & support in the event of cancellation.</p>");
                    sbResult.Append("<p style='margin-bottom: 0.3rem!important;'>3. Providing customer support and information in case of any delays/inconvenence.</p>");
                    sbResult.Append("</div>");
                    sbResult.Append("<div class='col-sm-4'>");
                    sbResult.Append("<h5><b>Cancellation Policy :</b></h5>");
                    if (!string.IsNullOrEmpty(objList[0].CANCEL_POLICY))
                    {
                        string[] splCancel = objList[0].CANCEL_POLICY.Split(';');
                        foreach (var cancel in splCancel)
                        {
                            if (!string.IsNullOrEmpty(cancel) && cancel.Contains(":"))
                            {
                                string[] singleCancel = cancel.Split(':');
                                sbResult.Append("<p style='margin-bottom: 0.3rem!important;'>");
                                sbResult.Append("Between " + singleCancel[0] + " Hrs ");
                                if (singleCancel[1] != "-1")
                                {
                                    sbResult.Append("to " + singleCancel[1] + " Hrs ");
                                }
                                sbResult.Append(singleCancel[2] + "%");
                                sbResult.Append("</p>");
                            }
                        }
                    }
                    sbResult.Append("</div>");
                    sbResult.Append("</div>");

                    sbResult.Append("<div class='row form-group'><h5><b>SeatSeller not responsible for :</b></h5></div>");

                    sbResult.Append("<div class='col-sm-12'>");
                    sbResult.Append("<p style='margin-bottom: 0.3rem!important;'>1. SeatSeller only a bus ticket agent.It doestn't operate bus services of it's own.</p>");
                    sbResult.Append("<p style='margin-bottom: 0.3rem!important;'>2. In order to provide a comprehensive choice of bus operators departure times and prices to customers.</p>");
                    sbResult.Append("<p style='margin-bottom: 0.3rem!important;'>3. It has tied up with many bus operators.</p>");
                    sbResult.Append("<p style='margin-bottom: 0.3rem!important;'>4. SeatSeller advice to customers is to choice bus operator they are aware of and whose service they are comfortable with.</p>");
                    sbResult.Append("</div>");

                    sbResult.Append("</div>");
                    //sbResult.Append("<div class='modal-footer'></div>");
                    //sbResult.Append("</div>");

                    result.Add(sbResult.ToString());
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return result;
        }
    }
}