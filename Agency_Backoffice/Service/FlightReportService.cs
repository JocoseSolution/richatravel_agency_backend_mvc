﻿using Agency_Backoffice.Models.Common;
using DataBaseLibrary.Account;
using DataBaseLibrary.Flight;
using ModelLibrary;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace Agency_Backoffice.Service
{
    public static class FlightReportService
    {
        #region [Export To Excel]
        public static DataTable ExportFlightTicketReport(FlightTicketFilter filter)
        {
            return FlightLibraryHelper.ExportFlightTicketReport(filter);
        }
        public static DataTable ExportRailTicketReport(RailModelFilter filter)
        {
            return FlightLibraryHelper.ExportRailTicketReport(filter);
        }
        public static DataTable ExportFlightRefundReport(FlightTicketFilter filter)
        {
            return FlightLibraryHelper.ExportFlightRefundReport(filter);
        }
        public static DataTable ExportFlightReIssueReport(FlightTicketFilter filter)
        {
            return FlightLibraryHelper.ExportFlightReIssueReport(filter);
        }
        public static DataTable ExportFlightHoldPnrReport(FlightTicketFilter filter)
        {
            return FlightLibraryHelper.ExportFlightHoldPnrReport(filter);
        }
        public static DataTable ExportFlightTicketStatusReport(FlightTicketFilter filter)
        {
            return FlightLibraryHelper.ExportFlightTicketStatusReport(filter);
        }
        public static DataTable OfflineRequestReportExportToExcel(FlightTicketFilter filter)
        {
            return FlightLibraryHelper.OfflineRequestReportExportToExcel(filter);
        }
        public static DataTable TravelExportToExcel(FlightCalender filter)
        {
            return FlightLibraryHelper.TravelExportToExcel(filter);
        }
        #endregion
        public static List<FlightTicketReport> GetFlightTicketReports(FlightTicketFilter filter)
        {
            if (!string.IsNullOrEmpty(filter.FromDate))
            {
                filter.FromDate = FlightDateFormate(filter.FromDate, "from");
            }
            if (!string.IsNullOrEmpty(filter.ToDate))
            {
                filter.ToDate = FlightDateFormate(filter.ToDate, "to");
            }

            return FlightLibraryHelper.GetFlightTicketReports(filter);
        }
        public static List<RailModel> GetRailReports(RailModelFilter filter)
        {
            if (!string.IsNullOrEmpty(filter.FromDate))
            {
                filter.FromDate = FlightDateFormate(filter.FromDate, "from");
            }
            if (!string.IsNullOrEmpty(filter.ToDate))
            {
                filter.ToDate = FlightDateFormate(filter.ToDate, "to");
            }

            return FlightLibraryHelper.GetRailReports(filter);
        }
        public static List<FlightTicketReport> GetFlightRefundReports(FlightTicketFilter filter)
        {
            if (!string.IsNullOrEmpty(filter.FromDate))
            {
                filter.FromDate = Commoncls.FilterDateFormate(filter.FromDate, "from");
            }
            else
            {
                filter.FromDate = Commoncls.FilterDateFormate(Commoncls.GetEmptyDate(), "from");
            }
            if (!string.IsNullOrEmpty(filter.ToDate))
            {
                filter.ToDate = Commoncls.FilterDateFormate(filter.ToDate, "to");
            }

            return FlightLibraryHelper.GetFlightRefundReports(filter);
        }
        public static List<OfflineRequest> GetProxyBookingReport(FlightTicketFilter filter)
        {
            if (!string.IsNullOrEmpty(filter.FromDate))
            {
                filter.FromDate = Commoncls.FilterDateFormate(filter.FromDate, "from");
            }
            if (!string.IsNullOrEmpty(filter.ToDate))
            {
                filter.ToDate = Commoncls.FilterDateFormate(filter.ToDate, "to");
            }
            return FlightLibraryHelper.GetProxyBookingReport(filter);
        }
        public static List<FlightTicketReport> GetHoldPNRReport(FlightTicketFilter filter)
        {
            if (!string.IsNullOrEmpty(filter.FromDate))
            {
                filter.FromDate = Commoncls.FilterDateFormate(filter.FromDate, "from");
            }
            if (!string.IsNullOrEmpty(filter.ToDate))
            {
                filter.ToDate = Commoncls.FilterDateFormate(filter.ToDate, "to");
            }
            return FlightLibraryHelper.GetHoldPNRReport(filter);
        }
        public static List<FlightTicketReport> GetTicketStatusReport(FlightTicketFilter filter)
        {
            if (!string.IsNullOrEmpty(filter.FromDate))
            {
                filter.FromDate = Commoncls.FilterDateFormate(filter.FromDate, "from");
            }
            if (!string.IsNullOrEmpty(filter.ToDate))
            {
                filter.ToDate = Commoncls.FilterDateFormate(filter.ToDate, "to");
            }

            return FlightLibraryHelper.GetTicketStatusReport(filter);
        }
        public static List<FlightTicketReport> GetReIssueDetail(FlightTicketFilter filter)
        {
            if (!string.IsNullOrEmpty(filter.FromDate))
            {
                filter.FromDate = Commoncls.FilterDateFormate(filter.FromDate, "from");
            }
            if (!string.IsNullOrEmpty(filter.ToDate))
            {
                filter.ToDate = Commoncls.FilterDateFormate(filter.ToDate, "to");
            }

            return FlightLibraryHelper.GetReIssueDetail(filter);
        }
        //public static string ConvertStringDateToStringDateFormate(string date)
        //{
        //    DateTime dtDate = new DateTime();

        //    if (!string.IsNullOrEmpty(date))
        //    {
        //        dtDate = DateTime.Parse(date);
        //        return dtDate.ToString("MM/dd/yyyy hh:mm tt");
        //    }

        //    return string.Empty;
        //}
        public static string CheckTktNo(int paxId, string orderId, string gdspnr)
        {
            return FlightLibraryHelper.CheckTktNo(paxId, orderId, gdspnr);
        }
        public static ReIssueReFund GetTicketdIntl(int paxId, string paxType)
        {
            return FlightLibraryHelper.GetTicketdIntl(paxId, paxType);
        }
        public static ReIssueReFund OldPaxInfo(string reissueId, string title, string fName, string mName, string lName, string paxType)
        {
            return FlightLibraryHelper.OldPaxInfo(reissueId, title, fName, mName, lName, paxType);
        }
        public static bool InsertReIssueCancelIntl(ReIssueReFund model)
        {
            return FlightLibraryHelper.InsertReIssueCancelIntl(model);
        }
        public static bool OTPTransactionInsert(string userId, string remark, string otpRefNo, string loginByOTP, string otpId, string serviceType)
        {
            return FlightLibraryHelper.OTPTransactionInsert(userId, remark, otpRefNo, loginByOTP, otpId, serviceType);
        }
        public static bool UpdateTicketTeansCharges(string orderid, string charge, string amount, string AgentId)
        {
            return FlightLibraryHelper.UpdateTicketTeansCharges(orderid, charge, amount, AgentId);
        }
        public static Dictionary<string, string> GetEmail_Credentilas(string orderId, string cmd_Type, string counter)
        {
            return FlightLibraryHelper.GetEmail_Credentilas(orderId, cmd_Type, counter);
        }
        public static bool InsertProxyDetails(OfflineRequest param)
        {
            bool isSuccess = false;
            try
            {
                if (param.TripType == "oneway")
                {
                    param.TripType = "One Way";
                    param.Trip = "O";
                }
                else
                {
                    param.TripType = "Round Trip";
                    param.Trip = "R";
                }

                param.FromCityCode = Commoncls.GetSpilitByCost(param.FromCity);//"DEL"
                param.ToCityCode = Commoncls.GetSpilitByCost(param.DestinationCity);//PAT

                string[] deptDate = param.DepartureDate.Split('/');
                if (deptDate.Length > 0)
                {
                    param.DepartDay = deptDate[0];
                    param.DepartMonth = deptDate[1];
                    param.DepartYear = deptDate[2];
                }

                if (param.FromCountry.Trim().ToLower() == "in" && param.ToCountry.Trim().ToLower() == "in")
                {
                    param.PType = "D";
                }
                else
                {
                    param.PType = "I";
                }

                param.ProjectId = "Nothing";
                param.BookedBy = "Nothing";
                param.Status = "Pending";

                if (FlightLibraryHelper.InsertProxyDetails(param))
                {
                    isSuccess = InsertProxyPaxDetail(param);
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return isSuccess;
        }
        //public static string FlightDateFormate(string date, string type)
        //{
        //    DateTime dtDate = new DateTime();

        //    if (!string.IsNullOrEmpty(date))
        //    {
        //        dtDate = DateTime.Parse(date);
        //        if (type == "from")
        //        {
        //            return dtDate.ToString("dd/MM/yyyy") + " 12:00:00 AM";//10/09/2021 12:00:00 AM
        //        }
        //        else
        //        {
        //            return dtDate.ToString("dd/MM/yyyy") + " 11:59:59 PM";//10/09/2021 11:59:59 PM
        //        }
        //    }

        //    return string.Empty;
        //}
        public static string FlightDateFormate(string date, string type)
        {
            if (!string.IsNullOrEmpty(date))
            {
                string[] splString = date.Split('/');
                if (type == "from")
                {
                    return splString[2] + "-" + splString[1] + "-" + splString[0] + " 12:00:00 AM";
                    //return dtDate.ToString("dd/MM/yyyy") + " 12:00:00 AM";//10/09/2021 12:00:00 AM
                }
                else
                {
                    return splString[2] + "-" + splString[1] + "-" + splString[0] + " 11:59:59 PM";
                    //return dtDate.ToString("dd/MM/yyyy") + " 11:59:59 PM";//10/09/2021 11:59:59 PM
                }
            }

            return string.Empty;
        }
        public static bool InsertProxyPaxDetail(OfflineRequest param)
        {
            bool issuccess = false;
            try
            {
                if (Convert.ToInt32(param.Adult) > 0)
                {
                    foreach (var guest in param.AdultDetails)
                    {
                        OfflineBookFlightDetail model = new OfflineBookFlightDetail();
                        model.AgentID = param.AgentId;
                        model.ProxyId = !string.IsNullOrEmpty(guest.ProxyId) ? guest.ProxyId : "0";
                        model.Title = guest.Title;
                        model.FirstName = guest.FirstName;
                        model.LastName = guest.LastName;
                        model.Age = guest.Age;
                        model.PaxType = guest.PaxType;
                        model.FreqFlyerNO = !string.IsNullOrEmpty(guest.FreqFlyerNO) ? guest.FreqFlyerNO : "";
                        model.PassportNo = !string.IsNullOrEmpty(guest.PassportNo) ? guest.PassportNo : "";
                        model.PPExp = !string.IsNullOrEmpty(guest.PPExp) ? guest.PPExp : "";
                        model.VisaDet = !string.IsNullOrEmpty(guest.VisaDet) ? guest.VisaDet : "";

                        if (FlightLibraryHelper.InsertProxyPaxDetail(model))
                        {
                            issuccess = true;
                        }
                        else
                        {
                            break;
                        }
                    }
                }

                if (Convert.ToInt32(param.Child) > 0)
                {
                    foreach (var guest in param.ChildDetails)
                    {
                        OfflineBookFlightDetail model = new OfflineBookFlightDetail();
                        model.AgentID = param.AgentId;
                        model.ProxyId = !string.IsNullOrEmpty(guest.ProxyId) ? guest.ProxyId : "0";
                        model.Title = guest.Title;
                        model.FirstName = guest.FirstName;
                        model.LastName = guest.LastName;
                        model.Age = guest.Age;
                        model.PaxType = guest.PaxType;
                        model.FreqFlyerNO = !string.IsNullOrEmpty(guest.FreqFlyerNO) ? guest.FreqFlyerNO : "";
                        model.PassportNo = !string.IsNullOrEmpty(guest.PassportNo) ? guest.PassportNo : "";
                        model.PPExp = !string.IsNullOrEmpty(guest.PPExp) ? guest.PPExp : "";
                        model.VisaDet = !string.IsNullOrEmpty(guest.VisaDet) ? guest.VisaDet : "";

                        if (FlightLibraryHelper.InsertProxyPaxDetail(model))
                        {
                            issuccess = true;
                        }
                        else
                        {
                            break;
                        }
                    }
                }
                if (Convert.ToInt32(param.Infant) > 0)
                {
                    foreach (var guest in param.InFantDetails)
                    {
                        OfflineBookFlightDetail model = new OfflineBookFlightDetail();
                        model.AgentID = param.AgentId;
                        model.ProxyId = !string.IsNullOrEmpty(guest.ProxyId) ? guest.ProxyId : "0";
                        model.Title = guest.Title;
                        model.FirstName = guest.FirstName;
                        model.LastName = guest.LastName;
                        model.Age = guest.Age;
                        model.PaxType = guest.PaxType;
                        model.FreqFlyerNO = !string.IsNullOrEmpty(guest.FreqFlyerNO) ? guest.FreqFlyerNO : "";
                        model.PassportNo = !string.IsNullOrEmpty(guest.PassportNo) ? guest.PassportNo : "";
                        model.PPExp = !string.IsNullOrEmpty(guest.PPExp) ? guest.PPExp : "";
                        model.VisaDet = !string.IsNullOrEmpty(guest.VisaDet) ? guest.VisaDet : "";

                        if (FlightLibraryHelper.InsertProxyPaxDetail(model))
                        {
                            issuccess = true;
                        }
                        else
                        {
                            break;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return issuccess;
        }
        public static FlightPassengerDetail GetFlightPassengerDetail(string proxyid)
        {
            return FlightLibraryHelper.GetFlightPassengerDetail(proxyid);
        }
        public static List<PaxDetailModel> SelectPaxDetail(string orderid, string transid)
        {
            return FlightLibraryHelper.SelectPaxDetail(orderid, transid);
        }
        public static List<FlightHeaderModel> SelectHeaderDetail(string orderid)
        {
            return FlightLibraryHelper.SelectHeaderDetail(orderid);
        }
        public static List<FltFareDetailsModel> SelectFareDetail(string orderid, string transid)
        {
            return FlightLibraryHelper.SelectFareDetail(orderid, transid);
        }
        public static List<FltDetailsModel> SelectFlightDetail(string orderid)
        {
            return FlightLibraryHelper.SelectFlightDetail(orderid);
        }
        public static FlightAgencyModel GetAgencyDetailByAgentId(string AgentId)
        {
            return FlightLibraryHelper.GetAgencyDetailByAgentId(AgentId);
        }
        public static string GetAgencyIdByOrderId(string orderid)
        {
            return FlightLibraryHelper.GetAgencyIdByOrderId(orderid);
        }
        public static List<SelectedFlightDetailsGalModel> GetSelectedFltDtls_Gal(string orderid, string agentid)
        {
            return FlightLibraryHelper.GetSelectedFltDtls_Gal(orderid, agentid);
        }
        public static List<BagInfomartionModel> GetBaggageInformation(string Trip, string VC, bool IsBagFare)
        {
            return FlightLibraryHelper.GetBaggageInformation(Trip, VC, IsBagFare);
        }
        public static List<T_Flt_Meal_And_Baggage_RequestModel> Get_MEAL_BAG_FareDetails(string orderid, string transid)
        {
            return FlightLibraryHelper.Get_MEAL_BAG_FareDetails(orderid, transid);
        }
        public static List<FlightTravCalender> GetFlightDetailsByAgentId_New(FlightCalender fltCal)
        {
            return FlightLibraryHelper.GetFlightDetailsByAgentId_New(fltCal);
        }
        public static string GetTicketDetail_Intl_calender(FlightCalender fltCal)
        {
            string result = string.Empty;
            List<FlightTravIntlCalender> fltTravList = FlightLibraryHelper.GetTicketDetail_Intl_calender(fltCal);
            if (fltTravList != null && fltTravList.Count > 0)
            {
                foreach (var item in fltTravList)
                {
                    //result += "<tr><td><a href='" + item.PIdUrl + "' target='_blank' style='color: #169F85;font-weight:bold;'>" + item.PId + "</a></td><td><a href='" + item.OrderIdUrl + "' target='_blank' style='color: #169F85;font-weight:bold;'>" + item.OrderId + "</a></td><td>" + item.Pnr + "</td><td>" + item.PName + "</td> <td>" + item.Airline + "</td><td>" + item.Sector + "</td><td>" + item.DeptTime + "</td><td>" + item.Provider + "</td><td>" + item.Trip + "</td><td>" + item.TripType + "</td></tr>";
                    result += "<tr><td><a href='" + item.OrderIdUrl + "' target='_blank' style='color: #21b2e7;font-weight:bold;'>" + item.OrderId + "</a></td><td>" + item.Pnr + "</td><td>" + item.PName + "</td> <td>" + item.Airline + "</td><td>" + item.Sector + "</td><td>" + item.DeptTime + "</td><td>" + item.Trip + "</td></tr>";
                }
            }
            return result;
        }
        public static SingleBookingDetail GetBookingDetails(string orderid)
        {
            return FlightLibraryHelper.GetBookingDetails(orderid);
        }
        public static FlightPayementDetail GetPayementProcess(string orderid)
        {
            return FlightLibraryHelper.GetPayementProcess(orderid);
        }
        public static List<FlightInvoice> GetInvoiceDetails(string orderid)
        {
            return FlightLibraryHelper.GetInvoiceDetails(orderid);
        }
        public static List<string> GetTicketDetails(string orderid)
        {
            List<string> result = new List<string>();
            try
            {
                if (!string.IsNullOrEmpty(orderid))
                {
                    SingleTicketReportModel model = new SingleTicketReportModel();
                    model.BookingDetail = GetBookingDetails(orderid);
                    model.PaymentDetail = GetPayementProcess(orderid);

                    StringBuilder sbResult = new StringBuilder();
                    sbResult.Append("<h5 class='modal-title'><i class='icofont-airplane icofont-rotate-90'></i>&nbsp;BOOKING INFORMATION : <strong style='color: green;'>" + orderid + "</strong></h5>");
                    sbResult.Append("<div style='margin-left: 50%;'>");
                    if (model.BookingDetail.BookingDetailList != null)
                    {
                        sbResult.Append("<a class='user-menu-link' href='" + model.BookingDetail.BookingDetailList[0].URL + "' target='_blank' style='color: #ef6c00;'><i class='icofont-eye'></i> View Ticket</a>&nbsp;&nbsp;");
                        sbResult.Append("<a class='user-menu-link' href='" + model.BookingDetail.BookingDetailList[0].URL2 + "' target='_blank' style='color: #ef6c00;'><i class='icofont-copy-invert'></i> Invoice </a>");
                    }
                    sbResult.Append("</div>");
                    sbResult.Append("<button type='button' class='close' data-dismiss='modal' aria-label='Close' style='font-size: 40px; cursor:pointer;color: red; margin: -20px -6px 0px 0px !important'>");
                    sbResult.Append("<span aria-hidden='true'>×</span>");
                    sbResult.Append("</button>");
                    result.Add(sbResult.ToString());

                    sbResult = new StringBuilder();
                    sbResult.Append("<section class='panel'>");
                    sbResult.Append("<div class='panel-body'>");
                    sbResult.Append("<div class='row'>");

                    sbResult.Append("<div class='col-md-4'><p> Booking ID: <strong>" + (model.BookingDetail.BookingDetailList != null ? model.BookingDetail.BookingDetailList[0].OrderID : "- - -") + "</strong></p></div>");
                    sbResult.Append("<div class='col-md-4'><p>Amount: <strong>₹ " + (model.BookingDetail.BookingDetailList != null ? model.BookingDetail.BookingDetailList[0].amount : "- - -") + "</strong></p></div>");
                    if (model.PaymentDetail.PaymentList != null)
                    {
                        if (model.PaymentDetail.PaymentList[0].Status == "Ticketed")
                        {
                            sbResult.Append("<div class='col-md-4'><p>Status :<span style='color: #e8f1e8;background: green;border-radius: 4px;font-size: 11px;padding:3px;'>" + model.PaymentDetail.PaymentList[0].Status + "</span></p></div>");
                        }
                        else
                        {
                            sbResult.Append("<div class='col-md-4'><p>Status :<span style='color: #e8f1e8;background: red;border-radius: 7px;font-size: 18px;'>" + model.PaymentDetail.PaymentList[0].Status + "</span></p></div>");
                        }
                    }

                    sbResult.Append("</div>");

                    sbResult.Append("<div class='row'>");
                    sbResult.Append("<div class='col-md-4'><p>Airline PNR : <strong>" + model.BookingDetail.BookingDetailList[0].Airlinepnr + "</strong></p></div>");
                    sbResult.Append("<div class='col-md-4'><p> Agency Name : <strong>" + (model.BookingDetail.BookingDetailList != null ? model.BookingDetail.BookingDetailList[0].Agent : "- - -") + "</strong></p></div>");
                    sbResult.Append("<div class='col-md-4'><label>Flow Type : <strong>Online</strong></label></div>");
                    sbResult.Append("</div>");

                    sbResult.Append("<div class='row'>");
                    sbResult.Append("<div class='col-md-4'><p>Booking Date : <strong>" + (model.BookingDetail.BookingDetailList != null ? model.BookingDetail.BookingDetailList[0].CreateDate : "- - -") + "</strong></p></div>");
                    sbResult.Append("<div class='col-md-4'><p> Agency email : <strong>" + (model.BookingDetail.BookingDetailList != null ? model.BookingDetail.BookingDetailList[0].email : "- - -") + "</strong></p></div>");
                    sbResult.Append("<div class='col-md-4'><p> Agency Mob. : <strong>" + (model.BookingDetail.BookingDetailList != null ? model.BookingDetail.BookingDetailList[0].mob : "- - -") + "</strong></p></div>");
                    sbResult.Append("</div>");
                    sbResult.Append("</div>");
                    sbResult.Append("</section>");

                    sbResult.Append("<section class='panel'>");
                    sbResult.Append("<div class='panel-body'>");
                    sbResult.Append("<header class='panel-heading'><i class='icofont-airplane-alt'></i>&nbsp;Booking Details</header>");
                    sbResult.Append("<div class='panel' style='margin: 15px auto;'>");
                    sbResult.Append("<div class='borderAll posRel whiteBg brRadius5 fl width100 fl padTB10' style='margin-bottom:10px;'>");
                    sbResult.Append("<div class='bkHalfCircleTop mobdn bkHalfCirctop posAbs'></div>");

                    sbResult.Append("<div class='col-md-3'>");
                    sbResult.Append("<div class='row'>");
                    sbResult.Append("<div class='col-md-5'><span class='db padB5'><img alt='Logo Not Found' style='width: 35px; margin-left: 20px;' src='" + (model.BookingDetail.BookingDetailList != null ? model.BookingDetail.BookingDetailList[0].VCLogoUrl : "- - -") + "'></span></div>");
                    sbResult.Append("<div class='col-md-7'>");
                    sbResult.Append("<span class='db greyLt ico12'>" + (model.BookingDetail.BookingDetailList != null ? model.BookingDetail.BookingDetailList[0].airlinename : "- - -") + "</span><span class='db greyLter padT2 ico11'>" + (model.BookingDetail.BookingDetailList != null ? model.BookingDetail.BookingDetailList[0].fltno : "- - -") + "</span>");
                    sbResult.Append("</div>");
                    sbResult.Append("</div>");
                    sbResult.Append("</div>");

                    sbResult.Append("<div class='col-md-3 col-sm-3 col-xs-4 padL20'>");
                    sbResult.Append("<span class='ico20 db padB10'>" + (model.BookingDetail.BookingDetailList != null ? model.BookingDetail.BookingDetailList[0].depairportname + "(" + model.BookingDetail.BookingDetailList[0].depairportcode + ")" : "- - -") + "</span>");
                    sbResult.Append("<span class='ico13 db greyLter lh1-2 mobdn'>" + (model.BookingDetail.BookingDetailList != null ? model.BookingDetail.BookingDetailList[0].DepDateTime : "- - -") + "</span>");
                    sbResult.Append("</div>");

                    sbResult.Append("<div class='col-md-3 col-sm-4 col-xs-4 mobdn txtCenter padLR20 padT20' style='text-align:center;'>");
                    sbResult.Append("<span class='db ico20 backgroundLn'>");
                    sbResult.Append("<i class='oval-2 fl'></i><i class='icofont-airplane icofont-2x icofont-rotate-90 fr ico22 blue icon-flight_line1'></i>");
                    sbResult.Append("</span></div>");

                    sbResult.Append("<div class='col-md-3 col-sm-3 col-xs-4 padL20'>");
                    sbResult.Append("<span class='ico20 db padT5 padB10'>" + (model.BookingDetail.BookingDetailList != null ? model.BookingDetail.BookingDetailList[0].arrivalairportname + "(" + model.BookingDetail.BookingDetailList[0].arrairportcode + ")" : "- - -") + "</span>");
                    sbResult.Append("<span class='ico13 db greyLter lh1-2 mobdn'>" + (model.BookingDetail.BookingDetailList != null ? model.BookingDetail.BookingDetailList[0].ArrDateTime : "- - -") + "</span>");
                    sbResult.Append("</div>");

                    sbResult.Append("<div class='bkHalfCircle bkHalfCircbot mobdn posAbs'></div>");
                    sbResult.Append("</div>");

                    sbResult.Append("<div class='tabl-responsive' style='margin-top: 80px;'>");
                    sbResult.Append("<table class='table table-collapse-phone' cellspacing='0' width='100%' showheaderwhenempty='true' style='width: 100%; max-width: 100% !important; border: 1px solid #ccc; border: 1px solid #b7b7b7; border-radius: 5px; '>");
                    sbResult.Append("<thead><tr><th>Sr No.</th><th>Passenger Name</th><th>Type</th><th>Ticket Number</th><th>Ticket Status</th><th>Base Fare</th></tr></thead>");
                    sbResult.Append("<tbody>");
                    if (model.BookingDetail.BookingDetailList != null)
                    {
                        var i = 1;
                        foreach (var item in model.BookingDetail.BookingDetailList)
                        {
                            sbResult.Append("<tr><td>" + i + ".</td><td>" + item.Title + " " + item.FName + " " + item.LName + "</td><td>" + item.PaxType + "</td><td>" + item.TicketNumber + "</td><td>" + item.Status + "</td><td>₹ " + item.BaseFare + "</td></tr>"); i++;
                        }
                    }
                    sbResult.Append("</tbody>");
                    sbResult.Append("</table>");
                    sbResult.Append("</div>");

                    sbResult.Append("<div class='' style='border: 1px solid #000; border-radius: 5px; padding: 10px;'>");
                    sbResult.Append("<div class='row' style='padding-left: 22px'>");
                    sbResult.Append("<div class='col-md-3'><p>Total Tax : <strong>₹ " + (model.BookingDetail.BookingDetailList != null ? model.BookingDetail.BookingDetailList[0].totaltax : "- - -") + "</strong></p></div>");
                    sbResult.Append("<div class='col-md-3'><p>Total Discount : <strong>₹ " + (model.BookingDetail.BookingDetailList != null ? model.BookingDetail.BookingDetailList[0].TotlaDiscount : "- - -") + "</strong></p></div>");
                    sbResult.Append("<div class='col-md-3'><p>Total netfare : <strong>₹ " + (model.BookingDetail.BookingDetailList != null ? model.BookingDetail.BookingDetailList[0].TotalbaseFare : "- - -") + "</strong></p></div>");
                    sbResult.Append("<div class='col-md-3'><p>Total Gross Fare : <strong>₹ " + (model.BookingDetail.BookingDetailList != null ? model.BookingDetail.BookingDetailList[0].amount : "- - -") + "</strong></p></div>");
                    sbResult.Append("</div>");

                    sbResult.Append("<div class='row' style='padding-left: 22px'>");
                    sbResult.Append("<div class='col-md-3'><p>Airlinepnr : <strong>" + (model.BookingDetail.BookingDetailList != null ? model.BookingDetail.BookingDetailList[0].Airlinepnr : "- - -") + "</strong></p></div>");
                    sbResult.Append("<div class='col-md-3'><p>bookingclass : <strong>" + (model.BookingDetail.BookingDetailList != null ? model.BookingDetail.BookingDetailList[0].bookingclass : "- - -") + "</strong></p></div>");
                    sbResult.Append("<div class='col-md-4'><p>Refundable : <strong>" + (model.BookingDetail.BookingDetailList != null ? model.BookingDetail.BookingDetailList[0].refundable : "- - -") + "</strong></p></div>");
                    sbResult.Append("</div>");
                    sbResult.Append("</div>");

                    sbResult.Append("<div style='clear:both;'></div>");
                    sbResult.Append("</div>");
                    sbResult.Append("</div>");
                    sbResult.Append("</section>");

                    sbResult.Append("<section class='panel'>");
                    sbResult.Append("<div class='row'>");
                    sbResult.Append("<div class='col-sm-12'>");
                    sbResult.Append("<section class='panel'>");
                    sbResult.Append("<header class='panel-heading'><i class='icofont-pay'></i>&nbsp;Payment Process</header>");
                    sbResult.Append("<div class='panel-body table-responsive' style='margin: 15px auto;'>");
                    sbResult.Append("<table class='table table-collapse-phone' cellspacing='0' width='100%' showheaderwhenempty='true' style='width: 100%; max-width: 100% !important; border: 1px solid #ccc; border: 1px solid #b7b7b7; border-radius: 5px; '>");
                    sbResult.Append("<thead><tr><th>Sr No.</th><th>Date</th><th>Booking Id</th><th>Flight Type</th><th style='text-align:right;'>Credit</th><th style='text-align:right;'>Debit</th><th style='text-align:right;'>Balance</th><th>Status</th></tr></thead>");
                    sbResult.Append("<tbody>");
                    if (model.PaymentDetail.PaymentList != null)
                    {
                        var i = 1;
                        foreach (var item in model.PaymentDetail.PaymentList)
                        {
                            sbResult.Append("<tr>");
                            sbResult.Append("<td>" + i + ".</td>");
                            sbResult.Append("<td>" + item.CreatedDate + "</td>");
                            sbResult.Append("<td>" + item.InvoiceNo + "</td>");
                            sbResult.Append("<td>" + item.BookingType + "</td>");
                            sbResult.Append("<td style='text-align:center;'>₹ " + item.Credit + "</td>");
                            sbResult.Append("<td style='text-align:center;'>₹ " + item.Debit + "</td>");
                            sbResult.Append("<td style='text-align:center;'>₹ " + item.Aval_Balance + "</td>");
                            sbResult.Append("<td>" + item.Status + "</td>");
                            sbResult.Append("</tr>");
                            i++;
                        }
                    }
                    sbResult.Append("</tbody>");
                    sbResult.Append("</table>");
                    sbResult.Append("</div>");
                    sbResult.Append("</section>");
                    sbResult.Append("</div>");
                    sbResult.Append("</div>");
                    sbResult.Append("</section>");
                    result.Add(sbResult.ToString());
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return result;
        }
        public static List<string> GetRefundCreditNode(string refundid, string ticketno)
        {
            List<string> result = new List<string>();
            try
            {
                if (!string.IsNullOrEmpty(refundid) && !string.IsNullOrEmpty(ticketno))
                {
                    CreditNodeDetails creditNode = FlightLibraryHelper.GetRefundCreditNode(refundid);
                    if (creditNode != null)
                    {
                        AgencyDetails agencyDel = AccountLibraryHelper.GetAgencyDetails(creditNode.UserID);
                        TBL_COMPANYADDRESS compAddress = new TBL_COMPANYADDRESS();

                        bool MgtFeeVisibleStatus = false;
                        bool IsCorp = false;
                        if (IsCorp)
                        {
                            compAddress = FlightLibraryHelper.GetCompanyAddress("CORP");
                        }
                        else
                        {
                            compAddress = FlightLibraryHelper.GetCompanyAddress("FWU");
                        }

                        StringBuilder sbResult = new StringBuilder();
                        sbResult.Append("<h5 class='modal-title'><i class='icofont-search-document'></i>&nbsp;CREDIT NOTE : <strong style='color: green;'>" + refundid + "</strong></h5>");
                        sbResult.Append("<button type='button' class='close' data-dismiss='modal' aria-label='Close' style='font-size: 40px; cursor:pointer;color: red; margin: -20px -6px 0px 0px !important'>");
                        sbResult.Append("<span aria-hidden='true'>×</span>");
                        sbResult.Append("</button>");
                        result.Add(sbResult.ToString());

                        sbResult = new StringBuilder();
                        sbResult.Append("<section class='panel'>");
                        sbResult.Append("<div class='panel-body'>");
                        sbResult.Append("<div class='row'>");
                        sbResult.Append("<div class='col-md-6'>");
                        sbResult.Append("<span><strong>" + compAddress.COMPANYNAME + "</strong></span><br>");
                        sbResult.Append("<span>" + compAddress.COMPANYADDRESS + "</span><br>");
                        sbResult.Append("<span>Ph: " + compAddress.PHONENO + " Fax: " + compAddress.FAX + "</span><br>");
                        sbResult.Append("<span>Email: " + compAddress.EMAIL + "</span>");
                        sbResult.Append("</div>");
                        sbResult.Append("<div class='col-md-6' style='text-align: right;'>");
                        sbResult.Append("<span><strong>" + agencyDel.Agency_Name + "</strong></span><br>");
                        sbResult.Append("<span>" + agencyDel.Address + "</span><br>");
                        sbResult.Append("<span>" + agencyDel.City + ", " + agencyDel.State + ", " + agencyDel.ZipCode + "</span><br>");
                        sbResult.Append("<span>" + agencyDel.Country + "</span>");
                        sbResult.Append("</div>");
                        sbResult.Append("</div>");
                        sbResult.Append("</div>");
                        sbResult.Append("</section>");

                        sbResult.Append("<section class='panel'>");
                        sbResult.Append("<div class='panel-body table-responsive' style='margin: 15px auto;'>");
                        sbResult.Append("<table class='table table-collapse-phone' cellspacing='0' width='100%' showheaderwhenempty='true' style='width: 100%; max-width: 100% !important; border: 1px solid #ccc; border: 1px solid #b7b7b7; border-radius: 5px; '>");
                        sbResult.Append("<thead><tr>");
                        sbResult.Append("<th>PNR</th><th>Ticket</th><th>Airline</th><th>Passenger</th><th>Sector</th>");
                        sbResult.Append("<th>Departure Date</th><th>BaseFare</th><th>Tax</th><th>Srv Tax</th><th>TF</th><th>Total</th>");
                        sbResult.Append("</tr></thead>");
                        sbResult.Append("<tbody>");

                        double Total = Convert.ToDouble(creditNode.Base_Fare) + Convert.ToDouble(!string.IsNullOrEmpty(creditNode.Tax) ? creditNode.Tax : "0") + Convert.ToDouble(creditNode.Service_Tax) + Convert.ToDouble(creditNode.Tran_Fees);


                        sbResult.Append("<tr>");
                        sbResult.Append("<td>" + creditNode.pnr_locator + "</td>");
                        sbResult.Append("<td>" + creditNode.Tkt_No + "</td>");
                        sbResult.Append("<td>" + creditNode.VC + "</td>");
                        sbResult.Append("<td>" + creditNode.pax_fname + " " + creditNode.pax_lname + "</td>");
                        sbResult.Append("<td>" + creditNode.Sector + "</td>");
                        sbResult.Append("<td>" + creditNode.departure_date + "</td>");
                        sbResult.Append("<td>" + creditNode.Base_Fare + "</td>");
                        sbResult.Append("<td>" + creditNode.Tax + "</td>");
                        if (!IsCorp)
                        {
                            sbResult.Append("<td>" + creditNode.Service_Tax + "</td>");
                            sbResult.Append("<td>0</td>");
                        }
                        sbResult.Append("<td>" + Total + "</td>");
                        sbResult.Append("</tr>");

                        sbResult.Append("<tr>");
                        sbResult.Append("<td colspan='9'></td>");
                        sbResult.Append("<td>DISCOUNT(-)</td>");
                        sbResult.Append("<td>" + (Convert.ToDouble(creditNode.Discount) - Convert.ToDouble(creditNode.Tran_Fees)) + "</td>");
                        sbResult.Append("</tr>");

                        sbResult.Append("<tr>");
                        sbResult.Append("<td colspan='9'></td>");
                        sbResult.Append("<td>TDS(+)</td>");
                        sbResult.Append("<td>" + creditNode.TDS + "</td>");
                        sbResult.Append("</tr>");

                        double NetFare = (Total + Convert.ToDouble(creditNode.TDS) - Convert.ToDouble(creditNode.Discount));
                        sbResult.Append("<tr>");
                        sbResult.Append("<td colspan='9'></td>");
                        sbResult.Append("<td>Net Fare</td>");
                        sbResult.Append("<td>" + NetFare + "</td>");
                        sbResult.Append("</tr>");

                        sbResult.Append("<tr>");
                        sbResult.Append("<td colspan='9'></td>");
                        sbResult.Append("<td>CANCELLATION CHARGE(-)</td>");
                        sbResult.Append("<td>" + creditNode.CancellationCharge + "</td>");
                        sbResult.Append("</tr>");

                        sbResult.Append("<tr>");
                        sbResult.Append("<td colspan='9'></td>");
                        sbResult.Append("<td>SERVICE CHARGE(-)</td>");
                        sbResult.Append("<td>" + creditNode.ServiceCharge + "</td>");
                        sbResult.Append("</tr>");

                        sbResult.Append("<tr>");
                        sbResult.Append("<td colspan='9'></td>");
                        sbResult.Append("<td>TDS(-)</td>");
                        sbResult.Append("<td>" + creditNode.TDS + "</td>");
                        sbResult.Append("</tr>");

                        double RefAmt = NetFare - (Convert.ToDouble(creditNode.CancellationCharge) + Convert.ToDouble(creditNode.ServiceCharge));

                        sbResult.Append("<tr>");
                        sbResult.Append("<td>Amount in word</td>");
                        sbResult.Append("<td colspan='8'>Rupees : " + Commoncls.ConvertToWords(RefAmt.ToString()) + "</td>");
                        sbResult.Append("<td>Refund Amount</td>");
                        sbResult.Append("<td>" + (RefAmt - Convert.ToDouble(!string.IsNullOrEmpty(creditNode.TDS) ? creditNode.TDS : "0")) + "</td>");
                        sbResult.Append("</tr>");

                        sbResult.Append("</tbody>");
                        sbResult.Append("</table>");
                        sbResult.Append("</div>");
                        sbResult.Append("</section>");

                        result.Add(sbResult.ToString());
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return result;
        }
    }
}