﻿using Agency_Backoffice.Models.Common;
using DataBaseLibrary.Wallet;
using ModelLibrary;
using System;
using System.Collections.Generic;

namespace Agency_Backoffice.Service
{
    public static class WalletService
    {
        public static List<UploadModel> GetUploadDetials(UploadModelFilter filter)
        {
            if (!string.IsNullOrEmpty(filter.FromDate))
            {
                filter.FromDate = Commoncls.FilterDateFormate(filter.FromDate, "from");
            }
            if (!string.IsNullOrEmpty(filter.ToDate))
            {
                filter.ToDate = Commoncls.FilterDateFormate(filter.ToDate, "to");
            }

            return WalletLibraryHelper.GetUploadDetials(filter);
        }
        public static List<UploadModel> GetCashInflowDetails(UploadModelFilter filter)
        {
            if (!string.IsNullOrEmpty(filter.FromDate))
            {
                filter.FromDate = Commoncls.FilterDateFormate(filter.FromDate, "from");
            }
            if (!string.IsNullOrEmpty(filter.ToDate))
            {
                filter.ToDate = Commoncls.FilterDateFormate(filter.ToDate, "to");
            }

            return WalletLibraryHelper.GetCashInflowDetails(filter);
        }
        public static List<BankModel> GetDistributorBankDetails(BankModel model)
        {

            return WalletLibraryHelper.GetDistributorBankDetails(model);
        }
        public static bool InsertBankdetials(BankModel model)
        {
            return WalletLibraryHelper.InsertBankdetials(model);
        }
        public static bool InsertDepositDetail(UploadRequest model)
        {
            return WalletLibraryHelper.InsertDepositDetail(model);
        }
        //public static string ConvertStringDateToStringDateFormate(string date)
        //{
        //    DateTime dtDate = new DateTime();

        //    if (!string.IsNullOrEmpty(date))
        //    {
        //        dtDate = DateTime.Parse(date);
        //        return dtDate.ToString("yyyy-MM-dd hh:mm tt");//2021-10-12 12:00 AM
        //    }

        //    return string.Empty;
        //}

        #region [Upload Request]
        public static List<UploadBankInformation> DepositeBankList(string agencyUserId)
        {
            return WalletLibraryHelper.DepositeBankList(agencyUserId);
        }
        public static List<string> DepositeOfficeList(string agencyUserId)
        {
            return WalletLibraryHelper.DepositeOfficeList(agencyUserId);
        }
        public static List<string> DepositeAccountAndBranch(string actionType, string bank = null, string type = null)
        {
            return WalletLibraryHelper.DepositeAccountAndBranch(actionType, bank, type);
        }
        #endregion
    }
}