﻿using Agency_Backoffice.Helper;
using Agency_Backoffice.Models;
using Agency_Backoffice.Models.Common;
using DataBaseLibrary.Account;
using ModelLibrary;
using System;
using System.Collections.Generic;
using System.Data;

namespace Agency_Backoffice.Service
{
    public static class AccountService
    {
        #region [Export To Excel]
        public static DataTable DomIntlSaleRegisterExportToExcel(AccountLedgerFilter filter)
        {
            return AccountLibraryHelper.DomIntlSaleRegisterExportToExcel(filter);
        }
        public static DataTable AgentDetaildynamicExportToExcel(AgentProfileFilter filter)
        {
            return AccountLibraryHelper.AgentDetaildynamicExportToExcel(filter);
        }
        #endregion
        public static List<AccountLedgerReport> GetLedgerDetail(AccountLedgerFilter filter)
        {
            if (!string.IsNullOrEmpty(filter.FromDate))
            {
                if (!filter.FromDate.Contains("AM"))
                {
                    filter.FromDate = Commoncls.FilterDateFormate(filter.FromDate, "from");
                }               
                
            }
            if (!string.IsNullOrEmpty(filter.ToDate))
            {
                if (!filter.ToDate.Contains("PM"))
                {
                    filter.ToDate = Commoncls.FilterDateFormate(filter.ToDate, "to");
                }
            }
            if (!string.IsNullOrEmpty(filter.AgentId))
            {
                filter.SearchType = "Agent";
            }
            else
            {
                filter.SearchType = "Own";
            }
            
            return AccountLibraryHelper.GetLedgerDetail(filter);
        }

        public static DataTable ExportLedgerReport(AccountLedgerFilter filter)
        {
            return AccountLibraryHelper.ExportLedgerReport(filter);
        }
        public static AccountLedgerReport GetClosingbal(AccountLedgerFilter filter)
        {
            return AccountLibraryHelper.GetClosingbal(filter);
        }

        public static int insertAgencyRegistrationDetails(AgencyDetails details)
        {
            return AccountLibraryHelper.insertAgencyRegistrationDetails(details);
        }
        public static AccountLedgerReport GetInvoice(string orderid)
        {
            return AccountLibraryHelper.GetInvoice(orderid);
        }
        public static AgencyDetails GetAgencyDetails(string userId)
        {
            return AccountLibraryHelper.GetAgencyDetails(userId);
        }
        public static AccountLedgerReport GetCompanyAddress(string AddressType)
        {
            return AccountLibraryHelper.GetCompanyAddress(AddressType);
        }
        public static List<AccountLedgerReport> GetStaffTransaction(AccountLedgerFilter filter)
        {
            if (!string.IsNullOrEmpty(filter.FromDate))
            {
                filter.FromDate = Commoncls.FilterDateFormate(filter.FromDate, "from");
            }
            if (!string.IsNullOrEmpty(filter.ToDate))
            {
                filter.ToDate = Commoncls.FilterDateFormate(filter.ToDate, "to");
            }

            return AccountLibraryHelper.GetStaffTransaction(filter);
        }
        public static List<AccountLedgerReport> IntGetInvoice(AccountLedgerFilter filter)
        {
            if (!string.IsNullOrEmpty(filter.FromDate))
            {
                filter.FromDate = Commoncls.FilterDateFormate(filter.FromDate, "from");
            }
            if (!string.IsNullOrEmpty(filter.ToDate))
            {
                filter.ToDate = Commoncls.FilterDateFormate(filter.ToDate, "to");
            }

            return AccountLibraryHelper.IntGetInvoice(filter);
        }
        public static List<AgentUpdateProfile> GetCreditlimithistory(AgentProfileFilter filter)
        {
            if (!string.IsNullOrEmpty(filter.FromDate))
            {
                filter.FromDate = Commoncls.FilterDateFormate(filter.FromDate, "from");
            }
            if (!string.IsNullOrEmpty(filter.ToDate))
            {
                filter.ToDate = Commoncls.FilterDateFormate(filter.ToDate, "to");
            }

            return AccountLibraryHelper.GetCreditlimithistory(filter);
        }
        public static List<AgentUpdateProfile> GetAgencyDetailsDynamic(AgentProfileFilter filter)
        {
            if (!string.IsNullOrEmpty(filter.FromDate))
            {
                filter.FromDate = Commoncls.FilterDateFormate(filter.FromDate, "from");
            }
            if (!string.IsNullOrEmpty(filter.ToDate))
            {
                filter.ToDate = Commoncls.FilterDateFormate(filter.ToDate, "to");
            }

            return AccountLibraryHelper.GetAgencyDetailsDynamic(filter);
        }
        //public static string ConvertStringDateToStringDateFormate(string date)
        //{
        //    DateTime dtDate = new DateTime();

        //    if (!string.IsNullOrEmpty(date))
        //    {
        //        dtDate = DateTime.Parse(date);
        //        return dtDate.ToString("MM/dd/yyyy hh:mm tt");
        //    }

        //    return string.Empty;
        //}
        public static bool LoginProcess(AgentStaffLogin login, ref string msg, ref string redirectUrl)
        {
            return AccountHelper.LoginProcess(login, ref msg, ref redirectUrl);
        }
        public static string ForgetPassword(ForgetPassword forget)
        {
            return AccountHelper.ForgetPassword(forget);
        }
        public static bool UpdateAgentProfileDetails(AgentUpdateProfile profile)
        {
            return AccountHelper.UpdateAgentProfileDetails(profile);
        }

        public static bool UpdateAgentByDistr(AgencyDetails details)
        {
            return AccountDataBase.UpdateAgentByDistr(details);
        }
        public static List<State> StateList(string country)
        {
            return AccountLibraryHelper.StateList(country);
        }

        public static List<City> CityList(string stateId)
        {
            return AccountLibraryHelper.CityList(stateId);
        }

        #region [Session Section]
        public static bool IsAgencyLogin(ref AgencyLoginSession lu)
        {
            return AccountHelper.IsAgencyLogin(ref lu);
        }
        public static bool GetLoginSession(ref LoginSessionDetails lu)
        {
            return AccountHelper.GetLoginSession(ref lu);
        }
        public static bool LogoutBackendLogin()
        {
            return AccountHelper.LogoutBackendLogin();
        }
        #endregion

        #region [Update Profile]
        public static void ReBindAgencyDetails(string userid)
        {
            AccountHelper.ReBindAgencyDetails(userid);
        }
        #endregion
    }
}