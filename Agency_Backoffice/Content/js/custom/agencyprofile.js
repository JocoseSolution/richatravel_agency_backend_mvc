﻿//$(function () {
//    var gstapplied = $("#hdnGstApplied").val();
//    if (gstapplied == "True") {
//        $("#IfGSTApplied").removeClass("hidden");
//        //$(".gsthaiapplied").removeClass("hidden");
//        //$(".gstnahihaiapplied").addClass("hidden");
//    }
//    else {
//        $("#IfNOTGSTApplied").removeClass("hidden");
//        $("#IfGSTApplied").addClass("hidden");
//        //$(".gsthaiapplied").addClass("hidden");
//        //$(".gstnahihaiapplied").removeClass("hidden");
//    }
//});

$("#change").click(function () { $(".Security").addClass("hidden"); $(".Securityupdate").removeClass("hidden"); $("#txtAgencyOldPass").val(""); $("#txtAgencyNewPass").val(""); $("#txtAgencyNewConfPass").val(""); });
$("#cancel").click(function () { $(".Security").removeClass("hidden"); $(".Securityupdate").addClass("hidden"); });
$("#gstbtn").click(function () {
    $(".Gstdetails").addClass("hidden");
    $(".Gstdetailsupadte").removeClass("hidden");

    $("#IfGSTApplied").addClass("hidden");
    $("#IfNOTGSTApplied").removeClass("hidden");

    $("#notAppliedgst").click();

    $("#GST_Number").val("");
    $("#Gst_CompanyName").val("");
    $("#Gst_CompAddress").val("");
    $("#Gst_State").prop('selectedIndex', 0);
    $("#Gst_City").children().remove();
    $("#Gst_City").append($('<option>').val(0).text('-- Select City --'));
    $("#Gst_Pincode").val("");
    $("#Gst_Phone").val("");
    $("#Gst_Email").val("");
    $("#GST_Remark").val("");
});
$("#gstcancel").click(function () {
    $(".Gstdetails").removeClass("hidden");
    $(".Gstdetailsupadte").addClass("hidden");

    $("#IfGSTApplied").removeClass("hidden");
    $("#IfNOTGSTApplied").addClass("hidden");
});
$("#appliedgst").click(function () {
    $(".notappiledbox").addClass("hidden"); $(".appiledbox").removeClass("hidden");

    $("#GST_Number").val("");
    $("#Gst_CompanyName").val("");
    $("#Gst_CompAddress").val("");
    $("#Gst_State").prop('selectedIndex', 0);
    $("#Gst_City").children().remove();
    $("#Gst_City").append($('<option>').val(0).text('-- Select City --'));
    $("#Gst_Pincode").val("");
    $("#Gst_Phone").val("");
    $("#Gst_Email").val("");
    $("#GST_Remark").val("");
});
$("#notAppliedgst").click(function () {
    $(".notappiledbox").removeClass("hidden"); $(".appiledbox").addClass("hidden");

    $("#GST_Number").val("");
    $("#Gst_CompanyName").val("");
    $("#Gst_CompAddress").val("");
    $("#Gst_State").prop('selectedIndex', 0);
    $("#Gst_City").children().remove();
    $("#Gst_City").append($('<option>').val(0).text('-- Select City --'));
    $("#Gst_Pincode").val("");
    $("#Gst_Phone").val("");
    $("#Gst_Email").val("");
    $("#GST_Remark").val("");
});

$("#txtAgencyOldPass").change(function () {
    $("#errorwrongpass").html("");
    var oldpassword = $(this).val();
    if (oldpassword != "") {
        var hdnPass = $("#hdnPass").val();
        if (oldpassword != hdnPass) {
            $("#errorwrongpass").html("wrong password!");
        }
    }
});

function UpdateSecurityFeature() {
    var thisbutton = $("#btnUpdateSecurity");
    if (CheckFocusBlankValidation("txtAgencyOldPass")) return !1;
    if ($("#errorwrongpass").html() != "") { return false; }
    if (CheckFocusBlankValidation("txtAgencyNewPass")) return !1;
    var ispasscorrect = CheckPassword($("#txtAgencyNewPass").val());
    if (ispasscorrect) {
        if (CheckFocusBlankValidation("txtAgencyNewConfPass")) return !1;
        if (CheckSamePasswordValidation('txtAgencyNewPass', 'txtAgencyNewConfPass')) return !1;

        $(thisbutton).html("Please Wait... <i class='fa fa-pulse fa-spinner processspin'></i>");

        $.ajax({
            type: "Post",
            url: ApplicationUrl + "/Account/ProcessToChangePassword",
            data: '{password: ' + JSON.stringify($("#txtAgencyNewPass").val()) + '}',
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (data) {
                if (data != null) {
                    if (data[0] == "success") {
                        ShowMessagePopup("<i class='fa fa-check-circle text-success'></i> Success !", "#00a300", "New Password changed successfully.", "#00a300", "reload");
                    }
                    else {
                        ShowMessagePopup("<i class='fa fa-exclamation-triangle text-danger'></i> Failed", "#d91717", "Error Occured !", "#d91717");
                    }
                }
                $(thisbutton).html("Update");
            }
        });
    }
}

function UploadCompanyLogo() {
    var thisbutton = $("#btnUploadCompLogo");
    if (CheckFocusBlankValidation("fluCompLogo")) return !1;
    var imgName = $("#fluCompLogo").val();
    if (imgName != "" && imgName != undefined) {
        $(thisbutton).html("Please Wait... <i class='fa fa-pulse fa-spinner processspin'></i>");

        var fileUpload = $("#fluCompLogo").get(0);
        var files = fileUpload.files;
        var fileData = new FormData();
        for (var i = 0; i < files.length; i++) {
            fileData.append(files[i].name, files[i]);
        }

        $.ajax({
            url: ApplicationUrl + '/Account/ProcessToChangeCompanyLogo',
            type: "POST",
            contentType: false, // Not to set any content header  
            processData: false, // Not to process data  
            data: fileData,
            //data: { fileData, imageId: imgId, hotcode: hotelco },
            success: function (data) {
                ShowMessagePopup("<i class='fa fa-check-circle text-success'></i> Success !", "#00a300", "Image uploaded successfully.", "#00a300", "reload");

                $(thisbutton).html("Upload");
            }
        });
    }
}

$("#fluCompLogo").on("change", function (e) {
    var imgType = this.files[0].type;
    var imgName = this.files[0].name.toLowerCase();
    let isok = false;
    if (imgName.indexOf(".jpg") != -1) { isok = true; }

    if (imgType != "image/jpeg" && isok == false) {
        /*ShowMessagePopup("<i class='fa fa-exclamation-triangle text-danger'></i> Failed !", "#d91717", "Image formate must be in 'JPEG/JPG' only !", "#d91717");*/
        ShowMessagePopup("<i class='fa fa-exclamation-triangle text-danger'></i> Failed !", "#d91717", "Image formate must be in 'JPG' only !", "#d91717");
        $(this).val("");
    }
    else if (isok == false) {
        /*ShowMessagePopup("<i class='fa fa-exclamation-triangle text-danger'></i> Failed !", "#d91717", "Image formate must be in 'JPEG/JPG' only !", "#d91717");*/
        ShowMessagePopup("<i class='fa fa-exclamation-triangle text-danger'></i> Failed !", "#d91717", "Image formate must be in 'JPG' only !", "#d91717");
        $(this).val("");
    }
});

function CheckPassword(inputtxt) {
    var passw = /^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[^\w\s]).{8,16}$/;
    if (inputtxt.match(passw)) {
        return true;
    }
    else {
        ShowMessagePopup("<i class='fa fa-exclamation-triangle text-danger'></i> Wrong Password !", "#d91717", "Password must contain:8-16 characters atleast 1 UpperCase Alphabet, 1 LowerCase Alphabet, 1 Number and 1 Special Character.", "#d91717");
        return false;
    }
}

function ShowMessagePopup(headerHeading, headcolor, bodyMsg, bodycolor, actiontype) {
    $(".successmessage").click();
    $(".succmodelheading").css("color", headcolor).html(headerHeading);
    $(".succmodelcontent").css("color", bodycolor).html("<p>" + bodyMsg + "</p>");
    if (actiontype == "reload") { $("#CommonFooterSection").html("<button type='button' class='btn btn-success' onclick='window.location.reload();'>Close</button>"); }
}

function FillCityList() {
    $("#Gst_City").prop("disabled", true);
    $("#Gst_City").children().remove();
    $("#Gst_City").append($('<option>').val(0).text('Please wait...')).after("<i class='fa fa-pulse fa-spinner text-success' style='position: absolute;float: right;right: 10px;top: 30px;font-size: 25px;'></i>");
    var stateID = $('#Gst_State').val();
    $.ajax({
        url: ApplicationUrl + '/Account/BindCityList',
        type: "GET",
        dataType: "JSON",
        data: { stateID: stateID },
        success: function (data) {
            $('#Gst_City').next('i').remove();
            $("#Gst_City").children().remove();
            $("#Gst_City").append($('<option>').val(0).text('-- Select City --'));
            $.each(data, function (i, data) {
                $("#Gst_City").append($('<option></option>').val(data.Value).html(data.Text));
            });
            $("#Gst_City").prop("disabled", false);
        }
    });
}

function UpdateGstDetailFeature() {
    var thisbutton = $("#btnUpdateGstDetail");

    if ($("#Gst_Email").val() != "") { if (CheckEmailValidatoin("Gst_Email")) return !1; }

    var gstdel = {};
    gstdel.GST_Number = $("#GST_Number").val();
    gstdel.Gst_CompanyName = $("#Gst_CompanyName").val();
    gstdel.Gst_CompAddress = $("#Gst_CompAddress").val();
    gstdel.Gst_State = $("#Gst_State option:selected").text();
    gstdel.Gst_StateCode = $("#Gst_State option:selected").val();
    gstdel.Gst_City = $("#Gst_City").val();
    gstdel.Gst_Pincode = $("#Gst_Pincode").val();
    gstdel.Gst_Phone = $("#Gst_Phone").val();
    gstdel.Gst_Email = $("#Gst_Email").val();
    gstdel.GST_Remark = $("#GST_Remark").val();

    var radioValue = $("input[name='gsttype']:checked").val();
    gstdel.Is_GST_Apply = radioValue == "Applied" ? true : false;

    $(thisbutton).html("Please Wait... <i class='fa fa-pulse fa-spinner processspin'></i>");

    $.ajax({
        type: "Post",
        url: ApplicationUrl + "/Account/ProcessToUpdateGstDetail",
        data: '{gstdel: ' + JSON.stringify(gstdel) + '}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (data) {
            if (data != null) {
                if (data[0] == "success") {
                    ShowMessagePopup("<i class='fa fa-check-circle text-success'></i> Success !", "#00a300", "GST details changed successfully.", "#00a300", "reload");
                }
                else {
                    ShowMessagePopup("<i class='fa fa-exclamation-triangle text-danger'></i> Failed", "#d91717", "Error Occured !", "#d91717");
                }
            }
            $(thisbutton).html("Update");
        }
    });
}

