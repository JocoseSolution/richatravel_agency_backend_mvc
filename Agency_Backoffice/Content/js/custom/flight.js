﻿function CheckForm() {
    $('.maincss').addClass('hidden');
    $('.tempscss').removeClass('hidden');
}

TicketDetails = (orderid) => {
    if (orderid != "") {
        $("#modelTicketDetailHeading").html("").html('<div class="col-sm-12 text-center"><h4>Please wait...<i class="fa fa-spinner fa-pulse"></i></h4></div>');
        $("#TicketDetailBodyContent").html("").html('<div class="col-sm-12 text-center"><h4>Please wait...<i class="fa fa-spinner fa-pulse"></i></h4></div>');
        $("#btnTicketDetailsReportClick").click();
        $.ajax({
            type: "Post",
            url: ApplicationUrl + "/FlightReport/GetTicketDetails",
            data: '{orderid: ' + JSON.stringify(orderid) + '}',
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (data) {
                $("#modelTicketDetailHeading").html("").html(data[0]);
                $("#TicketDetailBodyContent").html("").html(data[1]);
            }
        });
    }
}

RefundCreditNode = (refundid, ticketno) => {
    if (refundid != "" && ticketno != "") {
        $("#modelRefundCreditNodeHeading").html("").html('<div class="col-sm-12 text-center"><h4>Please wait...<i class="fa fa-spinner fa-pulse"></i></h4></div>');
        $("#RefundCreditNodeBodyContent").html("").html('<div class="col-sm-12 text-center"><h4>Please wait...<i class="fa fa-spinner fa-pulse"></i></h4></div>');
        $("#btnRefundCreditNodeClick").click();
        $.ajax({
            type: "Post",
            url: ApplicationUrl + "/FlightReport/GetRefundCreditNode",
            data: '{refundid: ' + JSON.stringify(refundid) + ',ticketno: ' + JSON.stringify(ticketno) + '}',
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (data) {
                $("#modelRefundCreditNodeHeading").html("").html(data[0]);
                $("#RefundCreditNodeBodyContent").html("").html(data[1]);
            }
        });
    }
}