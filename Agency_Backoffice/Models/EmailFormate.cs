﻿using Agency_Backoffice.Models.Common;
using ModelLibrary;
using System;
using System.Text;

namespace Agency_Backoffice.Models
{
    public static class EmailFormate
    {
        public static string EmailFormateReissueRequest(ReIssueReFund fltds)
        {
            StringBuilder message = new StringBuilder();

            try
            {
                if (fltds != null)
                {
                    message.Append(Utility.ReadHTMLTemplate("ReissueTemplate.html"));
                    message.Replace("#Logo#", Config.WebsiteUrl + Config.WebsiteLogo);
                    message.Replace("#Url#", Config.WebsiteUrl);
                    message.Replace("#Heading#", fltds.MailHeading);
                    message.Replace("#OrderId#", fltds.OrderId);
                    message.Replace("#PnrNo#", fltds.PNR);
                    message.Replace("#PaxName#", fltds.FName+ " " +fltds.LName);
                    //message.Replace("#PaxTYpe#", fltds.PaxType);
                    message.Replace("#NetAmount#", fltds.TotalFareAfterDis);
                    message.Replace("#Remark#", fltds.Remark);
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return message.ToString();
        }
    }
}