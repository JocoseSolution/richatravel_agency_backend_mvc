﻿using ModelLibrary;
using System;
using System.Collections.Generic;
using System.Text;
using System.Web.Mvc;

namespace Agency_Backoffice.Models.Common
{
    public static class Commoncls
    {
        public static string GetSpilitByCost(string param)
        {
            string result = string.Empty;

            if (!string.IsNullOrEmpty(param))
            {
                string[] city = param.Split('(');
                if (city.Length == 2)
                {
                    result = city[1].Replace(")", "");
                }
            }

            return result;
        }
        public static string GetSpilitedDateFormate(string date)//200220
        {
            string result = string.Empty;
            if (!string.IsNullOrEmpty(date))
            {
                if (date.Length == 6)
                {
                    if (!string.IsNullOrEmpty(date))
                    {
                        StringBuilder sb = new StringBuilder();
                        for (int i = 0; i < date.Length; i++)
                        {
                            if (i % 2 == 0)
                            {
                                sb.Append('-');
                            }
                            sb.Append(date[i]);
                        }

                        result = ConvertDateToStringDateFormate(sb.ToString().TrimStart('-'));
                    }

                }
            }
            return result;
        }
        public static string ConvertDateToStringDateFormate(string date)
        {
            DateTime dtDate = new DateTime();

            if (!string.IsNullOrEmpty(date))
            {
                dtDate = DateTime.Parse(date);
                return dtDate.ToString("dd/MMM/yy");
            }

            return string.Empty;
        }

        public static string GetEmptyDate()
        {
            return DateTime.Now.ToString("dd/MM/yyyy");
        }
        public static string FilterDateFormate(string date, string type)
        {
            string rutDate = string.Empty;
            if (!string.IsNullOrEmpty(date))
            {
                string[] dateSplit = date.Split('/');
                if (type == "from")
                {
                    rutDate = dateSplit[2] + "-" + dateSplit[1] + "-" + dateSplit[0] + " 12:00 AM";
                }
                else
                {
                    rutDate = dateSplit[2] + "-" + dateSplit[1] + "-" + dateSplit[0] + " 11:59 PM";
                }
            }
            return rutDate;
        }
        public static string ConvertNumbertoWords(long number)
        {
            if (number == 0) return "Zero";
            if (number < 0) return "minus " + ConvertNumbertoWords(Math.Abs(number));
            string words = "";
            if ((number / 1000000) > 0)
            {
                words += ConvertNumbertoWords(number / 100000) + " Lakh ";
                number %= 1000000;
            }
            if ((number / 1000) > 0)
            {
                words += ConvertNumbertoWords(number / 1000) + " Thousand ";
                number %= 1000;
            }
            if ((number / 100) > 0)
            {
                words += ConvertNumbertoWords(number / 100) + " Hundred ";
                number %= 100;
            }
            if (number > 0)
            {
                if (words != "") words += "And ";
                var unitsMap = new[]
                {
            "ZERO", "One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten", "Eleven", "Twelve", "Thirteen", "Fourteen", "Fifteen", "Sixteen", "Seventeen", "Eighteen", "Nineteen"
        };
                var tensMap = new[]
                {
            "ZERO", "Ten", "Twenty", "Thirty", "Forty", "Fifty", "Sixty", "Seventy", "Eighty", "Ninty"
        };
                if (number < 20) words += unitsMap[number];
                else
                {
                    words += tensMap[number / 10];
                    if ((number % 10) > 0) words += " " + unitsMap[number % 10];
                }
            }
            return words;
        }
        public static List<SelectListItem> CommonPopulateSelectListItem(List<CommonSelectListItem> comItem)
        {
            List<SelectListItem> items = new List<SelectListItem>();

            foreach (var item in comItem)
            {
                items.Add(new SelectListItem
                {
                    Text = item.Text.ToString(),
                    Value = item.Value.ToString()
                });
            }

            return items;
        }
        public static List<SelectListItem> PopulateState(List<State> stateList)
        {
            List<SelectListItem> items = new List<SelectListItem>();

            foreach (var item in stateList)
            {
                items.Add(new SelectListItem
                {
                    Text = item.StateName.ToString(),
                    Value = item.StateCode.ToString()
                });
            }

            return items;
        }
        public static List<SelectListItem> PopulateCity(List<City> cityList)
        {
            List<SelectListItem> items = new List<SelectListItem>();

            foreach (var item in cityList)
            {
                items.Add(new SelectListItem
                {
                    Text = item.CityName.ToString(),
                    Value = item.CityCode.ToString()
                });
            }

            return items;
        }
        public static List<SelectListItem> PopulateDepositeBank(List<UploadBankInformation> bankList)
        {
            List<SelectListItem> items = new List<SelectListItem>();

            foreach (var item in bankList)
            {
                items.Add(new SelectListItem
                {
                    Text = item.BankName.ToString(),
                    Value = item.BankName.ToString()
                });
            }

            return items;
        }
        public static List<SelectListItem> PopulateDepositeOffice(List<string> officeList)
        {
            List<SelectListItem> items = new List<SelectListItem>();

            foreach (var item in officeList)
            {
                items.Add(new SelectListItem
                {
                    Text = item.ToString(),
                    Value = item.ToString()
                });
            }

            return items;
        }
        public static List<SelectListItem> PopulateAccountAndBranch(List<string> abList)
        {
            List<SelectListItem> items = new List<SelectListItem>();

            foreach (var item in abList)
            {
                items.Add(new SelectListItem
                {
                    Text = item.ToString(),
                    Value = item.ToString()
                });
            }

            return items;
        }

        public static string ConvertToWords(string numb)
        {
            String val = "", wholeNo = numb, points = "", andStr = "", pointStr = "";
            String endStr = "Only";
            try
            {
                int decimalPlace = numb.IndexOf(".");
                if (decimalPlace > 0)
                {
                    wholeNo = numb.Substring(0, decimalPlace);
                    points = numb.Substring(decimalPlace + 1);
                    if (Convert.ToInt32(points) > 0)
                    {
                        andStr = "and";// just to separate whole numbers from points/cents    
                        endStr = "Paisa " + endStr;//Cents    
                        pointStr = ConvertDecimals(points);
                    }
                }
                val = String.Format("{0} {1}{2} {3}", ConvertWholeNumber(wholeNo).Trim(), andStr, pointStr, endStr);
            }
            catch { }
            return val;
        }
        private static string ConvertDecimals(string number)
        {
            String cd = "", digit = "", engOne = "";
            for (int i = 0; i < number.Length; i++)
            {
                digit = number[i].ToString();
                if (digit.Equals("0"))
                {
                    engOne = "Zero";
                }
                else
                {
                    engOne = ones(digit);
                }
                cd += " " + engOne;
            }
            return cd;
        }
        private static string ConvertWholeNumber(string Number)
        {
            string word = "";
            try
            {
                bool beginsZero = false;//tests for 0XX    
                bool isDone = false;//test if already translated    
                double dblAmt = (Convert.ToDouble(Number));
                //if ((dblAmt > 0) && number.StartsWith("0"))    
                if (dblAmt > 0)
                {//test for zero or digit zero in a nuemric    
                    beginsZero = Number.StartsWith("0");

                    int numDigits = Number.Length;
                    int pos = 0;//store digit grouping    
                    String place = "";//digit grouping name:hundres,thousand,etc...    
                    switch (numDigits)
                    {
                        case 1://ones' range    

                            word = ones(Number);
                            isDone = true;
                            break;
                        case 2://tens' range    
                            word = tens(Number);
                            isDone = true;
                            break;
                        case 3://hundreds' range    
                            pos = (numDigits % 3) + 1;
                            place = " Hundred ";
                            break;
                        case 4://thousands' range    
                        case 5:
                        case 6:
                            pos = (numDigits % 4) + 1;
                            place = " Thousand ";
                            break;
                        case 7://millions' range    
                        case 8:
                        case 9:
                            pos = (numDigits % 7) + 1;
                            place = " Million ";
                            break;
                        case 10://Billions's range    
                        case 11:
                        case 12:

                            pos = (numDigits % 10) + 1;
                            place = " Billion ";
                            break;
                        //add extra case options for anything above Billion...    
                        default:
                            isDone = true;
                            break;
                    }
                    if (!isDone)
                    {//if transalation is not done, continue...(Recursion comes in now!!)    
                        if (Number.Substring(0, pos) != "0" && Number.Substring(pos) != "0")
                        {
                            try
                            {
                                word = ConvertWholeNumber(Number.Substring(0, pos)) + place + ConvertWholeNumber(Number.Substring(pos));
                            }
                            catch { }
                        }
                        else
                        {
                            word = ConvertWholeNumber(Number.Substring(0, pos)) + ConvertWholeNumber(Number.Substring(pos));
                        }

                        //check for trailing zeros    
                        //if (beginsZero) word = " and " + word.Trim();    
                    }
                    //ignore digit grouping names    
                    if (word.Trim().Equals(place.Trim())) word = "";
                }
            }
            catch { }
            return word.Trim();
        }
        private static string ones(string Number)
        {
            int _Number = Convert.ToInt32(Number);
            String name = "";
            switch (_Number)
            {

                case 1:
                    name = "One";
                    break;
                case 2:
                    name = "Two";
                    break;
                case 3:
                    name = "Three";
                    break;
                case 4:
                    name = "Four";
                    break;
                case 5:
                    name = "Five";
                    break;
                case 6:
                    name = "Six";
                    break;
                case 7:
                    name = "Seven";
                    break;
                case 8:
                    name = "Eight";
                    break;
                case 9:
                    name = "Nine";
                    break;
            }
            return name;
        }
        private static string tens(string Number)
        {
            int _Number = Convert.ToInt32(Number);
            String name = null;
            switch (_Number)
            {
                case 10:
                    name = "Ten";
                    break;
                case 11:
                    name = "Eleven";
                    break;
                case 12:
                    name = "Twelve";
                    break;
                case 13:
                    name = "Thirteen";
                    break;
                case 14:
                    name = "Fourteen";
                    break;
                case 15:
                    name = "Fifteen";
                    break;
                case 16:
                    name = "Sixteen";
                    break;
                case 17:
                    name = "Seventeen";
                    break;
                case 18:
                    name = "Eighteen";
                    break;
                case 19:
                    name = "Nineteen";
                    break;
                case 20:
                    name = "Twenty";
                    break;
                case 30:
                    name = "Thirty";
                    break;
                case 40:
                    name = "Fourty";
                    break;
                case 50:
                    name = "Fifty";
                    break;
                case 60:
                    name = "Sixty";
                    break;
                case 70:
                    name = "Seventy";
                    break;
                case 80:
                    name = "Eighty";
                    break;
                case 90:
                    name = "Ninety";
                    break;
                default:
                    if (_Number > 0)
                    {
                        name = tens(Number.Substring(0, 1) + "0") + " " + ones(Number.Substring(1));
                    }
                    break;
            }
            return name;
        }
    }
}