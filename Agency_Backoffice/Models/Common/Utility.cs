﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace Agency_Backoffice.Models.Common
{
    public static class Utility
    {
        public static string ReadHTMLTemplate(string fileName)
        {
            using (StreamReader streamReader = new StreamReader(HttpContext.Current.Server.MapPath("~") + "\\" + "/EmailTemplates" + "/" + fileName))
            {
                try
                {
                    string xmlString = streamReader.ReadToEnd();
                    return xmlString;
                }
                finally
                {
                    streamReader.Close();
                    streamReader.Dispose();
                }
            }
        }
        public static string GetImagePath(string imageName, string folderName, string id)
        {
            return !string.IsNullOrEmpty(imageName) ? "/GetFile.ashx?path=" + folderName + "/" + id + "/" + imageName : string.Empty;
        }
        public static string GetFullPathWithAppData(string path, string id)
        {
            string fullPath = "/App_Data/";

            if (!string.IsNullOrEmpty(path))
            {
                fullPath = fullPath + path + "/" + id + "/";
            }

            return fullPath.Replace("//", "/");
        }
        public static string GetFullPathWithCustom(string path, string id)
        {
            string fullPath = "/Content/images/";

            if (!string.IsNullOrEmpty(path))
            {
                fullPath = fullPath + path + "/" + id + "/";
            }

            return fullPath.Replace("//", "/");
        }
        public static string FilterDateFormate(string date, string type)
        {
            if (!string.IsNullOrEmpty(date))
            {
                string formatedDate = "";
                string[] splitDate = date.Split('/');
                formatedDate = splitDate[2] + "-" + splitDate[1] + "-" + splitDate[0];

                if (type == "from")
                {
                    return formatedDate + " 12:00:00 AM";//10/09/2021 12:00:00 AM
                }
                else
                {
                    return formatedDate + " 11:59:59 PM";//10/09/2021 11:59:59 PM
                }
            }

            return string.Empty;
        }
        public static string DateFormateDDMMMYYYY(string date)
        {
            DateTime dtDate = new DateTime();

            if (!string.IsNullOrEmpty(date))
            {
                dtDate = DateTime.Parse(date);
                return dtDate.ToString("dd MMM yyyy hh:mm:ss tt");
            }

            return string.Empty;
        }
    }
}