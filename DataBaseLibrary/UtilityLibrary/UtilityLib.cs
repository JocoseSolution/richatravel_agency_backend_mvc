﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataBaseLibrary.UtilityLibrary
{
    internal static class UtilityLib
    {
        public static string DateFormateDDMMMYYYY(string date)
        {
            DateTime dtDate = new DateTime();

            if (!string.IsNullOrEmpty(date))
            {
                dtDate = DateTime.Parse(date);
                return dtDate.ToString("dd MMM yyyy hh:mm tt");//hh:mm:ss tt
            }

            return string.Empty;
        }
    }
}
