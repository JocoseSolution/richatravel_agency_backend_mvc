﻿using ModelLibrary;
using System;
using System.Data;
using System.Data.SqlClient;

namespace DataBaseLibrary.Account
{
    public static class AccountDataBase
    {
        private static SqlCommand sqlCommand { get; set; }
        private static SqlDataAdapter sqlDataAdapter { get; set; }
        private static DataSet dataSet { get; set; }
        private static DataTable dataTable { get; set; }
        public static DataTable GetAgencyDetailsDynamic(AgentProfileFilter filter)
        {
            try
            {
                sqlCommand = new SqlCommand("AgencyDetailsDynamic", ConnectToDataBase.MyAmdDBConnection);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("UserId", filter.AgentId);
                sqlCommand.Parameters.AddWithValue("AgentType", filter.Agent_Type);
                sqlCommand.Parameters.AddWithValue("SalesExecID", filter.SalesExecID);
                sqlCommand.Parameters.AddWithValue("FromDate", filter.FromDate);
                sqlCommand.Parameters.AddWithValue("ToDate", filter.ToDate);
                sqlCommand.Parameters.AddWithValue("DistrId", filter.Loginid);
                sqlCommand.Parameters.AddWithValue("UserType", filter.UserType);
                sqlCommand.Parameters.AddWithValue("DiSearch", filter.DiSearch);
                sqlDataAdapter = new SqlDataAdapter();
                sqlDataAdapter.SelectCommand = sqlCommand;
                dataSet = new DataSet();
                dataTable = new DataTable();
                sqlDataAdapter.Fill(dataSet, "AgencyDetails");
                dataTable = dataSet.Tables["AgencyDetails"];
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            filter.AgentId = null;
            return dataTable;
        }

        public static DataTable AgentStaffLogin(AgentStaffLogin login, ref string loginType)
        {
            try
            {
                sqlCommand = new SqlCommand("SP_AGENT_STAFFLOGIN", ConnectToDataBase.MyAmdDBConnection);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@UserId", login.UserId);
                sqlCommand.Parameters.AddWithValue("@Password", login.Password);
                sqlCommand.Parameters.AddWithValue("@IPAddress", login.IpAddress);
                sqlCommand.Parameters.AddWithValue("@ActionType", "GETUSERTYPE");
                sqlCommand.Parameters.Add("@Msg", SqlDbType.VarChar, 100);
                sqlCommand.Parameters["@Msg"].Direction = ParameterDirection.Output;

                sqlDataAdapter = new SqlDataAdapter();
                sqlDataAdapter.SelectCommand = sqlCommand;
                dataSet = new DataSet();

                dataTable = new DataTable();
                sqlDataAdapter.Fill(dataSet, "AgentStaffRegister");
                dataTable = dataSet.Tables["AgentStaffRegister"];

                loginType = Convert.ToString(sqlCommand.Parameters["@Msg"].Value).ToUpper();
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return dataTable;
        }
        public static DataTable CheckCRRecord(string agentid, ref string count)
        {
            try
            {

                sqlCommand = new SqlCommand("SP_CKECKCRDETAILS", ConnectToDataBase.MyAmdDBConnection);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@AGENTID", agentid);
                sqlCommand.Parameters.AddWithValue("@Count", SqlDbType.Int);
                sqlCommand.Parameters["@Count"].Direction = ParameterDirection.Output;
                sqlDataAdapter = new SqlDataAdapter();
                sqlDataAdapter.SelectCommand = sqlCommand;
                dataSet = new DataSet();
                dataTable = new DataTable();
                sqlDataAdapter.Fill(dataSet, "CKECKCRDETAILS");
                dataTable = dataSet.Tables["CKECKCRDETAILS"];

                count = Convert.ToString(sqlCommand.Parameters["@Count"].Value).ToUpper();
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return dataTable;
        }
        public static DataSet User_Auth(string uid, string passwd)
        {
            DataSet ds = new DataSet();
            try
            {
                sqlDataAdapter = new SqlDataAdapter("UserLoginNew", ConnectToDataBase.MyAmdDBConnection);
                sqlDataAdapter.SelectCommand.CommandType = CommandType.StoredProcedure;
                sqlDataAdapter.SelectCommand.Parameters.AddWithValue("@uid", uid);
                sqlDataAdapter.SelectCommand.Parameters.AddWithValue("@pwd", passwd);

                sqlDataAdapter.Fill(ds);
            }
            catch (Exception ex)
            {
                //clsErrorLog.LogInfo(ex);
                ex.ToString();
            }

            return ds;
        }
        public static DataTable GetAgencyDetails(string userId)
        {
            try
            {
                sqlCommand = new SqlCommand("AgencyDetails", ConnectToDataBase.MyAmdDBConnection);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("UserId", userId);

                sqlDataAdapter = new SqlDataAdapter();
                sqlDataAdapter.SelectCommand = sqlCommand;
                dataSet = new DataSet();

                dataTable = new DataTable();
                sqlDataAdapter.Fill(dataSet, "agent_register");
                dataTable = dataSet.Tables["agent_register"];
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return dataTable;
        }
        public static DataTable GetCompanyAddress(string AddressType)
        {
            try
            {
                sqlCommand = new SqlCommand("SP_GETADDRESS", ConnectToDataBase.MyAmdDBConnection);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@TYPE", AddressType);

                sqlDataAdapter = new SqlDataAdapter();
                sqlDataAdapter.SelectCommand = sqlCommand;
                dataSet = new DataSet();

                dataTable = new DataTable();
                sqlDataAdapter.Fill(dataSet, "GETADDRESS");
                dataTable = dataSet.Tables["GETADDRESS"];
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return dataTable;
        }
        public static DataSet LastLoginTime(string uid)
        {
            DataSet ds = new DataSet();
            try
            {
                sqlDataAdapter = new SqlDataAdapter("Sp_Tbl_UserLoginTime", ConnectToDataBase.MyAmdDBConnection);
                sqlDataAdapter.SelectCommand.CommandType = CommandType.StoredProcedure;
                sqlDataAdapter.SelectCommand.Parameters.AddWithValue("@userID", uid);

                sqlDataAdapter.Fill(ds);
            }
            catch (Exception ex)
            {
                ex.ToString();
                //clsErrorLog.LogInfo(ex);
            }

            return ds;
        }
        public static bool InsertLoginTime(string uid, string ipAddress)
        {
            try
            {
                sqlCommand = new SqlCommand("Sp_Tbl_UserLoginTime_Insert", ConnectToDataBase.MyAmdDBConnection);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@userID", uid);
                sqlCommand.Parameters.AddWithValue("@IPAdress", ipAddress);

                ConnectToDataBase.MyAmdOpenConnection();
                int isSuccess = sqlCommand.ExecuteNonQuery();
                ConnectToDataBase.MyAmdCloseConnection();

                if (isSuccess > 0)
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return false;
        }
        public static DataTable ForgetPassword(ForgetPassword forget)
        {
            try
            {
                sqlCommand = new SqlCommand("CheckForgotPassword", ConnectToDataBase.MyAmdDBConnection);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@UID", forget.UserId);
                sqlCommand.Parameters.AddWithValue("@Email", forget.Email);
                sqlCommand.Parameters.AddWithValue("@Mobile", forget.Mobile);

                sqlDataAdapter = new SqlDataAdapter();
                sqlDataAdapter.SelectCommand = sqlCommand;
                dataSet = new DataSet();

                dataTable = new DataTable();
                sqlDataAdapter.Fill(dataSet, "agent_register");
                dataTable = dataSet.Tables["agent_register"];
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return dataTable;
        }
        public static DataTable GetLedgerDetail(AccountLedgerFilter filter)
        {
            try
            {
                sqlCommand = new SqlCommand("sp_GetLedgerDetail_NewDEC2021", ConnectToDataBase.MyAmdDBConnection);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Parameters.Add(new SqlParameter("@usertype", (!string.IsNullOrEmpty(filter.UserType) ? filter.UserType : "")));
                sqlCommand.Parameters.Add(new SqlParameter("@LoginID", (!string.IsNullOrEmpty(filter.Loginid) ? filter.Loginid : "")));
                sqlCommand.Parameters.Add(new SqlParameter("@FormDate", (!string.IsNullOrEmpty(filter.FromDate) ? filter.FromDate : "")));
                sqlCommand.Parameters.Add(new SqlParameter("@ToDate", (!string.IsNullOrEmpty(filter.ToDate) ? filter.ToDate : "")));
                sqlCommand.Parameters.Add(new SqlParameter("@AgentId", (!string.IsNullOrEmpty(filter.AgentId) ? filter.AgentId : "")));
                sqlCommand.Parameters.Add(new SqlParameter("@BookingType", (!string.IsNullOrEmpty(filter.BookingType) ? filter.BookingType : "")));
                sqlCommand.Parameters.Add(new SqlParameter("@SearchType", (!string.IsNullOrEmpty(filter.SearchType) ? filter.SearchType : "")));
                sqlCommand.Parameters.Add(new SqlParameter("@TransType", (!string.IsNullOrEmpty(filter.TransType) ? filter.TransType : "")));
                sqlCommand.Parameters.Add(new SqlParameter("@OrderNo", (!string.IsNullOrEmpty(filter.OrderId) ? filter.OrderId : "")));
                sqlCommand.Parameters.Add(new SqlParameter("@AirCode", (!string.IsNullOrEmpty(filter.AirLine) ? filter.AirLine : "")));
                sqlDataAdapter = new SqlDataAdapter();
                sqlDataAdapter.SelectCommand = sqlCommand;
                dataSet = new DataSet();
                sqlDataAdapter.Fill(dataSet, "GetLedger");
                dataTable = dataSet.Tables["GetLedger"];
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return dataTable;
        }

        public static DataTable GetExcelLedgerDetail(AccountLedgerFilter filter)
        {
            try
            {
                sqlCommand = new SqlCommand("sp_GetLedgerDetail_NewDEC2021ForExcel", ConnectToDataBase.MyAmdDBConnection);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Parameters.Add(new SqlParameter("@usertype", (!string.IsNullOrEmpty(filter.UserType) ? filter.UserType : "")));
                sqlCommand.Parameters.Add(new SqlParameter("@LoginID", (!string.IsNullOrEmpty(filter.Loginid) ? filter.Loginid : "")));
                sqlCommand.Parameters.Add(new SqlParameter("@FormDate", (!string.IsNullOrEmpty(filter.FromDate) ? filter.FromDate : "")));
                sqlCommand.Parameters.Add(new SqlParameter("@ToDate", (!string.IsNullOrEmpty(filter.ToDate) ? filter.ToDate : "")));
                sqlCommand.Parameters.Add(new SqlParameter("@AgentId", (!string.IsNullOrEmpty(filter.AgentId) ? filter.AgentId : "")));
                sqlCommand.Parameters.Add(new SqlParameter("@BookingType", (!string.IsNullOrEmpty(filter.BookingType) ? filter.BookingType : "")));
                sqlCommand.Parameters.Add(new SqlParameter("@SearchType", (!string.IsNullOrEmpty(filter.SearchType) ? filter.SearchType : "")));
                sqlCommand.Parameters.Add(new SqlParameter("@TransType", (!string.IsNullOrEmpty(filter.TransType) ? filter.TransType : "")));
                sqlCommand.Parameters.Add(new SqlParameter("@OrderNo", (!string.IsNullOrEmpty(filter.OrderId) ? filter.OrderId : "")));
                sqlCommand.Parameters.Add(new SqlParameter("@AirCode", (!string.IsNullOrEmpty(filter.AirLine) ? filter.AirLine : "")));
                sqlDataAdapter = new SqlDataAdapter();
                sqlDataAdapter.SelectCommand = sqlCommand;
                dataSet = new DataSet();
                sqlDataAdapter.Fill(dataSet, "GetLedger");
                dataTable = dataSet.Tables["GetLedger"];
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return dataTable;
        }
        public static DataSet GetInvoice(string orderid)
        {
            dataSet = new DataSet();
            try
            {
                sqlCommand = new SqlCommand("GetInvoiceDetails", ConnectToDataBase.MyAmdDBConnection);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Parameters.Add(new SqlParameter("@orderid", orderid));
                sqlDataAdapter = new SqlDataAdapter();
                sqlDataAdapter.SelectCommand = sqlCommand;
                sqlDataAdapter.Fill(dataSet, "GetInvoice");
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return dataSet;
        }
        public static DataTable GetStaffTransaction(AccountLedgerFilter filter)
        {
            try
            {
                sqlCommand = new SqlCommand("GetStaffTransaction", ConnectToDataBase.MyAmdDBConnection);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Parameters.Add(new SqlParameter("@UserType", (!string.IsNullOrEmpty(filter.UserType) ? filter.UserType : string.Empty)));
                sqlCommand.Parameters.Add(new SqlParameter("@StaffUserId", (!string.IsNullOrEmpty(filter.StaffUserId) ? filter.StaffUserId : string.Empty)));
                sqlCommand.Parameters.Add(new SqlParameter("@FormDate", (!string.IsNullOrEmpty(filter.FromDate) ? filter.FromDate : string.Empty)));
                sqlCommand.Parameters.Add(new SqlParameter("@ToDate", (!string.IsNullOrEmpty(filter.ToDate) ? filter.ToDate : string.Empty)));
                sqlCommand.Parameters.Add(new SqlParameter("@AgentId", (!string.IsNullOrEmpty(filter.Loginid) ? filter.Loginid : string.Empty)));
                sqlCommand.Parameters.Add(new SqlParameter("@Module", (!string.IsNullOrEmpty(filter.Module) ? filter.Module : string.Empty)));
                sqlCommand.Parameters.Add(new SqlParameter("@ServiceType", (!string.IsNullOrEmpty(filter.ServiceType) ? filter.ServiceType : string.Empty)));
                sqlCommand.Parameters.Add(new SqlParameter("@OrderId", (!string.IsNullOrEmpty(filter.OrderId) ? filter.OrderId : string.Empty)));
                sqlCommand.Parameters.Add(new SqlParameter("@SearchType", (!string.IsNullOrEmpty(filter.SearchType) ? filter.SearchType : string.Empty)));
                sqlDataAdapter = new SqlDataAdapter();
                sqlDataAdapter.SelectCommand = sqlCommand;
                dataSet = new DataSet();
                sqlDataAdapter.Fill(dataSet, "GetTransaction");
                dataTable = dataSet.Tables["GetTransaction"];
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return dataTable;
        }
        public static DataTable IntGetInvoice(AccountLedgerFilter filter)
        {
            try
            {
                sqlCommand = new SqlCommand("IntSelectInvoiceDetails", ConnectToDataBase.MyAmdDBConnection);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Parameters.Add(new SqlParameter("@usertype", (!string.IsNullOrEmpty(filter.UserType) ? filter.UserType : string.Empty)));
                sqlCommand.Parameters.Add(new SqlParameter("@LoginID", (!string.IsNullOrEmpty(filter.Loginid) ? filter.Loginid : string.Empty)));
                sqlCommand.Parameters.Add(new SqlParameter("@FormDate", (!string.IsNullOrEmpty(filter.FromDate) ? filter.FromDate : string.Empty)));
                sqlCommand.Parameters.Add(new SqlParameter("@ToDate", (!string.IsNullOrEmpty(filter.ToDate) ? filter.ToDate : string.Empty)));
                sqlCommand.Parameters.Add(new SqlParameter("@OderId", (!string.IsNullOrEmpty(filter.OrderId) ? filter.OrderId : string.Empty)));
                sqlCommand.Parameters.Add(new SqlParameter("@PNR", (!string.IsNullOrEmpty(filter.PNR) ? filter.PNR : string.Empty)));
                sqlCommand.Parameters.Add(new SqlParameter("@TicketNo", (!string.IsNullOrEmpty(filter.TicketNo) ? filter.TicketNo : string.Empty)));
                sqlCommand.Parameters.Add(new SqlParameter("@AgentId", (!string.IsNullOrEmpty(filter.AgentId) ? filter.AgentId : string.Empty)));
                sqlCommand.Parameters.Add(new SqlParameter("@Airline", (!string.IsNullOrEmpty(filter.AirlinePNR) ? filter.AirlinePNR : string.Empty)));
                sqlCommand.Parameters.Add(new SqlParameter("@Trip", (!string.IsNullOrEmpty(filter.Trip) ? filter.Trip : string.Empty)));
                sqlCommand.Parameters.Add(new SqlParameter("@ProjectID", (!string.IsNullOrEmpty(filter.ProjectID) ? filter.ProjectID : string.Empty)));
                sqlDataAdapter = new SqlDataAdapter();
                sqlDataAdapter.SelectCommand = sqlCommand;
                dataSet = new DataSet();
                sqlDataAdapter.Fill(dataSet, "SelectInvoice");
                dataTable = dataSet.Tables["SelectInvoice"];
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return dataTable;
        }

        public static DataTable GetCreditlimithistory(AgentProfileFilter filter)
        {
            try
            {
                sqlCommand = new SqlCommand("GET_CREDITLIMIT_HISTORY_DISTR", ConnectToDataBase.MyAmdDBConnection);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Parameters.Add(new SqlParameter("@AgentId", (!string.IsNullOrEmpty(filter.AgentId) ? filter.AgentId : string.Empty)));
                sqlCommand.Parameters.Add(new SqlParameter("@DistrId", (!string.IsNullOrEmpty(filter.DistrId) ? filter.DistrId : string.Empty)));
                sqlCommand.Parameters.Add(new SqlParameter("@FormDate", (!string.IsNullOrEmpty(filter.FromDate) ? filter.FromDate : string.Empty)));
                sqlCommand.Parameters.Add(new SqlParameter("@ToDate", (!string.IsNullOrEmpty(filter.ToDate) ? filter.ToDate : string.Empty)));                
                sqlDataAdapter = new SqlDataAdapter();
                sqlDataAdapter.SelectCommand = sqlCommand;
                dataSet = new DataSet();
                sqlDataAdapter.Fill(dataSet, "CREDITLIMIT_HISTORY");
                dataTable = dataSet.Tables["CREDITLIMIT_HISTORY"];
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return dataTable;
        }
        public static bool UpdateAgentProfileDetails(AgentUpdateProfile profile)
        {
            try
            {
                sqlCommand = new SqlCommand("UpdateAgentProfile", ConnectToDataBase.MyAmdDBConnection);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@Type", profile.Type);
                sqlCommand.Parameters.AddWithValue("@UID", profile.AgentId);
                sqlCommand.Parameters.AddWithValue("@pwd", profile.Password);
                sqlCommand.Parameters.AddWithValue("@AEmail", profile.AgentEmail);
                sqlCommand.Parameters.AddWithValue("@Landline", profile.LandLine);
                sqlCommand.Parameters.AddWithValue("@Fax", profile.Fax);
                sqlCommand.Parameters.AddWithValue("@Address", profile.Address);
                sqlCommand.Parameters.AddWithValue("@City", profile.City.Trim().ToLower() != "0" ? profile.City : "");
                sqlCommand.Parameters.AddWithValue("@State", profile.State.Trim().ToLower() != "-- select state --" ? profile.State : "");
                sqlCommand.Parameters.AddWithValue("@Country", profile.Country);
                sqlCommand.Parameters.AddWithValue("@StateCode", !string.IsNullOrEmpty(profile.StateCode) ? profile.StateCode : "");

                //GST
                sqlCommand.Parameters.AddWithValue("@GSTNO", !string.IsNullOrEmpty(profile.GSTNO) ? profile.GSTNO : "");
                sqlCommand.Parameters.AddWithValue("@GSTCompanyName", !string.IsNullOrEmpty(profile.GSTCompanyName) ? profile.GSTCompanyName : "");
                sqlCommand.Parameters.AddWithValue("@GSTCompanyAddress", !string.IsNullOrEmpty(profile.GSTCompanyAddress) ? profile.GSTCompanyAddress : "");
                sqlCommand.Parameters.AddWithValue("@GSTPhoneNo", !string.IsNullOrEmpty(profile.GSTPhoneNo) ? profile.GSTPhoneNo : "");
                sqlCommand.Parameters.AddWithValue("@GSTEmail", !string.IsNullOrEmpty(profile.GSTEmail) ? profile.GSTEmail : "");
                sqlCommand.Parameters.AddWithValue("@IsGSTApply", profile.IsGSTApply);
                sqlCommand.Parameters.AddWithValue("@GSTRemark", !string.IsNullOrEmpty(profile.GSTRemark) ? profile.GSTRemark : "");
                sqlCommand.Parameters.AddWithValue("@GST_Pincode", !string.IsNullOrEmpty(profile.GstPinCode) ? profile.GstPinCode : "");

                ConnectToDataBase.MyAmdOpenConnection();
                int isSuccess = sqlCommand.ExecuteNonQuery();
                ConnectToDataBase.MyAmdCloseConnection();

                if (isSuccess > 0)
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return false;
        }

        public static decimal CheckDistrBalance(string AgentId, decimal Amount)
        {
            int isSuccess = 0;
            try
            {
                sqlCommand = new SqlCommand("SP_CHECKDISTRBALANCE", ConnectToDataBase.MyAmdDBConnection);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@AGENTID", AgentId);
                sqlCommand.Parameters.AddWithValue("@TAAMOUNT", Amount);
                ConnectToDataBase.MyAmdOpenConnection();
                isSuccess = sqlCommand.ExecuteNonQuery();
                ConnectToDataBase.MyAmdCloseConnection();
                if (isSuccess > 0)
                {
                    return isSuccess;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return isSuccess;
        }
        public static DataTable StateList(string country)
        {
            try
            {
                sqlCommand = new SqlCommand("select STATEID as Code,STATE as Name from Tbl_STATE where COUNTRY='" + country + "' order by STATE", ConnectToDataBase.MyAmdDBConnection);

                sqlDataAdapter = new SqlDataAdapter();
                sqlDataAdapter.SelectCommand = sqlCommand;
                dataSet = new DataSet();
                sqlDataAdapter.Fill(dataSet, "Tbl_STATE");
                dataTable = dataSet.Tables["Tbl_STATE"];
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return dataTable;
        }
        public static DataTable CityList(string stateId)
        {
            try
            {
                sqlCommand = new SqlCommand("select CITY, STATEID from TBL_CITY where STATEID='" + stateId + "' order by CITY", ConnectToDataBase.MyAmdDBConnection);

                sqlDataAdapter = new SqlDataAdapter();
                sqlDataAdapter.SelectCommand = sqlCommand;
                dataSet = new DataSet();
                sqlDataAdapter.Fill(dataSet, "TBL_CITY");
                dataTable = dataSet.Tables["TBL_CITY"];
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return dataTable;
        }
        public static DataSet AgentAmountDebitCreditByDistrWithCreditLimit(string AgentID, string DistrId, string InvoiceNo, string PnrNo, string TicketNo, string TicketingCarrier, string YatraAccountID, string AccountID, string ExecutiveID, string IPAddress, double Amount, string BookingType, string Remark, int PaxId, string ProjectId, string BookedBy, string BillNo, string ActionType, string UploadType)
        {
            DataSet ds = new DataSet();
            try
            {
                sqlDataAdapter = new SqlDataAdapter("AgentAmountDebitCreditByDistrWithCreditLimit", ConnectToDataBase.MyAmdDBConnection);
                sqlDataAdapter.SelectCommand.CommandType = CommandType.StoredProcedure;
                sqlDataAdapter.SelectCommand.Parameters.AddWithValue("@ActionType", ActionType);
                sqlDataAdapter.SelectCommand.Parameters.AddWithValue("@InvoiceNo", InvoiceNo);
                sqlDataAdapter.SelectCommand.Parameters.AddWithValue("@PnrNo", PnrNo);
                sqlDataAdapter.SelectCommand.Parameters.AddWithValue("@TicketNo", TicketNo);
                sqlDataAdapter.SelectCommand.Parameters.AddWithValue("@TicketingCarrier", TicketingCarrier);
                sqlDataAdapter.SelectCommand.Parameters.AddWithValue("@YatraAccountID", YatraAccountID);
                sqlDataAdapter.SelectCommand.Parameters.AddWithValue("@AccountID", AccountID);
                sqlDataAdapter.SelectCommand.Parameters.AddWithValue("@ExecutiveID", ExecutiveID);
                sqlDataAdapter.SelectCommand.Parameters.AddWithValue("@IPAddress", IPAddress);
                sqlDataAdapter.SelectCommand.Parameters.AddWithValue("@BookingType", BookingType);
                sqlDataAdapter.SelectCommand.Parameters.AddWithValue("@Remark", Remark);
                sqlDataAdapter.SelectCommand.Parameters.AddWithValue("@PaxId", PaxId);
                sqlDataAdapter.SelectCommand.Parameters.AddWithValue("@ProjectId", ProjectId);
                sqlDataAdapter.SelectCommand.Parameters.AddWithValue("@BookedBy", BookedBy);
                sqlDataAdapter.SelectCommand.Parameters.AddWithValue("@BillNo", BillNo);
                sqlDataAdapter.SelectCommand.Parameters.AddWithValue("@UserId", AgentID);
                sqlDataAdapter.SelectCommand.Parameters.AddWithValue("@DistrId", DistrId);
                sqlDataAdapter.SelectCommand.Parameters.AddWithValue("@Amount", Amount);
                sqlDataAdapter.SelectCommand.Parameters.AddWithValue("@UploadType", UploadType);
                sqlDataAdapter.Fill(ds);
            }
            catch (Exception ex)
            {
                //clsErrorLog.LogInfo(ex);
                ex.ToString();
            }

            return ds;
        }
        public static int insertUploadDetails(string AgentID, string AgencyName, string AccountID, string IPAddress, double Debit, double Credit, string Remark, string UploadType, double LastAval_Balance, double CurrentAval_Balance, string YtrRcptNo)
        {
            int isSuccess = 0;
            try
            {
                sqlCommand = new SqlCommand("InsertUploadDetails", ConnectToDataBase.MyAmdDBConnection);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@agentid", AgentID);
                sqlCommand.Parameters.AddWithValue("@agencyname", AgencyName);
                sqlCommand.Parameters.AddWithValue("@accid", AccountID);
                sqlCommand.Parameters.AddWithValue("@IPaddress", IPAddress);
                sqlCommand.Parameters.AddWithValue("@debit", Debit);
                sqlCommand.Parameters.AddWithValue("@credit", Credit);
                sqlCommand.Parameters.AddWithValue("@remark", Remark);
                sqlCommand.Parameters.AddWithValue("@Uploadtype", UploadType);
                sqlCommand.Parameters.AddWithValue("@lastavlbal", LastAval_Balance);
                sqlCommand.Parameters.AddWithValue("@curravlbal", CurrentAval_Balance);
                sqlCommand.Parameters.AddWithValue("@YtrRcptNo", YtrRcptNo);
                ConnectToDataBase.MyAmdOpenConnection();
                isSuccess = sqlCommand.ExecuteNonQuery();
                ConnectToDataBase.MyAmdCloseConnection();

                if (isSuccess > 0)
                {
                    return isSuccess;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return isSuccess;
        }
        public static DataSet GetUploadTypeByType(string Type)
        {
            DataSet ds = new DataSet();
            try
            {
                sqlDataAdapter = new SqlDataAdapter("GetUploadTypeByType", ConnectToDataBase.MyAmdDBConnection);
                sqlDataAdapter.SelectCommand.CommandType = CommandType.StoredProcedure;
                sqlDataAdapter.SelectCommand.Parameters.AddWithValue("@Type", Type);
                sqlDataAdapter.Fill(ds);
            }
            catch (Exception ex)
            {
                //clsErrorLog.LogInfo(ex);
                ex.ToString();
            }

            return ds;
        }
        public static bool UpdateAgentByDistr(AgencyDetails details)
        {
            try
            {
                sqlCommand = new SqlCommand("UpdateAgentByDistr", ConnectToDataBase.MyAmdDBConnection);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@AgentStatus", details.Agent_Status);
                sqlCommand.Parameters.AddWithValue("@AgentTkTStatus", details.Online_Tkt);
                sqlCommand.Parameters.AddWithValue("@UserID", details.User_Id);
                ConnectToDataBase.MyAmdOpenConnection();
                int isSuccess = sqlCommand.ExecuteNonQuery();
                ConnectToDataBase.MyAmdCloseConnection();

                if (isSuccess > 0)
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return false;
        }
        public static DataTable GetSalesRef()
        {
            DataTable ds = new DataTable();
            try
            {
                sqlDataAdapter = new SqlDataAdapter("GetSalesRef", ConnectToDataBase.MyAmdDBConnection);
                sqlDataAdapter.SelectCommand.CommandType = CommandType.StoredProcedure;
                sqlDataAdapter.Fill(ds);
            }
            catch (Exception ex)
            {
                //clsErrorLog.LogInfo(ex);
                ex.ToString();
            }

            return ds;
        }
        public static DataTable SPGETSTATECITY()
        {
            DataTable ds = new DataTable();
            try
            {
                sqlDataAdapter = new SqlDataAdapter("SP_GET_STATECITY", ConnectToDataBase.MyAmdDBConnection);
                sqlDataAdapter.SelectCommand.CommandType = CommandType.StoredProcedure;
                sqlDataAdapter.SelectCommand.Parameters.AddWithValue("@INPUT", "india");
                sqlDataAdapter.SelectCommand.Parameters.AddWithValue("@SEARCH", "COUNTRY");
                sqlDataAdapter.Fill(ds);
            }
            catch (Exception ex)
            {
                //clsErrorLog.LogInfo(ex);
                ex.ToString();
            }

            return ds;
        }
        public static int insertAgencyRegistrationDetails(AgencyDetails details)
        {
            int isSuccess = 0;
            try
            {
                sqlCommand = new SqlCommand("InsertRegistrationDetails", ConnectToDataBase.MyAmdDBConnection);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@user_id", details.User_Id);
                sqlCommand.Parameters.AddWithValue("@Title", details.Title);
                sqlCommand.Parameters.AddWithValue("@Fname", details.FName);
                sqlCommand.Parameters.AddWithValue("@Lname", details.LName);
                sqlCommand.Parameters.AddWithValue("@Address", details.Address);
                sqlCommand.Parameters.AddWithValue("@city", details.City);
                sqlCommand.Parameters.AddWithValue("@state", details.State);
                sqlCommand.Parameters.AddWithValue("@country", details.Country);
                sqlCommand.Parameters.AddWithValue("@Area", details.Area);
                sqlCommand.Parameters.AddWithValue("@zipcode", details.ZipCode);
                sqlCommand.Parameters.AddWithValue("@Phone", details.Phone);
                sqlCommand.Parameters.AddWithValue("@Mobile", details.Mobile);
                sqlCommand.Parameters.AddWithValue("@email", details.Email);
                sqlCommand.Parameters.AddWithValue("@Alt_Email","0");
                sqlCommand.Parameters.AddWithValue("@Fax_no", "0");
                sqlCommand.Parameters.AddWithValue("@Agency_Name", details.Agency_Name);
                sqlCommand.Parameters.AddWithValue("@Website", "NA");
                sqlCommand.Parameters.AddWithValue("@NameOnPan", details.NamePanCard);
                sqlCommand.Parameters.AddWithValue("@PanNo",details.PanNo);
                sqlCommand.Parameters.AddWithValue("@Status", "TA");
                sqlCommand.Parameters.AddWithValue("@Stax_no", "NA");
                sqlCommand.Parameters.AddWithValue("@Remark", "NA");
                sqlCommand.Parameters.AddWithValue("@Sec_Qes", "NA");
                sqlCommand.Parameters.AddWithValue("@Sec_Ans", "NA");
                sqlCommand.Parameters.AddWithValue("@PWD", details.Password);
                sqlCommand.Parameters.AddWithValue("@Agent_Type",details.Agent_Type);
                sqlCommand.Parameters.AddWithValue("@Distr", details.Distr);
                sqlCommand.Parameters.AddWithValue("@SalesExecID", details.SalesExecID);
                sqlCommand.Parameters.AddWithValue("@ag_logo", "NA");
                ConnectToDataBase.MyAmdOpenConnection();
                isSuccess = sqlCommand.ExecuteNonQuery();
                ConnectToDataBase.MyAmdCloseConnection();

                if (isSuccess > 0)
                {
                    return isSuccess;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return isSuccess;
        }

    }
}
