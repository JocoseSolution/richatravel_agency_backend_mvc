﻿using ModelLibrary;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace DataBaseLibrary.Flight
{
    public static class FlightDataBase
    {
        private static SqlCommand sqlCommand { get; set; }
        private static SqlDataAdapter sqlDataAdapter { get; set; }
        private static DataSet dataSet { get; set; }
        private static DataTable dataTable { get; set; }
        public static DataTable GetBookingDetails(string orderid)
        {
            try
            {
                sqlCommand = new SqlCommand("GetBookingDetails", ConnectToDataBase.MyAmdDBConnection);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Parameters.Add(new SqlParameter("@ordid", orderid));
                sqlDataAdapter = new SqlDataAdapter();
                sqlDataAdapter.SelectCommand = sqlCommand;
                dataSet = new DataSet();
                sqlDataAdapter.Fill(dataSet, "BookingDetail");
                dataTable = dataSet.Tables["BookingDetail"];
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return dataTable;
        }
        public static DataTable GetRefundCreditNode(string refundid, string trip, string status)
        {
            try
            {
                sqlCommand = new SqlCommand("GetCreditNodeDetails", ConnectToDataBase.MyAmdDBConnection);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Parameters.Add(new SqlParameter("@RefundID", refundid));
                sqlCommand.Parameters.Add(new SqlParameter("@Trip", trip));
                sqlCommand.Parameters.Add(new SqlParameter("@Status", status));
                sqlDataAdapter = new SqlDataAdapter();
                sqlDataAdapter.SelectCommand = sqlCommand;
                dataSet = new DataSet();
                sqlDataAdapter.Fill(dataSet, "CancellationIntl");
                dataTable = dataSet.Tables["CancellationIntl"];
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return dataTable;
        }
        public static DataTable GetCompanyAddress(string addressType)
        {
            try
            {
                sqlCommand = new SqlCommand("SP_GETADDRESS", ConnectToDataBase.MyAmdDBConnection);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Parameters.Add(new SqlParameter("@TYPE", addressType));
                sqlDataAdapter = new SqlDataAdapter();
                sqlDataAdapter.SelectCommand = sqlCommand;
                dataSet = new DataSet();
                sqlDataAdapter.Fill(dataSet, "GETADDRESS");
                dataTable = dataSet.Tables["GETADDRESS"];
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return dataTable;
        }
        public static DataTable GetPayementProcess(string orderid)
        {
            try
            {
                sqlCommand = new SqlCommand("GetPayementProcess", ConnectToDataBase.MyAmdDBConnection);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Parameters.Add(new SqlParameter("@ordid", orderid));
                sqlDataAdapter = new SqlDataAdapter();
                sqlDataAdapter.SelectCommand = sqlCommand;
                dataSet = new DataSet();
                sqlDataAdapter.Fill(dataSet, "BookingDetail");
                dataTable = dataSet.Tables["BookingDetail"];
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return dataTable;
        }

        public static DataTable GetInvoiceDetails(string orderid)
        {
            try
            {
                sqlCommand = new SqlCommand("GetInvoiceDetails", ConnectToDataBase.MyAmdDBConnection);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Parameters.Add(new SqlParameter("@orderid", orderid));
                sqlDataAdapter = new SqlDataAdapter();
                sqlDataAdapter.SelectCommand = sqlCommand;
                dataSet = new DataSet();
                sqlDataAdapter.Fill(dataSet, "GetInvoiceDetails");
                dataTable = dataSet.Tables["GetInvoiceDetails"];
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return dataTable;
        }
        public static DataTable GetTicektReport(FlightTicketFilter filter)
        {
            try
            {
                sqlCommand = new SqlCommand("USP_GetTicketDetail_Intl", ConnectToDataBase.MyAmdDBConnection);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Parameters.Add(new SqlParameter("@usertype", (!string.IsNullOrEmpty(filter.UserType) ? filter.UserType : "")));
                sqlCommand.Parameters.Add(new SqlParameter("@LoginID", (!string.IsNullOrEmpty(filter.Loginid) ? filter.Loginid : "")));
                sqlCommand.Parameters.Add(new SqlParameter("@FormDate", (!string.IsNullOrEmpty(filter.FromDate) ? filter.FromDate : "")));
                sqlCommand.Parameters.Add(new SqlParameter("@ToDate", (!string.IsNullOrEmpty(filter.ToDate) ? filter.ToDate : "")));
                sqlCommand.Parameters.Add(new SqlParameter("@OderId", (!string.IsNullOrEmpty(filter.OrderId) ? filter.OrderId : "")));
                sqlCommand.Parameters.Add(new SqlParameter("@PNR", (!string.IsNullOrEmpty(filter.PNR) ? filter.PNR : "")));
                sqlCommand.Parameters.Add(new SqlParameter("@AirlinePNR", (!string.IsNullOrEmpty(filter.AirlinePNR) ? filter.AirlinePNR : "")));
                sqlCommand.Parameters.Add(new SqlParameter("@PaxName", (!string.IsNullOrEmpty(filter.PaxName) ? filter.PaxName : "")));
                sqlCommand.Parameters.Add(new SqlParameter("@TicketNo", (!string.IsNullOrEmpty(filter.TicketNo) ? filter.TicketNo : "")));
                sqlCommand.Parameters.Add(new SqlParameter("@AgentId", (!string.IsNullOrEmpty(filter.AgentId) ? filter.AgentId : "")));
                sqlCommand.Parameters.Add(new SqlParameter("@Trip", (!string.IsNullOrEmpty(filter.Trip) ? filter.Trip : "")));
                sqlCommand.Parameters.Add(new SqlParameter("@Status", (!string.IsNullOrEmpty(filter.Status) ? filter.Status : "")));
                sqlDataAdapter = new SqlDataAdapter();
                sqlDataAdapter.SelectCommand = sqlCommand;
                dataSet = new DataSet();
                sqlDataAdapter.Fill(dataSet, "GetTicketDetail_Intl");
                dataTable = dataSet.Tables["GetTicketDetail_Intl"];
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return dataTable;
        }

        public static DataTable GetRaliReport(RailModelFilter filter)
        {
            try
            {
                sqlCommand = new SqlCommand("SP_GETRailUploadDataByRailway", ConnectToDataBase.MyAmdDBConnection);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Parameters.Add(new SqlParameter("@AgencyId", (!string.IsNullOrEmpty(filter.Loginid) ? filter.Loginid : "")));
                sqlCommand.Parameters.Add(new SqlParameter("@FromDate", (!string.IsNullOrEmpty(filter.FromDate) ? filter.FromDate : "")));
                sqlCommand.Parameters.Add(new SqlParameter("@ToDate", (!string.IsNullOrEmpty(filter.ToDate) ? filter.ToDate : "")));
                sqlCommand.Parameters.Add(new SqlParameter("@Type", "agency"));
                sqlDataAdapter = new SqlDataAdapter();
                sqlDataAdapter.SelectCommand = sqlCommand;
                dataSet = new DataSet();
                sqlDataAdapter.Fill(dataSet, "GETRailUploadData");
                dataTable = dataSet.Tables["GETRailUploadData"];
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return dataTable;
        }
        public static DataTable GetRefundReport(FlightTicketFilter filter)
        {
            try
            {
                sqlCommand = new SqlCommand("USP_GetCancellationDetail_Intl", ConnectToDataBase.MyAmdDBConnection);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Parameters.Add(new SqlParameter("@UserType", (!string.IsNullOrEmpty(filter.UserType) ? filter.UserType : string.Empty)));
                sqlCommand.Parameters.Add(new SqlParameter("@UserID", (!string.IsNullOrEmpty(filter.Loginid) ? filter.Loginid : string.Empty)));
                sqlCommand.Parameters.Add(new SqlParameter("@FormDate", (!string.IsNullOrEmpty(filter.FromDate) ? filter.FromDate : string.Empty)));
                sqlCommand.Parameters.Add(new SqlParameter("@ToDate", (!string.IsNullOrEmpty(filter.ToDate) ? filter.ToDate : string.Empty)));
                sqlCommand.Parameters.Add(new SqlParameter("@OderId", (!string.IsNullOrEmpty(filter.OrderId) ? filter.OrderId : string.Empty)));
                sqlCommand.Parameters.Add(new SqlParameter("@PNR", (!string.IsNullOrEmpty(filter.PNR) ? filter.PNR : string.Empty)));
                sqlCommand.Parameters.Add(new SqlParameter("@Airline", (!string.IsNullOrEmpty(filter.AirlinePNR) ? filter.AirlinePNR : string.Empty)));
                sqlCommand.Parameters.Add(new SqlParameter("@PaxName", (!string.IsNullOrEmpty(filter.PaxName) ? filter.PaxName : string.Empty)));
                sqlCommand.Parameters.Add(new SqlParameter("@TicketNo   ", (!string.IsNullOrEmpty(filter.TicketNo) ? filter.TicketNo : string.Empty)));
                sqlCommand.Parameters.Add(new SqlParameter("@AgentId", (!string.IsNullOrEmpty(filter.AgentId) ? filter.AgentId : string.Empty)));
                sqlCommand.Parameters.Add(new SqlParameter("@Trip", (!string.IsNullOrEmpty(filter.Trip) ? filter.Trip : string.Empty)));
                sqlCommand.Parameters.Add(new SqlParameter("@Status", (!string.IsNullOrEmpty(filter.Status) ? filter.Status : string.Empty)));
                sqlCommand.Parameters.Add(new SqlParameter("@ExecID", (!string.IsNullOrEmpty(filter.ExecID) ? filter.ExecID : string.Empty)));
                sqlCommand.Parameters.Add(new SqlParameter("@PartnerName", (!string.IsNullOrEmpty(filter.PartnerName) ? filter.PartnerName : string.Empty)));
                sqlCommand.Parameters.Add(new SqlParameter("@PaymentMode", (!string.IsNullOrEmpty(filter.PaymentMode) ? filter.PaymentMode : string.Empty)));
                sqlDataAdapter = new SqlDataAdapter();
                sqlDataAdapter.SelectCommand = sqlCommand;
                dataSet = new DataSet();
                sqlDataAdapter.Fill(dataSet, "GetRefundDetail_Intl");
                dataTable = dataSet.Tables["GetRefundDetail_Intl"];
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return dataTable;
        }
        public static DataTable GetHoldPNRReport(FlightTicketFilter filter)
        {
            try
            {
                sqlCommand = new SqlCommand("SP_GETHOLDPNRREPORT", ConnectToDataBase.MyAmdDBConnection);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Parameters.Add(new SqlParameter("@USERTYPE", (!string.IsNullOrEmpty(filter.UserType) ? filter.UserType : string.Empty)));
                sqlCommand.Parameters.Add(new SqlParameter("@LOGINID", (!string.IsNullOrEmpty(filter.Loginid) ? filter.Loginid : string.Empty)));
                sqlCommand.Parameters.Add(new SqlParameter("@FormDate", (!string.IsNullOrEmpty(filter.FromDate) ? filter.FromDate : string.Empty)));
                sqlCommand.Parameters.Add(new SqlParameter("@ToDate", (!string.IsNullOrEmpty(filter.ToDate) ? filter.ToDate : string.Empty)));
                sqlCommand.Parameters.Add(new SqlParameter("@OderId", (!string.IsNullOrEmpty(filter.OrderId) ? filter.OrderId : string.Empty)));
                sqlCommand.Parameters.Add(new SqlParameter("@PNR", (!string.IsNullOrEmpty(filter.PNR) ? filter.PNR : string.Empty)));
                sqlCommand.Parameters.Add(new SqlParameter("@Airline", (!string.IsNullOrEmpty(filter.AirlinePNR) ? filter.AirlinePNR : string.Empty)));
                sqlCommand.Parameters.Add(new SqlParameter("@PaxName", (!string.IsNullOrEmpty(filter.PaxName) ? filter.PaxName : string.Empty)));
                sqlCommand.Parameters.Add(new SqlParameter("@TicketNo   ", (!string.IsNullOrEmpty(filter.TicketNo) ? filter.TicketNo : string.Empty)));
                sqlCommand.Parameters.Add(new SqlParameter("@AgentId", (!string.IsNullOrEmpty(filter.AgentId) ? filter.AgentId : string.Empty)));
                sqlCommand.Parameters.Add(new SqlParameter("@TRIP", (!string.IsNullOrEmpty(filter.Trip) ? filter.Trip : string.Empty)));
                sqlCommand.Parameters.Add(new SqlParameter("@Action", (!string.IsNullOrEmpty(filter.Actionby) ? filter.Actionby : string.Empty)));
                sqlCommand.Parameters.Add(new SqlParameter("@execid", (!string.IsNullOrEmpty(filter.ExecID) ? filter.ExecID : string.Empty)));
                sqlCommand.Parameters.Add(new SqlParameter("@distrid", (!string.IsNullOrEmpty(filter.distrid) ? filter.distrid : string.Empty)));
                sqlDataAdapter = new SqlDataAdapter();
                sqlDataAdapter.SelectCommand = sqlCommand;
                dataSet = new DataSet();
                sqlDataAdapter.Fill(dataSet, "GETHOLDPNRREPORT");
                dataTable = dataSet.Tables["GETHOLDPNRREPORT"];
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return dataTable;
        }
        public static DataTable GetSelectedFltDtls_Gal(string orderid, string agentid)
        {
            try
            {
                sqlCommand = new SqlCommand("GetSelectedFltDtls_Gal", ConnectToDataBase.MyAmdDBConnection);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Parameters.Add(new SqlParameter("@TrackId", orderid));
                sqlCommand.Parameters.Add(new SqlParameter("@UserId", agentid));
                sqlDataAdapter = new SqlDataAdapter();
                sqlDataAdapter.SelectCommand = sqlCommand;
                dataSet = new DataSet();
                sqlDataAdapter.Fill(dataSet, "GetSelectedFltDtls");
                dataTable = dataSet.Tables["GetSelectedFltDtls"];
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return dataTable;
        }
        public static DataTable Get_MEAL_BAG_FareDetails(string orderid, string transid)
        {
            try
            {
                sqlCommand = new SqlCommand("SP_SELECT_MBAG_DETAILS", ConnectToDataBase.MyAmdDBConnection);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Parameters.Add(new SqlParameter("@OID", orderid));
                sqlCommand.Parameters.Add(new SqlParameter("@paxid", transid));
                sqlDataAdapter = new SqlDataAdapter();
                sqlDataAdapter.SelectCommand = sqlCommand;
                dataSet = new DataSet();
                sqlDataAdapter.Fill(dataSet, "SP_SELECT_MBAG");
                dataTable = dataSet.Tables["SP_SELECT_MBAG"];
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return dataTable;
        }
        public static DataTable GetBaggageInformation(string Trip, string VS, bool IsBagFare)
        {
            try
            {
                sqlCommand = new SqlCommand("SP_GETBAGGAGE_INFO", ConnectToDataBase.MyAmdDBConnection);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Parameters.Add(new SqlParameter("@TRIP", Trip));
                sqlCommand.Parameters.Add(new SqlParameter("@AIRLINE", VS));
                sqlCommand.Parameters.Add(new SqlParameter("@IsBagFare", IsBagFare));
                sqlDataAdapter = new SqlDataAdapter();
                sqlDataAdapter.SelectCommand = sqlCommand;
                dataSet = new DataSet();
                sqlDataAdapter.Fill(dataSet, "GETBAGGAGE");
                dataTable = dataSet.Tables["GETBAGGAGE"];
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return dataTable;
        }
        public static DataTable GetProxyBookingReport(FlightTicketFilter filter)
        {
            try
            {
                sqlCommand = new SqlCommand("GetProxyBookingReport", ConnectToDataBase.MyAmdDBConnection);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Parameters.Add(new SqlParameter("@usertype", (!string.IsNullOrEmpty(filter.UserType) ? filter.UserType : string.Empty)));
                sqlCommand.Parameters.Add(new SqlParameter("@LoginID", (!string.IsNullOrEmpty(filter.Loginid) ? filter.Loginid : string.Empty)));
                sqlCommand.Parameters.Add(new SqlParameter("@FormDate", (!string.IsNullOrEmpty(filter.FromDate) ? filter.FromDate : string.Empty)));
                sqlCommand.Parameters.Add(new SqlParameter("@ToDate", (!string.IsNullOrEmpty(filter.ToDate) ? filter.ToDate : string.Empty)));
                sqlCommand.Parameters.Add(new SqlParameter("@AgentID", (!string.IsNullOrEmpty(filter.AgentId) ? filter.AgentId : string.Empty)));
                sqlCommand.Parameters.Add(new SqlParameter("@ExecID", (!string.IsNullOrEmpty(filter.ExecID) ? filter.ExecID : string.Empty)));
                sqlCommand.Parameters.Add(new SqlParameter("@Status", (!string.IsNullOrEmpty(filter.Status) ? filter.Status : string.Empty)));
                sqlCommand.Parameters.Add(new SqlParameter("@ProxyTrip", (!string.IsNullOrEmpty(filter.ExecID) ? filter.ExecID : string.Empty)));
                sqlDataAdapter = new SqlDataAdapter();
                sqlDataAdapter.SelectCommand = sqlCommand;
                dataSet = new DataSet();
                sqlDataAdapter.Fill(dataSet, "GetProxyBooking");
                dataTable = dataSet.Tables["GetProxyBooking"];
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return dataTable;
        }
        public static DataTable GetReIssueDetail(FlightTicketFilter filter)
        {
            try
            {
                sqlCommand = new SqlCommand("USP_GetReIssueDetail_Intl", ConnectToDataBase.MyAmdDBConnection);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Parameters.Add(new SqlParameter("@UserType", (!string.IsNullOrEmpty(filter.UserType) ? filter.UserType : string.Empty)));
                sqlCommand.Parameters.Add(new SqlParameter("@UserID", (!string.IsNullOrEmpty(filter.Loginid) ? filter.Loginid : string.Empty)));
                sqlCommand.Parameters.Add(new SqlParameter("@FormDate", (!string.IsNullOrEmpty(filter.FromDate) ? filter.FromDate : string.Empty)));
                sqlCommand.Parameters.Add(new SqlParameter("@ToDate", (!string.IsNullOrEmpty(filter.ToDate) ? filter.ToDate : string.Empty)));
                sqlCommand.Parameters.Add(new SqlParameter("@OderId", (!string.IsNullOrEmpty(filter.OrderId) ? filter.OrderId : string.Empty)));
                sqlCommand.Parameters.Add(new SqlParameter("@PNR", (!string.IsNullOrEmpty(filter.PNR) ? filter.PNR : string.Empty)));
                sqlCommand.Parameters.Add(new SqlParameter("@Airline", (!string.IsNullOrEmpty(filter.AirlinePNR) ? filter.AirlinePNR : string.Empty)));
                sqlCommand.Parameters.Add(new SqlParameter("@PaxName", (!string.IsNullOrEmpty(filter.PaxName) ? filter.PaxName : string.Empty)));
                sqlCommand.Parameters.Add(new SqlParameter("@TicketNo   ", (!string.IsNullOrEmpty(filter.TicketNo) ? filter.TicketNo : string.Empty)));
                sqlCommand.Parameters.Add(new SqlParameter("@AgentId", (!string.IsNullOrEmpty(filter.AgentId) ? filter.AgentId : string.Empty)));
                sqlCommand.Parameters.Add(new SqlParameter("@Trip", (!string.IsNullOrEmpty(filter.Trip) ? filter.Trip : string.Empty)));
                sqlCommand.Parameters.Add(new SqlParameter("@Status", (!string.IsNullOrEmpty(filter.Status) ? filter.Status : string.Empty)));
                sqlCommand.Parameters.Add(new SqlParameter("@ExecID", (!string.IsNullOrEmpty(filter.ExecID) ? filter.ExecID : string.Empty)));
                sqlDataAdapter = new SqlDataAdapter();
                sqlDataAdapter.SelectCommand = sqlCommand;
                dataSet = new DataSet();
                sqlDataAdapter.Fill(dataSet, "GetReIssueDetail_Intl");
                dataTable = dataSet.Tables["GetReIssueDetail_Intl"];
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return dataTable;
        }
        public static DataTable GetTicketStatusReport(FlightTicketFilter filter)
        {
            try
            {
                sqlCommand = new SqlCommand("USP_GetTicketDetail_IntlStatus", ConnectToDataBase.MyAmdDBConnection);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Parameters.Add(new SqlParameter("@UserType", (!string.IsNullOrEmpty(filter.UserType) ? filter.UserType : string.Empty)));
                sqlCommand.Parameters.Add(new SqlParameter("@LoginID", (!string.IsNullOrEmpty(filter.Loginid) ? filter.Loginid : string.Empty)));
                sqlCommand.Parameters.Add(new SqlParameter("@FormDate", (!string.IsNullOrEmpty(filter.FromDate) ? filter.FromDate : string.Empty)));
                sqlCommand.Parameters.Add(new SqlParameter("@ToDate", (!string.IsNullOrEmpty(filter.ToDate) ? filter.ToDate : string.Empty)));
                sqlCommand.Parameters.Add(new SqlParameter("@OderId", (!string.IsNullOrEmpty(filter.OrderId) ? filter.OrderId : string.Empty)));
                sqlCommand.Parameters.Add(new SqlParameter("@PNR", (!string.IsNullOrEmpty(filter.PNR) ? filter.PNR : string.Empty)));
                sqlCommand.Parameters.Add(new SqlParameter("@AirlinePNR", (!string.IsNullOrEmpty(filter.AirlinePNR) ? filter.AirlinePNR : string.Empty)));
                sqlCommand.Parameters.Add(new SqlParameter("@PaxName", (!string.IsNullOrEmpty(filter.PaxName) ? filter.PaxName : string.Empty)));
                sqlCommand.Parameters.Add(new SqlParameter("@TicketNo   ", (!string.IsNullOrEmpty(filter.TicketNo) ? filter.TicketNo : string.Empty)));
                sqlCommand.Parameters.Add(new SqlParameter("@AgentId", (!string.IsNullOrEmpty(filter.AgentId) ? filter.AgentId : string.Empty)));
                sqlCommand.Parameters.Add(new SqlParameter("@Trip", (!string.IsNullOrEmpty(filter.Trip) ? filter.Trip : string.Empty)));
                sqlCommand.Parameters.Add(new SqlParameter("@Status", (!string.IsNullOrEmpty(filter.Status) ? filter.Status : string.Empty)));
                sqlDataAdapter = new SqlDataAdapter();
                sqlDataAdapter.SelectCommand = sqlCommand;
                dataSet = new DataSet();
                sqlDataAdapter.Fill(dataSet, "GetTicketDetail_IntlStatus");
                dataTable = dataSet.Tables["GetTicketDetail_IntlStatus"];
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return dataTable;
        }
        public static DataTable CheckTktNo(int paxId, string orderId, string pnr)
        {
            try
            {
                sqlCommand = new SqlCommand("CheckTktNo_New", ConnectToDataBase.MyAmdDBConnection);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Parameters.Add(new SqlParameter("@PaxId", paxId));
                sqlCommand.Parameters.Add(new SqlParameter("@OrderId", orderId));
                sqlCommand.Parameters.Add(new SqlParameter("@PNR", pnr));
                sqlDataAdapter = new SqlDataAdapter();
                sqlDataAdapter.SelectCommand = sqlCommand;
                dataSet = new DataSet();
                sqlDataAdapter.Fill(dataSet, "GetTable");
                dataTable = dataSet.Tables["GetTable"];
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return dataTable;
        }

        public static DataTable GetGenerateBillNoCorp(string type)
        {
            try
            {
                sqlCommand = new SqlCommand("GenerateBillNoCorp", ConnectToDataBase.MyAmdDBConnection);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Parameters.Add(new SqlParameter("@type", type));
                sqlDataAdapter = new SqlDataAdapter();
                sqlDataAdapter.SelectCommand = sqlCommand;
                dataSet = new DataSet();
                sqlDataAdapter.Fill(dataSet, "GetGenerateBillNoCorp");
                dataTable = dataSet.Tables["GetGenerateBillNoCorp"];
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return dataTable;
        }
        public static DataTable GetTicketdIntl(int paxId, string paxType)
        {
            try
            {
                sqlCommand = new SqlCommand("CancelRequestIntl", ConnectToDataBase.MyAmdDBConnection);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Parameters.Add(new SqlParameter("@PaxId", paxId));
                sqlCommand.Parameters.Add(new SqlParameter("@PaxType", paxType));
                sqlDataAdapter = new SqlDataAdapter();
                sqlDataAdapter.SelectCommand = sqlCommand;
                dataSet = new DataSet();
                sqlDataAdapter.Fill(dataSet, "GetTable");
                dataTable = dataSet.Tables["GetTable"];
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return dataTable;
        }
        public static DataTable OldPaxInfo(string reissueId, string title, string fName, string mName, string lName, string paxType)
        {
            try
            {
                sqlCommand = new SqlCommand("SP_GetOldPaxDetails", ConnectToDataBase.MyAmdDBConnection);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Parameters.Add(new SqlParameter("@reissueid", reissueId));
                sqlCommand.Parameters.Add(new SqlParameter("@Title", title));
                sqlCommand.Parameters.Add(new SqlParameter("@FName", fName));
                sqlCommand.Parameters.Add(new SqlParameter("@MName", mName));
                sqlCommand.Parameters.Add(new SqlParameter("@LName", lName));
                sqlCommand.Parameters.Add(new SqlParameter("@PaxType", paxType));
                sqlDataAdapter = new SqlDataAdapter();
                sqlDataAdapter.SelectCommand = sqlCommand;
                dataSet = new DataSet();
                sqlDataAdapter.Fill(dataSet, "GetTable");
                dataTable = dataSet.Tables["GetTable"];
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return dataTable;
        }
        public static bool InsertReIssueCancelIntl(ReIssueReFund model)
        {
            try
            {
                sqlCommand = new SqlCommand("InsReIssueCancelIntl", ConnectToDataBase.MyAmdDBConnection);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@PaxID", !string.IsNullOrEmpty(model.PaxID) ? model.PaxID : string.Empty);
                sqlCommand.Parameters.AddWithValue("@PNR", model.PNR);
                sqlCommand.Parameters.AddWithValue("@Tkt_No", model.TicketNo);
                sqlCommand.Parameters.AddWithValue("@Sector", model.Sector);
                sqlCommand.Parameters.AddWithValue("@departure", model.Departure);
                sqlCommand.Parameters.AddWithValue("@destination", model.Destination);
                sqlCommand.Parameters.AddWithValue("@Title", model.Title);
                sqlCommand.Parameters.AddWithValue("@pax_fname", model.FName);
                sqlCommand.Parameters.AddWithValue("@pax_lname", model.LName);
                sqlCommand.Parameters.AddWithValue("@PaxType", model.PaxType);
                sqlCommand.Parameters.AddWithValue("@Booking_date", model.BookingDate);
                sqlCommand.Parameters.AddWithValue("@departure_date", model.DepartDate);
                sqlCommand.Parameters.AddWithValue("@UserID", model.UserID);
                sqlCommand.Parameters.AddWithValue("@Agency_Name", model.AgencyName);
                sqlCommand.Parameters.AddWithValue("@Base_Fare", model.BaseFare);
                sqlCommand.Parameters.AddWithValue("@Tax", !string.IsNullOrEmpty(model.Tax) ? model.Tax : string.Empty);
                sqlCommand.Parameters.AddWithValue("@YQ", model.YQ);
                sqlCommand.Parameters.AddWithValue("@Service_Tax", model.STax);
                sqlCommand.Parameters.AddWithValue("@Tran_Fees", model.TFee);
                sqlCommand.Parameters.AddWithValue("@Discount", model.Dis);
                sqlCommand.Parameters.AddWithValue("@CB", model.CB);
                sqlCommand.Parameters.AddWithValue("@TDS", model.TDS);
                sqlCommand.Parameters.AddWithValue("@TotalFare", model.TotalFare);
                sqlCommand.Parameters.AddWithValue("@TotalFareAfterDiscount", model.TotalFareAfterDis);
                sqlCommand.Parameters.AddWithValue("@OrderID", model.OrderId);
                sqlCommand.Parameters.AddWithValue("@DepTime", model.DepTime);
                sqlCommand.Parameters.AddWithValue("@AirlinePNR", model.AirlinePNR);
                sqlCommand.Parameters.AddWithValue("@FNo", model.FNo);
                sqlCommand.Parameters.AddWithValue("@Remark", model.Remark);
                sqlCommand.Parameters.AddWithValue("@IDs", model.RefundID); // genrate random number//"RISU""8346397933"//RISU8346397933
                sqlCommand.Parameters.AddWithValue("@Status", model.Status);
                sqlCommand.Parameters.AddWithValue("@VC", model.VC);
                sqlCommand.Parameters.AddWithValue("@ReqTyp", model.ReqTyp);
                sqlCommand.Parameters.AddWithValue("@Trip", !string.IsNullOrEmpty(model.Trip) ? model.Trip : string.Empty);
                sqlCommand.Parameters.AddWithValue("@ProjectID", model.ProjectID);//Nothing
                sqlCommand.Parameters.AddWithValue("@MgtFee", model.MgtFee);//0
                sqlCommand.Parameters.AddWithValue("@CancelledBy", model.CancelledBy);
                sqlCommand.Parameters.AddWithValue("@BillNoCorp", model.BillNoCorp);//Nothing
                sqlCommand.Parameters.AddWithValue("@BillNoAir", model.BillNoAir);//Nothing

                ConnectToDataBase.MyAmdOpenConnection();
                int isSuccess = sqlCommand.ExecuteNonQuery();
                ConnectToDataBase.MyAmdCloseConnection();

                if (isSuccess > 0)
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return false;
        }
        public static bool OTPTransactionInsert(string userId, string remark, string otpRefNo, string loginByOTP, string otpId, string serviceType)
        {
            try
            {
                sqlCommand = new SqlCommand("SP_Insert_Transaction_OTP", ConnectToDataBase.MyAmdDBConnection);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@UserID", userId);
                sqlCommand.Parameters.AddWithValue("@Remark", remark);
                sqlCommand.Parameters.AddWithValue("@OTPRefNo", otpRefNo);
                sqlCommand.Parameters.AddWithValue("@LoginByOTP", loginByOTP);
                sqlCommand.Parameters.AddWithValue("@OTPId", otpId);
                sqlCommand.Parameters.AddWithValue("@ServiceType", serviceType);

                ConnectToDataBase.MyAmdOpenConnection();
                int isSuccess = sqlCommand.ExecuteNonQuery();
                ConnectToDataBase.MyAmdCloseConnection();

                if (isSuccess > 0)
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return false;
        }

        public static bool UpdateTicketTeansCharges(string orderid, string charge, string amount, string AgentId)
        {
            try
            {
                sqlCommand = new SqlCommand("SP_UPDATE_TICKETTRANS_CHARGES", ConnectToDataBase.MyAmdDBConnection);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@AgentId", AgentId);
                sqlCommand.Parameters.AddWithValue("@OrderId", orderid);
                sqlCommand.Parameters.AddWithValue("@Amount", Convert.ToDecimal(amount));
                sqlCommand.Parameters.AddWithValue("@ChargeType", charge);
                sqlCommand.Parameters.AddWithValue("@CreatedBy", AgentId);
                sqlCommand.Parameters.AddWithValue("@IPAddress", ":1");
                sqlCommand.Parameters.AddWithValue("@ActionType", charge);
                sqlCommand.Parameters.Add("@Msg", SqlDbType.VarChar, 100).Direction = ParameterDirection.Output;
                ConnectToDataBase.MyAmdOpenConnection();
                int isSuccess = sqlCommand.ExecuteNonQuery();
                ConnectToDataBase.MyAmdCloseConnection();
                if (isSuccess > 0)
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return false;
        }
        public static DataTable GetEmail_Credentilas(string orderId, string cmd_Type, string counter)
        {
            try
            {
                sqlCommand = new SqlCommand("USP_TICKETSTATUS", ConnectToDataBase.MyAmdDBConnection);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Parameters.Add(new SqlParameter("@ORDERID", orderId));
                sqlCommand.Parameters.Add(new SqlParameter("@CMD_TYPE", cmd_Type));
                sqlCommand.Parameters.Add(new SqlParameter("@COUNTER", counter));
                sqlDataAdapter = new SqlDataAdapter();
                sqlDataAdapter.SelectCommand = sqlCommand;
                dataSet = new DataSet();
                sqlDataAdapter.Fill(dataSet, "GetTable");
                dataTable = dataSet.Tables["GetTable"];
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return dataTable;
        }
        public static DataTable GetCityList(string param)
        {
            try
            {
                sqlCommand = new SqlCommand("CityAutoSearch", ConnectToDataBase.MyAmdDBConnection);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Parameters.Add(new SqlParameter("@param1", param));

                sqlDataAdapter = new SqlDataAdapter();
                sqlDataAdapter.SelectCommand = sqlCommand;
                dataSet = new DataSet();
                sqlDataAdapter.Fill(dataSet, "WorldAirportInfo");
                dataTable = dataSet.Tables["WorldAirportInfo"];
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return dataTable;
        }
        public static DataTable GetAirlinesList(string param)
        {
            try
            {
                sqlCommand = new SqlCommand("AilrineList", ConnectToDataBase.MyAmdDBConnection);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Parameters.Add(new SqlParameter("@param1", param));

                sqlDataAdapter = new SqlDataAdapter();
                sqlDataAdapter.SelectCommand = sqlCommand;
                dataSet = new DataSet();
                sqlDataAdapter.Fill(dataSet, "AirLineNames");
                dataTable = dataSet.Tables["AirLineNames"];
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return dataTable;
        }

        public static DataTable AgencyAutoSearch(string param, string distrbutorid)
        {
            try
            {
                sqlCommand = new SqlCommand("AgencyAutoSearch", ConnectToDataBase.MyAmdDBConnection);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Parameters.Add(new SqlParameter("@param1", param));
                sqlCommand.Parameters.Add(new SqlParameter("@UserType", "DI"));
                sqlCommand.Parameters.Add(new SqlParameter("@DistrId", distrbutorid));

                sqlDataAdapter = new SqlDataAdapter();
                sqlDataAdapter.SelectCommand = sqlCommand;
                dataSet = new DataSet();
                sqlDataAdapter.Fill(dataSet, "AutoSearch");
                dataTable = dataSet.Tables["AutoSearch"];
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return dataTable;
        }
        public static bool InsertProxyDetails(OfflineRequest param)
        {
            bool issuccess = false;
            try
            {
                sqlCommand = new SqlCommand("InsertProxy", ConnectToDataBase.MyAmdDBConnection);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@BookingType", param.BookingType);
                sqlCommand.Parameters.AddWithValue("@TravelType", param.TripType);
                sqlCommand.Parameters.AddWithValue("@ProxyFrom", param.FromCityCode);
                sqlCommand.Parameters.AddWithValue("@ProxyTo", param.ToCityCode);
                sqlCommand.Parameters.AddWithValue("@DepartDate", param.DepartureDate);
                sqlCommand.Parameters.AddWithValue("@ReturnDate", !string.IsNullOrEmpty(param.ReturnDate) ? param.ReturnDate : "");
                sqlCommand.Parameters.AddWithValue("@DepartTime", param.Time);
                sqlCommand.Parameters.AddWithValue("@ReturnTime", !string.IsNullOrEmpty(param.ReturnDate) ? "Anytime" : "");
                sqlCommand.Parameters.AddWithValue("@Adult", param.Adult);
                sqlCommand.Parameters.AddWithValue("@Child", param.Child);
                sqlCommand.Parameters.AddWithValue("@Infrant", param.Infant);
                sqlCommand.Parameters.AddWithValue("@Class", param.ClassType);
                sqlCommand.Parameters.AddWithValue("@Airline", param.PreferedAirlinesCode);
                sqlCommand.Parameters.AddWithValue("@Classes", param.Classes);
                sqlCommand.Parameters.AddWithValue("@PaymentMode", param.PaymentLimit);
                sqlCommand.Parameters.AddWithValue("@Remark", !string.IsNullOrEmpty(param.Remark) ? param.Remark : "");
                sqlCommand.Parameters.AddWithValue("@AgentID", param.AgentUser_Id);
                sqlCommand.Parameters.AddWithValue("@Ag_Name", param.Agentname);
                sqlCommand.Parameters.AddWithValue("@Status", param.Status);
                sqlCommand.Parameters.AddWithValue("@Trip", param.Trip);
                sqlCommand.Parameters.AddWithValue("@Ptype", param.PType);
                sqlCommand.Parameters.AddWithValue("@ProjectID", !string.IsNullOrEmpty(param.ProjectId) ? param.ProjectId : "");
                sqlCommand.Parameters.AddWithValue("@BookedBy", !string.IsNullOrEmpty(param.BookedBy) ? param.BookedBy : "");

                ConnectToDataBase.MyAmdOpenConnection();
                int isSuccess = sqlCommand.ExecuteNonQuery();
                ConnectToDataBase.MyAmdCloseConnection();

                issuccess = true;
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return issuccess;
        }
        public static bool InsertProxyPaxDetail(OfflineBookFlightDetail param)
        {
            try
            {
                sqlCommand = new SqlCommand("InsertProxyPax_NM", ConnectToDataBase.MyAmdDBConnection);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@ProxyID", param.ProxyId);
                sqlCommand.Parameters.AddWithValue("@Title", param.Title);
                sqlCommand.Parameters.AddWithValue("@FirstName", param.FirstName);
                sqlCommand.Parameters.AddWithValue("@LastName", param.LastName);
                sqlCommand.Parameters.AddWithValue("@Age", param.Age);
                sqlCommand.Parameters.AddWithValue("@AgentID", param.AgentID);
                sqlCommand.Parameters.AddWithValue("@PaxType", param.PaxType);
                sqlCommand.Parameters.AddWithValue("@FFNO", !string.IsNullOrEmpty(param.FreqFlyerNO) ? param.FreqFlyerNO : "");
                sqlCommand.Parameters.AddWithValue("@PASSNO", !string.IsNullOrEmpty(param.PassportNo) ? param.PassportNo : "");
                sqlCommand.Parameters.AddWithValue("@PPEXP", !string.IsNullOrEmpty(param.PPExp) ? param.PPExp : "");
                sqlCommand.Parameters.AddWithValue("@VISADET", !string.IsNullOrEmpty(param.VisaDet) ? param.VisaDet : "");

                ConnectToDataBase.MyAmdOpenConnection();
                int isSuccess = sqlCommand.ExecuteNonQuery();
                ConnectToDataBase.MyAmdCloseConnection();

                if (isSuccess > 0)
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return false;
        }
        //public static DataTable GetAdultPassengerDetailList(string proxyid, string tableName)
        //{
        //    try
        //    {
        //        sqlCommand = new SqlCommand("Select * from " + tableName + " where ProxyID=" + proxyid, ConnectToDataBase.MyAmdDBConnection);

        //        sqlDataAdapter = new SqlDataAdapter();
        //        sqlDataAdapter.SelectCommand = sqlCommand;
        //        dataSet = new DataSet();
        //        sqlDataAdapter.Fill(dataSet, "PassDetail");
        //        dataTable = dataSet.Tables["PassDetail"];
        //    }
        //    catch (Exception)
        //    {

        //        throw;
        //    }

        //    return dataTable;
        //}        
        public static DataTable GetDetailFromAnyTable(string query)
        {
            try
            {
                sqlCommand = new SqlCommand(query, ConnectToDataBase.MyAmdDBConnection);

                sqlDataAdapter = new SqlDataAdapter();
                sqlDataAdapter.SelectCommand = sqlCommand;
                dataSet = new DataSet();
                sqlDataAdapter.Fill(dataSet, "CommonDetail");
                dataTable = dataSet.Tables["CommonDetail"];
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return dataTable;
        }
        public static DataTable GetFlightDetailsByAgentId_New(FlightCalender fltCal)
        {
            try
            {
                sqlCommand = new SqlCommand("Sp_GetFlightDetailsForTravelCal_PNRCONTS_New", ConnectToDataBase.MyAmdDBConnection);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Parameters.Add(new SqlParameter("@usertype", (!string.IsNullOrEmpty(fltCal.UserType) ? fltCal.UserType : "")));
                sqlCommand.Parameters.Add(new SqlParameter("@LoginID", (!string.IsNullOrEmpty(fltCal.LoginId) ? fltCal.LoginId : "")));
                sqlCommand.Parameters.Add(new SqlParameter("@PNR", (!string.IsNullOrEmpty(fltCal.PNR) ? fltCal.PNR : "")));
                sqlCommand.Parameters.Add(new SqlParameter("@airlinecode", (!string.IsNullOrEmpty(fltCal.AirlineCode) ? fltCal.AirlineCode : "")));
                sqlCommand.Parameters.Add(new SqlParameter("@PaxName", (!string.IsNullOrEmpty(fltCal.PaxName) ? fltCal.PaxName : "")));
                sqlCommand.Parameters.Add(new SqlParameter("@AgentID", (!string.IsNullOrEmpty(fltCal.AgentId) ? fltCal.AgentId : "")));
                sqlCommand.Parameters.Add(new SqlParameter("@date", (!string.IsNullOrEmpty(fltCal.Date) ? fltCal.Date : "")));
                sqlCommand.Parameters.Add(new SqlParameter("@trip", (!string.IsNullOrEmpty(fltCal.Trip) ? fltCal.Trip : "")));
                sqlCommand.Parameters.Add(new SqlParameter("@triptype", (!string.IsNullOrEmpty(fltCal.TripType) ? fltCal.TripType : "")));
                sqlCommand.Parameters.Add(new SqlParameter("@Source", (!string.IsNullOrEmpty(fltCal.Source) ? fltCal.Source : "")));
                sqlCommand.Parameters.Add(new SqlParameter("@destination", (!string.IsNullOrEmpty(fltCal.Destination) ? fltCal.Destination : "")));
                sqlDataAdapter = new SqlDataAdapter();
                sqlDataAdapter.SelectCommand = sqlCommand;
                dataSet = new DataSet();
                sqlDataAdapter.Fill(dataSet, "GetFlightDetailsForTravelCal");
                dataTable = dataSet.Tables["GetFlightDetailsForTravelCal"];
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return dataTable;
        }
        public static DataTable GetTicketDetail_Intl_calender(FlightCalender fltCal)
        {
            try
            {
                sqlCommand = new SqlCommand("USP_GetTicketDetail_Intl_CalenderN_New", ConnectToDataBase.MyAmdDBConnection);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Parameters.Add(new SqlParameter("@Calender_Date", (!string.IsNullOrEmpty(fltCal.Date) ? fltCal.Date : "")));
                sqlCommand.Parameters.Add(new SqlParameter("@AgentId", (!string.IsNullOrEmpty(fltCal.AgentId) ? fltCal.AgentId : "")));
                sqlCommand.Parameters.Add(new SqlParameter("@LoginID", (!string.IsNullOrEmpty(fltCal.LoginId) ? fltCal.LoginId : "")));
                sqlCommand.Parameters.Add(new SqlParameter("@usertype", (!string.IsNullOrEmpty(fltCal.UserType) ? fltCal.UserType : "")));
                sqlCommand.Parameters.Add(new SqlParameter("@month", (!string.IsNullOrEmpty(fltCal.Month) ? fltCal.Month : "")));
                sqlCommand.Parameters.Add(new SqlParameter("@year", (!string.IsNullOrEmpty(fltCal.Year) ? fltCal.Year : "")));
                sqlCommand.Parameters.Add(new SqlParameter("@PNR", (!string.IsNullOrEmpty(fltCal.PNR) ? fltCal.PNR : "")));
                sqlCommand.Parameters.Add(new SqlParameter("@PaxName", (!string.IsNullOrEmpty(fltCal.PaxName) ? fltCal.PaxName : "")));
                sqlCommand.Parameters.Add(new SqlParameter("@Source", (!string.IsNullOrEmpty(fltCal.Source) ? fltCal.Source : "")));
                sqlCommand.Parameters.Add(new SqlParameter("@destination", (!string.IsNullOrEmpty(fltCal.Destination) ? fltCal.Destination : "")));
                sqlCommand.Parameters.Add(new SqlParameter("@AirlinePNR", (!string.IsNullOrEmpty(fltCal.AirlineCode) ? fltCal.AirlineCode : "")));
                sqlCommand.Parameters.Add(new SqlParameter("@trip", (!string.IsNullOrEmpty(fltCal.Trip) ? fltCal.Trip : "")));
                sqlCommand.Parameters.Add(new SqlParameter("@triptype", (!string.IsNullOrEmpty(fltCal.TripType) ? fltCal.TripType : "")));
                sqlDataAdapter = new SqlDataAdapter();
                sqlDataAdapter.SelectCommand = sqlCommand;
                dataSet = new DataSet();
                sqlDataAdapter.Fill(dataSet, "FlightDetailsByAgentId");
                dataTable = dataSet.Tables["FlightDetailsByAgentId"];
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return dataTable;
        }

        public static string TicketCopyExportPDF(string OrderId, string TransID)
        {
            string TicketFormate = "";
            string BackClor = "";
            int initialAdt = 0;
            int initalChld = 0;
            int initialift = 0;
            decimal MealBagTotalPrice = 0;
            decimal AdtTtlFare = 0;
            decimal ChdTtlFare = 0;
            decimal INFTtlFare = 0;
            decimal fare = 0;
            decimal TBasefare = 0;
            decimal ABasefare = 0;
            decimal CBasefare = 0;
            decimal IBasefare = 0;
            decimal Tfuel = 0;
            decimal Afuel = 0;
            decimal Cfuel = 0;
            decimal Ifuel = 0;
            decimal Ttax = 0;
            decimal Atax = 0;
            decimal Ctax = 0;
            decimal Itax = 0;
            decimal Tgst = 0;
            decimal Agst = 0;
            decimal Cgst = 0;
            decimal Igst = 0;
            decimal TTransfee = 0;
            decimal ATransfee = 0;
            decimal CTransfee = 0;
            decimal ITransfee = 0;
            decimal TTranscharge = 0;
            decimal ATranscharge = 0;
            decimal CTranscharge = 0;
            decimal ITranscharge = 0;
            DataTable FltPaxList = new DataTable();
            DataTable FltDetailsList = new DataTable();
            DataTable FltProvider = new DataTable();
            DataTable FltBaggage = new DataTable();
            DataTable dtagentid = new DataTable();
            DataTable FltagentDetail = new DataTable();
            DataTable fltTerminal = new DataTable();
            DataTable fltFare = new DataTable();
            DataTable fltMealAndBag = new DataTable();
            DataTable fltMealAndBag1 = new DataTable();
            DataSet fltAirportDetails = new DataSet();
            DataSet SelectedFltDS = new DataSet();
            DataTable ProxyCharge = new DataTable();
            DataTable FltHeaderList = new DataTable();
            FltPaxList = SelectPaxDetail(OrderId, TransID);
            FltHeaderList = SelectHeaderDetail(OrderId);
            FltDetailsList = SelectFlightDetail(OrderId);
            ProxyCharge = ProxyChargeMth(OrderId);
            dtagentid = SelectAgent(OrderId);
            SelectedFltDS = GetFltDtls(OrderId, dtagentid.Rows[0]["AgentID"].ToString());
            bool Bag = false;
            if (!string.IsNullOrEmpty(Convert.ToString(SelectedFltDS.Tables[0].Rows[0]["IsBagFare"])))
                Bag = Convert.ToBoolean(SelectedFltDS.Tables[0].Rows[0]["IsBagFare"]);
            FltBaggage = (GetBaggageInformation(Convert.ToString(FltHeaderList.Rows[0]["Trip"]), Convert.ToString(FltHeaderList.Rows[0]["VC"]), Bag));
            FltagentDetail = SelectAgencyDetail(dtagentid.Rows[0]["AgentID"].ToString());
            fltFare = SelectFareDetail(OrderId, TransID);
            DateTime dt = Convert.ToDateTime(Convert.ToString(FltHeaderList.Rows[0]["CreateDate"]));
            string date = dt.ToString("dd/MMM/yyyy").Replace('-', '/');
            string Createddate = date.Split('/')[0] + " " + date.Split('/')[1] + " " + date.Split('/')[2];
            try
            {
                if (((Convert.ToString(FltHeaderList.Rows[0]["Status"]).ToLower().Trim() == "confirm" || Convert.ToString(FltHeaderList.Rows[0]["Status"]).ToLower().Trim() == "confirmbyagent")))
                {
                    TicketFormate += "<table style='width:100%;'>";
                    TicketFormate += "<tr>";
                    TicketFormate += "<td style='font-size: 15px; width: 15%; text-align: left; padding: 5px;'>";
                    TicketFormate += "<b>Booking Reference No. " + OrderId + "</b>";
                    TicketFormate += "</td>";
                    TicketFormate += "</tr>";
                    TicketFormate += "<tr>";
                    TicketFormate += "<td style='font-size: 14px; width: 15%; text-align: left; padding: 5px;'>";
                    TicketFormate += "PNR-<b>" + FltHeaderList.Rows[0]["GdsPnr"] + " </b>is on <b>HOLD</b>.Functioning team is working on it and may take 15 minutes to resolve. Please contact our customer care representative at <b>+91 00 0000 0000</b> for any further assistance";
                    TicketFormate += "</td>";
                    TicketFormate += "</tr>";
                    TicketFormate += "</table>";
                }
                string dd = "";
                string AgentAddress = "";
                try
                {
                    dd = Config.WebsiteUrl + "/AgentLogo/" + Convert.ToString(dtagentid.Rows[0]["AgentID"]) + ".jpg";
                    AgentAddress = "<p style='font-weight: bolder; font-size: 20px;height: 8px;margin: 0px 0 20px !important;text-align:right;'>" + Convert.ToString(FltagentDetail.Rows[0]["Agency_Name"]) + "</p><p style='font-size: 13px;height: 8px;margin: 0px 0 13px !important;text-align:right;'>" + Convert.ToString(FltagentDetail.Rows[0]["Address"]) + "</p><p style='font-size: 13px;height: 8px;margin: 0px 0 13px !important;text-align:right;'>" + Convert.ToString(FltagentDetail.Rows[0]["Address1"]) + "</p><p style='font-size: 13px;height: 8px;margin: 0px 0 13px !important;text-align:right;'>Mobile:" + Convert.ToString(FltagentDetail.Rows[0]["Mobile"]) + "</p><p style='font-size: 13px;height: 8px;margin: 0px 0 13px !important;text-align:right;'>Email:" + Convert.ToString(FltagentDetail.Rows[0]["Email"]) + "</p>";
                }
                catch (Exception ex)
                {
                    ex.ToString();
                    dd = "../Advance_CSS/Icons/logo(ft).png";
                }
                if ((FltDetailsList.Rows[0]["AirlineCode"].ToString() == "SG"))
                    BackClor = "#9c1304";
                else if ((FltDetailsList.Rows[0]["AirlineCode"].ToString() == "6E"))
                    BackClor = "#2d3c9a";
                else if ((FltDetailsList.Rows[0]["AirlineCode"].ToString() == "G8"))
                    BackClor = "#172169";
                else if ((FltDetailsList.Rows[0]["AirlineCode"].ToString() == "UK"))
                    BackClor = "#47143d";
                else if ((FltDetailsList.Rows[0]["AirlineCode"].ToString() == "AK"))
                    BackClor = "#e32525";
                else if ((FltDetailsList.Rows[0]["AirlineCode"].ToString() == "I5"))
                    BackClor = "#e32525";
                else if ((FltDetailsList.Rows[0]["AirlineCode"].ToString() == "AI"))
                    BackClor = "#e32525";
                else
                    BackClor = "#000";

                TicketFormate += "<div class='large-12 medium-12 small-12' style='border:1px solid " + BackClor + "'>";
                TicketFormate += "<table style='width: 100%;'>";
                TicketFormate += "<tbody>";
                TicketFormate += "<tr id='trLogoWithAgencySection' style='border: 1px solid  " + BackClor + ";'>";
                TicketFormate += "<td colspan='4'> <img id='agencylogo' src='" + dd + "' alt='Logo' width='170' height='87'/></td>";
                TicketFormate += "<td style='background-color: white;width:55%' colspan='4'>";
                TicketFormate += AgentAddress;
                TicketFormate += "</td>";
                TicketFormate += "</tr>";
                TicketFormate += "<tr style='border: 2px solid  " + BackClor + ";'>";
                TicketFormate += "<td  colspan='4' style='background-color: white;border: 1px solid #ccc;'>";
                TicketFormate += "<p style='font-size: 15px;height: 8px;'>Booking Date:" + FltHeaderList.Rows[0]["Createdate"] + "</p>";
                TicketFormate += "<p style='font-size: 15px;height: 8px;'>Ref No:" + OrderId + "</p>";
                if (FltHeaderList.Rows[0]["Status"].ToString() == "Ticketed")
                {
                    TicketFormate += "<p style='font-size: 15px;height: 8px;'>Status: <span style='font-weight: bold;color: green;'>Confirm</span></p>";
                }
                else
                {
                    TicketFormate += "<p style='font-size: 15px;height: 8px;'>Status:" + FltHeaderList.Rows[0]["Status"] + "</p>";
                }

                TicketFormate += "</td>";
                TicketFormate += "<td colspan='2' style='background-color: white;border: 1px solid #ccc;width: 270px;border-right: 0px solid #fff;'><img alt='Logo Not Found' style='width: 35px;margin-left: 20px;border-radius: 20px;' src='http://richatravels.in/AirLogo/sm" + FltDetailsList.Rows[0]["AirlineCode"] + ".gif'/><i style='font-weight: bold;font-size: 15px;margin-left: 15px;margin: 0px 0 0px !important;'>" + FltDetailsList.Rows[0]["AirlineName"] + "</i>";
                TicketFormate += "</td>";
                TicketFormate += "<td colspan='2' style='background-color: white;border: 1px solid #ccc;width: 176px;border-left: 0px solid;'>  <p style='font-weight: bold;font-size: 15px;margin: 0px 0 0px !important;'>" + FltHeaderList.Rows[0]["GdsPnr"] + " &nbsp; <b style='color:red; font-waight:bold;'>" + FltHeaderList.Rows[0]["MordifyStatus"].ToString().ToUpper() + "</b></p><p style='margin: 0px 0 0px !important;'>Airline PNR</p>";
                TicketFormate += "</td>";
                TicketFormate += "</tr>";
                TicketFormate += "</tbody>";
                TicketFormate += "</table>";
                TicketFormate += "<table style='border: 1px solid  " + BackClor + "; padding: 0px !important; width: 100%;'>";
                TicketFormate += " <tbody>";
                TicketFormate += "<tr><td class='pri' style='background-color:" + BackClor + "; color: #fff; font-size: 12px; font-weight: bold; padding: 5px;width: 100%' colspan='6'>Passenger Information</td></tr>";
                TicketFormate += "<tr>";
                TicketFormate += "<td colspan='6' style='font-size: 12px; padding: 5px; width: 100%'>";
                TicketFormate += "<table style='font-size: 12px; padding: 5px; width: 100%; border: 1px solid #000'>";
                TicketFormate += "<tbody style='border: 1px solid #000;'>";
                TicketFormate += "<tr style='border: 1px solid #000;'>";
                TicketFormate += "<th style='font-size: 12px; text-align: left; padding: 5px; border-right: 1px solid #000;' colspan='2'>Email</th>";
                TicketFormate += "<th style='font-size: 12px; text-align: left; padding: 5px; border-right: 1px solid #000;'colspan='2'>Contact No</th>";
                TicketFormate += "<th style='font-size: 12px; text-align: center; padding: 5px;' colspan='2'>Booking Mode </th>";
                TicketFormate += "</tr>";
                TicketFormate += "<tr>";
                TicketFormate += "<td style='font-size: 12px; text-align: left; padding: 5px; border-right: 1px solid #000 !important;'  colspan='2'>" + FltHeaderList.Rows[0]["PgEmail"] + "</td>";
                TicketFormate += "<td style='font-size: 12px; text-align: left; padding: 5px; border-right: 1px solid #000 !important;'  colspan='2'>" + FltHeaderList.Rows[0]["PgMobile"] + "</td>";
                TicketFormate += "<td style='font-size: 12px; text-align: center; padding: 5px; border-right: 1px solid #000 !important;'  colspan='2'><span style='color:green;font-weight: bold;text-align:center'>Online<span/></td>";
                TicketFormate += "</tr>";
                TicketFormate += "</tbody>";
                TicketFormate += "</table>";
                TicketFormate += "</tr>";
                TicketFormate += "</tbody>";
                TicketFormate += "</table>";
                if ((Convert.ToString(FltHeaderList.Rows[0]["Status"]).ToLower().Trim() == "ticketed"))
                {
                    for (int f = 0; f <= FltDetailsList.Rows.Count - 1; f++)
                    {
                        TicketFormate += "<table style='border: 1px solid  " + BackClor + "; padding: 0px !important; width: 100%;'>";
                        TicketFormate += " <tbody>";
                        TicketFormate += "<tr><td class='pri' style='background-color:" + BackClor + "; color: #fff; font-size: 12px; font-weight: bold; padding: 5px;width: 40%' colspan='4'>Airline Pnr :  " + FltHeaderList.Rows[0]["GdsPnr"] + "/" + FltHeaderList.Rows[0]["AirlinePnr"] + "</td><td class='pri' style='background-color: " + BackClor + "; color: #fff; font-size: 12px; font-weight: bold; padding: 5px;text-align: end;width: 60%' colspan='4'>Confirmed Booked On:" + FltHeaderList.Rows[0]["Createdate"] + "</td></tr>";
                        TicketFormate += " <tr>";
                        TicketFormate += "<td colspan='8' style='font-size: 12px; padding: 5px; width: 100%'>";
                        TicketFormate += "<table style='font-size: 12px; padding: 5px; width: 100%;'>";
                        TicketFormate += " <tbody>";
                        TicketFormate += " <tr>";
                        string strDepdt = Convert.ToString(FltDetailsList.Rows[f]["DepDate"]);
                        strDepdt = (strDepdt.Length == 8 ? Left(strDepdt, 4) + "-" + Mid(strDepdt, 4, 2) + "-" + Right(strDepdt, 2) : "20" + Right(strDepdt, 2) + "-" + Mid(strDepdt, 2, 2) + "-" + Left(strDepdt, 2));
                        DateTime deptdt = Convert.ToDateTime(strDepdt);
                        strDepdt = deptdt.ToString("dd/MMM/yy").Replace('-', '/');


                        // Response.Write(strDepdt)

                        string depDay = Convert.ToString(deptdt.DayOfWeek);
                        strDepdt = strDepdt.Split('/')[0] + " " + strDepdt.Split('/')[1] + " " + strDepdt.Split('/')[2];
                        string strdeptime = Convert.ToString(FltDetailsList.Rows[f]["DepTime"]);
                        try
                        {
                            if (strdeptime.Length > 4)
                                strdeptime = strdeptime.Substring(0, 2) + " : " + strdeptime.Substring(3, 2);
                            else
                                strdeptime = strdeptime.Substring(0, 2) + " : " + strdeptime.Substring(2, 2);
                        }
                        catch (Exception ex)
                        {
                            ex.ToString();
                        }
                        TicketFormate += "<td colspan='2' style='font-size: 12px; text-align: left; vertical-align: top;'><img alt='" + Convert.ToString(FltagentDetail.Rows[0]["Agency_Name"]) + "' style='width: 50px;' src='" + Config.WebsiteUrl + "/AirLogo/sm" + FltDetailsList.Rows[f]["AirlineCode"] + ".gif'/><br />" + FltDetailsList.Rows[f]["AirlineName"] + "<br />" + FltDetailsList.Rows[f]["AirlineCode"] + " " + FltDetailsList.Rows[f]["FltNumber"] + "<br/></td>";
                        TicketFormate += "<td colspan='2' style='font-size: 12px; text-align: left; padding: 2px; text-align: right;'>";
                        TicketFormate += "<p style='font-size: 16px;font-weight:600;text-align:right;'>" + FltDetailsList.Rows[f]["DepAirName"] + "( " + FltDetailsList.Rows[f]["DFrom"] + ")" + " " + strdeptime + "</p>"; // 'tttt

                        TicketFormate += "<br />" + strDepdt + "<br />"; // 'tttt
                        fltTerminal = TerminalDetails(OrderId, FltDetailsList.Rows[f]["DFrom"].ToString(), "");
                        if (string.IsNullOrEmpty(Convert.ToString(fltTerminal.Rows[0]["DepartureTerminal"])))
                            TicketFormate += fltTerminal.Rows[0]["DepAirportName"] + " - Trml: NA";
                        else
                            TicketFormate += fltTerminal.Rows[0]["DepAirportName"] + " - Trml:" + fltTerminal.Rows[0]["DepartureTerminal"];
                        TicketFormate += "</td>";
                        string strArvdt = Convert.ToString(FltDetailsList.Rows[f]["ArrDate"]);
                        strArvdt = (strArvdt.Length == 8 ? Left(strArvdt, 4) + "-" + Mid(strArvdt, 4, 2) + "-" + Right(strArvdt, 2) : "20" + Right(strArvdt, 2) + "-" + Mid(strArvdt, 2, 2) + "-" + Left(strArvdt, 2));
                        DateTime Arrdt = Convert.ToDateTime(strArvdt);
                        strArvdt = Arrdt.ToString("dd/MMM/yy").Replace('-', '/');
                        string ArrDay = Convert.ToString(Arrdt.DayOfWeek);
                        strArvdt = strArvdt.Split('/')[0] + " " + strArvdt.Split('/')[1] + " " + strArvdt.Split('/')[2];
                        string strArrtime = Convert.ToString(FltDetailsList.Rows[f]["ArrTime"]);
                        try
                        {
                            if (strArrtime.Length > 4)
                                strArrtime = strArrtime.Substring(0, 2) + " : " + strArrtime.Substring(3, 2);
                            else
                                strArrtime = strArrtime.Substring(0, 2) + " : " + strArrtime.Substring(2, 2);
                        }
                        catch (Exception ex)
                        {
                            ex.ToString();
                        }
                        string totdur = "";
                        string stops = "";
                        try
                        {
                            totdur = Convert.ToString(SelectedFltDS.Tables[0].Rows[0]["Tot_Dur"]);
                            stops = Convert.ToString(SelectedFltDS.Tables[0].Rows[0]["Stops"]);
                        }
                        catch (Exception ex)
                        {
                            totdur = Convert.ToString(SelectedFltDS.Tables[0].Rows[0]["Duration"]);
                            stops = Convert.ToString(FltDetailsList.Rows.Count);
                        }
                        TicketFormate += "<td colspan='2' style='text-align: center;'><i style='font-weight: 600'>" + totdur + "</i><br />"; // 'tttt
                        TicketFormate += "------------------<br />";
                        TicketFormate += "" + stops + "<br />"; // ''tttt
                        TicketFormate += "" + Convert.ToString(SelectedFltDS.Tables[0].Rows[0]["AdtFareType"]) + "</td>"; // ''ttt
                        TicketFormate += "<td colspan='2' style='font-size: 12px; text-align: left; padding: 2px;'>";
                        TicketFormate += "<p style='font-size: 16px; font-weight: 600;'>" + strArrtime + " " + FltDetailsList.Rows[f]["ArrAirName"].ToString().Trim() + " (" + FltDetailsList.Rows[f]["ATo"].ToString().Trim() + ")</p>"; // 'ttt



                        TicketFormate += "<br />" + strArvdt + "<br />"; // '''tttttt

                        fltTerminal = TerminalDetails(OrderId, "", FltDetailsList.Rows[f]["ATo"].ToString());
                        if (string.IsNullOrEmpty(Convert.ToString(fltTerminal.Rows[0]["ArrivalTerminal"])))
                            TicketFormate += fltTerminal.Rows[0]["ArrvlAirportName"] + " - Trml: NA";
                        else
                            TicketFormate += fltTerminal.Rows[0]["ArrvlAirportName"] + " - Trml:" + fltTerminal.Rows[0]["ArrivalTerminal"];
                        TicketFormate += "</td>"; // ''tttt
                        TicketFormate += "</tr>";
                        TicketFormate += "</tbody>";
                        TicketFormate += "</table>";
                        TicketFormate += "</td>";
                        TicketFormate += "</tr>";
                        TicketFormate += " </tbody>";
                        TicketFormate += "</table>";
                    }
                    TicketFormate += " <table style='border: 1px solid #0b2759; font-size: 12px; padding: 0px !important; width: 100%;'>";
                    TicketFormate += "<tbody>";
                    TicketFormate += "<tr>";
                    TicketFormate += "<td class='pri' style='background-color: " + BackClor + "; color: #fff; font-size: 12px; font-weight: bold; padding: 5px;' colspan='6'>Passenger &amp; Ticket Information</td>";
                    TicketFormate += "</tr>";
                    TicketFormate += "<tr>";
                    TicketFormate += "<td colspan='6' style='font-size: 12px; padding: 5px; width: 100%'>";
                    TicketFormate += "<table style='font-size: 12px; padding: 5px; width: 100%; border: 1px solid #000'>";
                    TicketFormate += "<tbody style='border: 1px solid #000;'>";
                    TicketFormate += "<tr style='border: 1px solid #000;'>";
                    TicketFormate += "<th style='font-size: 12px; text-align: left; padding: 5px; border-right: 1px solid #000;'>Passenger Information</th>";
                    TicketFormate += "<th style='font-size: 12px; text-align: left; padding: 5px; border-right: 1px solid #000;'>Sector</th>";
                    TicketFormate += "<th style='font-size: 12px; text-align: left; padding: 5px; border-right: 1px solid #000;'>Ticket No</th>";
                    TicketFormate += "<th style='font-size: 12px; text-align: left; padding: 5px; border-right: 1px solid #000;'>Seat</th>";
                    TicketFormate += "<th style='font-size: 12px; text-align: left; padding: 5px; border-right: 1px solid #000;'>Meal</th>";
                    TicketFormate += "<th style='font-size: 12px; text-align: left; padding: 5px;'>Extra</th>";
                    TicketFormate += "</tr>";

                    for (int p = 0; p <= FltPaxList.Rows.Count - 1; p++)
                    {
                        TicketFormate += "<tr style='border: 1px solid #000;'>";
                        TicketFormate += "<td style='font-size: 12px; text-align: left; padding: 5px; border-right: 1px solid #000 !important;'>" + FltPaxList.Rows[p]["Name"] + "</td>"; // 'ttt
                        TicketFormate += "<td style='font-size: 12px; text-align: left; padding: 5px; border-right: 1px solid #000 !important;border-collapse:collapse !important; padding: 0px 0px !important; margin:0 !important; text-indent:10px;'>";
                        TicketFormate += "<table style='font-size: 12px; padding: 5px; width: 100%;;margin-bottom: 0px;'>"; // 'ttt
                        TicketFormate += "<tbody>";
                        for (int f = 0; f <= FltDetailsList.Rows.Count - 1; f++)
                        {
                            TicketFormate += "<tr  >";
                            TicketFormate += "<td style='font-size: 12px; text-align: left; padding: 5px;'>" + FltDetailsList.Rows[f]["DFrom"] + "-" + FltDetailsList.Rows[f]["ATo"] + "</td>"; // 'ttt
                            TicketFormate += "</tr>";
                        }
                        TicketFormate += "</tbody>";
                        TicketFormate += "</table>"; // 'ttt
                        TicketFormate += "</td>";
                        TicketFormate += "<td style='font-size: 12px; text-align: left; padding: 5px; border-right: 1px solid #000 !important; '>" + Convert.ToString(FltPaxList.Rows[p]["TicketNumber"]) + "</td>";
                        TicketFormate += "<td style='font-size: 12px; text-align: left; padding: 5px; border-right: 1px solid #000 !important;'>None</td>";
                        TicketFormate += "<td style='font-size: 12px; text-align: left; padding: 5px; border-right: 1px solid #000 !important;'>None</td>";
                        TicketFormate += "<td style='font-size: 12px; text-align: left; padding: 5px; border-right: 1px solid #000 !important;'>None</td>";
                        TicketFormate += "</tr>";
                    }

                    TicketFormate += "</tbody>";
                    TicketFormate += "</table>";
                    TicketFormate += "</td>";
                    TicketFormate += "</tr>";
                    TicketFormate += "</tbody>";
                    TicketFormate += "</table>";
                    if (TransID == "" || TransID == null)
                    {
                        TicketFormate += "<table style='border: 1px solid #0b2759; font-size: 12px; padding: 0px !important; width: 100%;'>";
                        TicketFormate += "<tbody>";
                        TicketFormate += "<tr class='pri2 hidden'>";
                        TicketFormate += "<td colspan='6' style= 'background-color:#fff;width:100%;border: 1px solid #000;'>";
                        TicketFormate += "<table style='width:100%;' id='fareinfo'>";
                        TicketFormate += "<tbody style='border: 1px solid #000;'>";
                        TicketFormate += "<tr>";
                        TicketFormate += "<td class='pri' style='background-color: " + BackClor + "; color: #fff; font-size: 12px; font-weight: bold; padding: 5px;' colspan='9'>Fare Information</td>";
                        TicketFormate += "</tr>";
                        TicketFormate += "<tr style='border: 1px solid #000;'>";
                        TicketFormate += "<th style='font-size: 12px; text-align: left; padding: 5px; border-right: 1px solid #000;'>Pax Type</th>";
                        TicketFormate += "<th style='font-size: 12px; text-align: left; padding: 5px; border-right: 1px solid #000;'>Pax Count</th>";
                        TicketFormate += "<th style='font-size: 12px; text-align: left; padding: 5px; border-right: 1px solid #000;'>Base fare</th>";
                        TicketFormate += "<th class='pri2' style='font-size: 12px; text-align: left; padding: 5px; border-right: 1px solid #000;'>Fuel Surcharge</th>";
                        TicketFormate += "<th style='font-size: 12px; text-align: left; padding: 5px; border-right: 1px solid #000;'>Tax</th>";
                        TicketFormate += "<th style='font-size: 12px; text-align: left; padding: 5px; border-right: 1px solid #000;'>STax</th>";
                        TicketFormate += "<th style='font-size: 12px; text-align: left; padding: 5px; border-right: 1px solid #000;'>Trans Fee</th>";
                        TicketFormate += "<th style='font-size: 12px; text-align: left; padding: 5px; border-right: 1px solid #000;'>Trans Charge</th>";
                        TicketFormate += "<th style='font-size: 12px; text-align: left; padding: 5px; border-right: 1px solid #000;'>Total</th>";
                        TicketFormate += "</tr>";
                        for (int fd = 0; fd <= fltFare.Rows.Count - 1; fd++)
                        {
                            if (fltFare.Rows[fd]["PaxType"].ToString() == "ADT" && initialAdt == 0)
                            {
                                int numberOfADT = FltPaxList.AsEnumerable().Where(x => x["PaxType"].ToString() == "ADT").ToList().Count;
                                TicketFormate += "<tr>";
                                TicketFormate += "<td style='font-size: 12px; text-align: right; padding: 5px; border-right: 1px solid #000;'>";
                                TicketFormate += fltFare.Rows[fd]["PaxType"];
                                TicketFormate += "</td>";
                                TicketFormate += "<td style='font-size: 12px; text-align: right; padding: 5px; border-right: 1px solid #000;' id='td_adtcnt'>" + numberOfADT + "</td>";
                                TicketFormate += "<td style='font-size: 12px; text-align: right; padding: 5px; border-right: 1px solid #000;'>";
                                TicketFormate += (Convert.ToDecimal(fltFare.Rows[fd]["BaseFare"]) * numberOfADT).ToString();
                                ABasefare = Convert.ToDecimal(fltFare.Rows[fd]["BaseFare"]) * numberOfADT;
                                TicketFormate += "</td>";
                                TicketFormate += "<td style='font-size: 12px; text-align: right; padding: 5px; border-right: 1px solid #000;'>";
                                TicketFormate += (Convert.ToDecimal(fltFare.Rows[fd]["Fuel"]) * numberOfADT).ToString();
                                Afuel = Convert.ToDecimal(fltFare.Rows[fd]["Fuel"]) * numberOfADT;
                                TicketFormate += "</td>";
                                TicketFormate += "<td style='font-size: 12px; text-align: right; padding: 5px; border-right: 1px solid #000;'id='td_taxadt'>";
                                TicketFormate += ((Convert.ToDecimal(fltFare.Rows[fd]["Tax"]) * numberOfADT) + (Convert.ToDecimal(fltFare.Rows[fd]["TCharge"]) * numberOfADT)).ToString();
                                TicketFormate += (Convert.ToDecimal(fltFare.Rows[fd]["Tax"]) * numberOfADT).ToString();
                                Atax = (Convert.ToDecimal(fltFare.Rows[fd]["Tax"]) * numberOfADT) + (Convert.ToDecimal(fltFare.Rows[fd]["TCharge"]) * numberOfADT);
                                Atax = (Convert.ToDecimal(fltFare.Rows[fd]["Tax"]) * numberOfADT);
                                TicketFormate += "</td>";
                                TicketFormate += "<td style='font-size: 12px; text-align: right; padding: 5px; border-right: 1px solid #000;'>";
                                TicketFormate += (Convert.ToDecimal(fltFare.Rows[fd]["ServiceTax"]) * numberOfADT).ToString();
                                Agst = Convert.ToDecimal(fltFare.Rows[fd]["ServiceTax"]) * numberOfADT;
                                TicketFormate += "</td>";
                                TicketFormate += "<td style='font-size: 12px; text-align: right; padding: 5px; border-right: 1px solid #000;'>";
                                TicketFormate += (Convert.ToDecimal(fltFare.Rows[fd]["TFee"]) * numberOfADT).ToString();
                                TTransfee = Convert.ToDecimal(fltFare.Rows[fd]["TFee"]) * numberOfADT;
                                TicketFormate += "</td>";
                                TicketFormate += "<td style='font-size: 12px; text-align: right; padding: 5px; border-right: 1px solid #000;'id='td_tcadt'>";
                                TicketFormate += (Convert.ToDecimal(fltFare.Rows[fd]["TCharge"]) * numberOfADT).ToString();
                                ATranscharge = Convert.ToDecimal(fltFare.Rows[fd]["TCharge"]) * numberOfADT;
                                TicketFormate += "</td>";
                                TicketFormate += "<td style='font-size: 12px; text-align: right; padding: 5px; border-right: 1px solid #000;' id='td_adttot'>";
                                AdtTtlFare = (Convert.ToDecimal(fltFare.Rows[fd]["Total"]) * numberOfADT);
                                TicketFormate += AdtTtlFare.ToString();
                                TicketFormate += "</td>";
                                TicketFormate += "</tr>";

                                initialAdt += 1;
                            }

                            if (fltFare.Rows[fd]["PaxType"].ToString() == "CHD" && initalChld == 0)
                            {
                                int numberOfCHD = FltPaxList.AsEnumerable().Where(x => x["PaxType"].ToString() == "CHD").ToList().Count;
                                TicketFormate += "<tr>";
                                TicketFormate += "<td style='font-size: 12px; text-align: right; padding: 5px; border-right: 1px solid #000;border-top: 1px solid #000;background: #fff;'>";
                                TicketFormate += fltFare.Rows[fd]["PaxType"];
                                TicketFormate += "</td>";
                                TicketFormate += "<td style='font-size: 12px; text-align: right; padding: 5px; border-right: 1px solid #000;border-top: 1px solid #000;background: #fff;' id='td_chdcnt'>" + numberOfCHD + "</td>";
                                TicketFormate += "<td style='font-size: 12px; text-align: right; padding: 5px; border-right: 1px solid #000;border-top: 1px solid #000;background: #fff;'>";
                                TicketFormate += (Convert.ToDecimal(fltFare.Rows[fd]["BaseFare"]) * numberOfCHD).ToString();
                                CBasefare = Convert.ToDecimal(fltFare.Rows[fd]["BaseFare"]) * numberOfCHD;
                                TicketFormate += "</td>";
                                TicketFormate += "<td style='font-size: 12px; text-align: right; padding: 5px; border-right: 1px solid #000;border-top: 1px solid #000;background: #fff;'>";
                                TicketFormate += (Convert.ToDecimal(fltFare.Rows[fd]["Fuel"]) * numberOfCHD).ToString();
                                Cfuel = Convert.ToDecimal(fltFare.Rows[fd]["Fuel"]) * numberOfCHD;
                                TicketFormate += "</td>";
                                TicketFormate += "<td style='font-size: 12px; text-align: right; padding: 5px; border-right: 1px solid #000;border-top: 1px solid #000;background: #fff;'id='td_taxchd'>";
                                TicketFormate += (Convert.ToDecimal(fltFare.Rows[fd]["Tax"]) * numberOfCHD).ToString();
                                Ctax = Convert.ToDecimal(fltFare.Rows[fd]["Tax"]) * numberOfCHD;
                                TicketFormate += "</td>";
                                TicketFormate += "<td style='font-size: 12px; text-align: right; padding: 5px; border-right: 1px solid #000;border-top: 1px solid #000;background: #fff;'>";
                                TicketFormate += (Convert.ToDecimal(fltFare.Rows[fd]["ServiceTax"]) * numberOfCHD).ToString();
                                Cgst = Convert.ToDecimal(fltFare.Rows[fd]["ServiceTax"]) * numberOfCHD;
                                TicketFormate += "</td>";
                                TicketFormate += "<td style='font-size: 12px; text-align: right; padding: 5px; border-right: 1px solid #000;border-top: 1px solid #000;background: #fff;'>";
                                TicketFormate += (Convert.ToDecimal(fltFare.Rows[fd]["TFee"]) * numberOfCHD).ToString();
                                CTransfee = Convert.ToDecimal(fltFare.Rows[fd]["TFee"]) * numberOfCHD;
                                TicketFormate += "</td>";
                                TicketFormate += "<td style='font-size: 12px; text-align: right; padding: 5px; border-right: 1px solid #000;border-top: 1px solid #000;background: #fff;'id='td_tcchd'>";
                                TicketFormate += (Convert.ToDecimal(fltFare.Rows[fd]["TCharge"]) * numberOfCHD).ToString();
                                CTranscharge = Convert.ToDecimal(fltFare.Rows[fd]["TCharge"]) * numberOfCHD;
                                TicketFormate += "</td>";
                                TicketFormate += "<td style='font-size: 12px; text-align: right; padding: 5px; border-right: 1px solid #000;border-top: 1px solid #000;background: #fff;'id='td_chdtot'>";
                                ChdTtlFare = (Convert.ToDecimal(fltFare.Rows[fd]["Total"]) * numberOfCHD);
                                TicketFormate += ChdTtlFare.ToString();
                                TicketFormate += "</td>";

                                TicketFormate += "</tr>";

                                initalChld += 1;
                            }
                            if (fltFare.Rows[fd]["PaxType"].ToString() == "INF" && initialift == 0)
                            {
                                int numberOfINF = FltPaxList.AsEnumerable().Where(x => x["PaxType"].ToString() == "INF").ToList().Count;
                                TicketFormate += "<tr>";
                                TicketFormate += "<td style='font-size: 12px; text-align: right; padding: 5px; border-right: 1px solid #000;border-top: 1px solid #000;background: #fff;'>";
                                TicketFormate += fltFare.Rows[fd]["PaxType"];
                                TicketFormate += "</td>";
                                TicketFormate += "<td style='font-size: 12px; text-align: right; padding: 5px; border-right: 1px solid #000;border-top: 1px solid #000;background: #fff;' id='td_infcnt'>" + numberOfINF + "</td>";
                                TicketFormate += "<td style='font-size: 12px; text-align: right; padding: 5px; border-right: 1px solid #000;border-top: 1px solid #000;background: #fff;'>";
                                TicketFormate += (Convert.ToDecimal(fltFare.Rows[fd]["BaseFare"]) * numberOfINF).ToString();
                                IBasefare = Convert.ToDecimal(fltFare.Rows[fd]["BaseFare"]) * numberOfINF;
                                TicketFormate += "</td>";
                                TicketFormate += "<td style='font-size: 12px; text-align: right; padding: 5px; border-right: 1px solid #000;border-top: 1px solid #000;background: #fff;'>";
                                TicketFormate += (Convert.ToDecimal(fltFare.Rows[fd]["Fuel"]) * numberOfINF).ToString();
                                Ifuel = Convert.ToDecimal(fltFare.Rows[fd]["Fuel"]) * numberOfINF;
                                TicketFormate += "</td>";
                                TicketFormate += "<td style='font-size: 12px; text-align: right; padding: 5px; border-right: 1px solid #000;border-top: 1px solid #000;background: #fff;'>";
                                TicketFormate += (Convert.ToDecimal(fltFare.Rows[fd]["Tax"]) * numberOfINF).ToString();
                                Itax = (Convert.ToDecimal(fltFare.Rows[fd]["Tax"]) * numberOfINF);
                                TicketFormate += "</td>";
                                TicketFormate += "<td style='font-size: 12px; text-align: right; padding: 5px; border-right: 1px solid #000;border-top: 1px solid #000;background: #fff;'>";
                                TicketFormate += (Convert.ToDecimal(fltFare.Rows[fd]["ServiceTax"]) * numberOfINF).ToString();
                                Igst = Convert.ToDecimal(fltFare.Rows[fd]["ServiceTax"]) * numberOfINF;
                                TicketFormate += "</td>";
                                TicketFormate += "<td style='font-size: 12px; text-align: right; padding: 5px; border-right: 1px solid #000;border-top: 1px solid #000;background: #fff;'>";
                                TicketFormate += (Convert.ToDecimal(fltFare.Rows[fd]["TFee"]) * numberOfINF).ToString();
                                ITransfee = Convert.ToDecimal(fltFare.Rows[fd]["TFee"]) * numberOfINF;
                                TicketFormate += "</td>";
                                TicketFormate += "<td style='font-size: 12px; text-align: right; padding: 5px; border-right: 1px solid #000;border-top: 1px solid #000;background: #fff;'>";
                                TicketFormate += (Convert.ToDecimal(fltFare.Rows[fd]["TCharge"]) * numberOfINF).ToString();
                                ITranscharge = Convert.ToDecimal(fltFare.Rows[fd]["TCharge"]) * numberOfINF;
                                TicketFormate += "</td>";
                                TicketFormate += "<td style='font-size: 12px; text-align: right; padding: 5px; border-right: 1px solid #000;border-top: 1px solid #000;background: #fff;'id='td_Inftot'>";
                                INFTtlFare = (Convert.ToDecimal(fltFare.Rows[fd]["Total"]) * numberOfINF);
                                TicketFormate += INFTtlFare.ToString();
                                TicketFormate += "</td>";
                                TicketFormate += "</tr>";
                                initialift += 1;
                            }
                        }
                        TicketFormate += "</tbody>";
                        TicketFormate += "</table>";
                        TicketFormate += "</td>";
                        TicketFormate += "</tr>";
                        TicketFormate += "</tbody>";
                        TicketFormate += "</table>";
                        decimal ticketcopymarkup = 0;
                        decimal ticketcopymarkupTC = 0;
                        if ((fltFare.Rows.Count > 0))
                        {
                            if ((Convert.ToString(fltFare.Rows[0]["ticketcopymarkupforTax"]) != "0"))
                                ticketcopymarkup = Convert.ToDecimal(fltFare.Rows[0]["ticketcopymarkupforTax"]);
                            if ((Convert.ToString(fltFare.Rows[0]["ticketcopymarkupforTC"]) != "0"))
                                ticketcopymarkupTC = Convert.ToDecimal(fltFare.Rows[0]["ticketcopymarkupforTC"]);
                        }
                        int numADT = FltPaxList.AsEnumerable().Where(x => x["PaxType"].ToString() == "ADT").ToList().Count;
                        int numCHD = FltPaxList.AsEnumerable().Where(x => x["PaxType"].ToString() == "CHD").ToList().Count;
                        int tpaxcount = numADT + numCHD;
                        ticketcopymarkup = ticketcopymarkup * tpaxcount;
                        ticketcopymarkupTC = ticketcopymarkupTC * tpaxcount;
                        TBasefare = ABasefare + CBasefare + IBasefare;
                        TTransfee = ATransfee + CTransfee + ITransfee;
                        TTranscharge = ATranscharge + CTranscharge + ITranscharge;
                        Tfuel = Afuel + Cfuel + Ifuel;
                        Ttax = Atax + Ctax + Itax + TTransfee + TTranscharge + ticketcopymarkup + ticketcopymarkupTC;
                        Tgst = Agst + Cgst + Igst;

                        fare = AdtTtlFare + ChdTtlFare + INFTtlFare + ticketcopymarkupTC + ticketcopymarkup;
                        //basetaxfarenrm.Value = Atax + Ctax + Itax + TTransfee + TTranscharge;
                    }
                    else
                    {
                        TicketFormate += "<tr>";
                        TicketFormate += "<td colspan='2' style='width:100%;'>";
                        TicketFormate += "<table style='width:100%;'>";


                        TicketFormate += "<tr>";
                        TicketFormate += "<td style='font-size: 10px; width: 50%; text-align: left; vertical-align: top; display:none;'id='td_perpaxtype'>" + FltPaxList.Rows[0]["PaxType"] + "</td>";
                        TicketFormate += "<td style='font-size: 10px; width: 50%; text-align: left; vertical-align: top;'>Base Fare</td>";
                        TicketFormate += "<td style='font-size: 10px; width: 50%; text-align: right; vertical-align: top;'>";
                        TicketFormate += fltFare.Rows[0]["BaseFare"].ToString();
                        TicketFormate += "</td>";
                        TicketFormate += "</tr>";
                        TicketFormate += "<tr>";
                        TicketFormate += "<td style='font-size: 10px; width: 50%; text-align: left; vertical-align: top;'>Fuel Surcharge</td>";
                        TicketFormate += "<td style='font-size: 10px; width: 50%; text-align: right; vertical-align: top;'>";
                        TicketFormate += fltFare.Rows[0]["Fuel"].ToString();
                        TicketFormate += "</td>";
                        TicketFormate += "</tr>";
                        TicketFormate += "<tr>";
                        TicketFormate += "<td style='font-size: 10px; width: 50%; text-align: left; vertical-align: top;'>Tax</td>";
                        TicketFormate += "<td style='font-size: 10px; width: 50%; text-align: right; vertical-align: top;' id='td_perpaxtax'>";
                        TicketFormate += fltFare.Rows[0]["Tax"].ToString();
                        TicketFormate += "</td>";
                        TicketFormate += "</tr>";
                        TicketFormate += "<tr>";
                        TicketFormate += "<td style='font-size: 10px; width: 50%; text-align: left; vertical-align: top;'>STax</td>";
                        TicketFormate += "<td style='font-size: 10px; width: 50%; text-align: right; vertical-align: top;'>";
                        TicketFormate += fltFare.Rows[0]["ServiceTax"].ToString();
                        TicketFormate += "</td>";
                        TicketFormate += "</tr>";
                        TicketFormate += "<tr >";
                        TicketFormate += "<td style='font-size: 10px; width: 50%; text-align: left; vertical-align: top;'>Trans Fee</td>";
                        TicketFormate += "<td style='font-size: 10px; width: 50%; text-align: right; vertical-align: top;'>";
                        TicketFormate += fltFare.Rows[0]["TFee"].ToString();
                        TicketFormate += "</td>";
                        TicketFormate += "</tr>";
                        TicketFormate += "<tr>";
                        TicketFormate += "<td style='font-size: 10px; width: 50%; text-align: left; vertical-align: top;'>Trans Charge</td>";
                        TicketFormate += "<td style='font-size: 10px; width: 50%; text-align: right; vertical-align: top;'id='td_perpaxtc'>";
                        TicketFormate += fltFare.Rows[0]["TCharge"].ToString();
                        TicketFormate += "</td>";
                        TicketFormate += "</tr>";
                        TicketFormate += "<tr>";

                        decimal ResuCharge = 0;
                        decimal ResuServiseCharge = 0;
                        decimal ResuFareDiff = 0;
                        if (Convert.ToString(FltHeaderList.Rows[0]["ResuCharge"]) != null && Convert.ToString(FltHeaderList.Rows[0]["ResuCharge"]) != "")
                        {
                            TicketFormate += "<tr>";
                            TicketFormate += "<td style='font-size: 10px; width: 50%; text-align: right; vertical-align: top;'>Reissue Charge</td>";
                            TicketFormate += "<td style='font-size: 10px; width: 50%; text-align: right; vertical-align: top;'>";
                            TicketFormate += FltHeaderList.Rows[0]["ResuCharge"].ToString();
                            ResuCharge = (Convert.ToDecimal(FltHeaderList.Rows[0]["ResuCharge"]));
                            TicketFormate += "</td>";
                            TicketFormate += "</tr>";
                        }
                        if (Convert.ToString(FltHeaderList.Rows[0]["ResuServiseCharge"]) != null && Convert.ToString(FltHeaderList.Rows[0]["ResuServiseCharge"]) != "")
                        {
                            TicketFormate += "<tr>";
                            TicketFormate += "<td style='font-size: 10px; width: 50%; text-align: right; vertical-align: top;'>Reissue Srv. Charge</td>";
                            TicketFormate += "<td style='font-size: 10px; width: 50%; text-align: right; vertical-align: top;'>";
                            TicketFormate += FltHeaderList.Rows[0]["ResuServiseCharge"].ToString();
                            ResuServiseCharge = (Convert.ToDecimal(FltHeaderList.Rows[0]["ResuServiseCharge"]));
                            TicketFormate += "</td>";
                            TicketFormate += "</tr>";
                        }
                        if (Convert.ToString(FltHeaderList.Rows[0]["ResuFareDiff"]) != null && Convert.ToString(FltHeaderList.Rows[0]["ResuFareDiff"]) != "")
                        {
                            TicketFormate += "<tr>";
                            TicketFormate += "<td style='font-size: 10px; width: 50%; text-align: right; vertical-align: top;'>Reissue Fare Diff</td>";
                            TicketFormate += "<td style='font-size: 10px; width: 50%; text-align: right; vertical-align: top;'>";
                            TicketFormate += FltHeaderList.Rows[0]["ResuFareDiff"].ToString();
                            ResuFareDiff = (Convert.ToDecimal(FltHeaderList.Rows[0]["ResuFareDiff"]));
                            TicketFormate += "</td>";
                            TicketFormate += "</tr>";
                        }
                        TicketFormate += "<td style='font-size: 11px; width: 50%; text-align: right; vertical-align: top;'>TOTAL</td>";
                        TicketFormate += "<td style='font-size: 11px; width: 50%; text-align: right; vertical-align: top;' id='td_totalfare'>";
                        fare = (Convert.ToDecimal(fltFare.Rows[0]["BaseFare"]) + Convert.ToDecimal(fltFare.Rows[0]["Fuel"]) + Convert.ToDecimal(fltFare.Rows[0]["Tax"]) + Convert.ToDecimal(fltFare.Rows[0]["ServiceTax"]) + Convert.ToDecimal(fltFare.Rows[0]["TCharge"]) + Convert.ToDecimal(fltFare.Rows[0]["TFee"]) + ResuCharge + ResuServiseCharge + ResuFareDiff);
                        TicketFormate += fare.ToString();
                        TicketFormate += "</td>";
                        TicketFormate += "</tr>";
                    }
                    if (fltMealAndBag.Rows.Count > 0)
                    {
                        TicketFormate += "<tr>";
                        TicketFormate += "<td colspan='8' style= 'background-color: #0b2759;width:100%;'>";
                        TicketFormate += "<table style='width:100%;'>";
                        TicketFormate += "<tbody style='border: 1px solid #000;'>";
                        TicketFormate += "<tr>";
                        TicketFormate += "<td style='background-color: " + BackClor + "; color: #fff; font-size: 12px; font-weight: bold; padding: 5px;' colspan='9'>Meal & Baggage Information</td>";
                        TicketFormate += "</tr>";
                        TicketFormate += "<tr style='border: 1px solid #000;'>";
                        TicketFormate += "<th style='font-size: 12px; text-align: left; padding: 5px; border-right: 1px solid #000;'>Pax Name</th>";
                        TicketFormate += "<th style='font-size: 12px; text-align: left; padding: 5px; border-right: 1px solid #000;'>Trip Type</th>";
                        TicketFormate += "<th style='font-size: 12px; text-align: left; padding: 5px; border-right: 1px solid #000;'>Meal Code</th>";
                        TicketFormate += "<th style='font-size: 12px; text-align: left; padding: 5px; border-right: 1px solid #000;'>Meal Price</th>";
                        TicketFormate += "<th style='font-size: 12px; text-align: left; padding: 5px; border-right: 1px solid #000;'>Baggage Code</th>";
                        TicketFormate += "<th style='font-size: 12px; text-align: left; padding: 5px; border-right: 1px solid #000;'>Baggage Price</th>";
                        TicketFormate += "<th style='font-size: 12px; text-align: left; padding: 5px; border-right: 1px solid #000;'>Total</th>";
                        TicketFormate += "</tr>";



                        for (int i = 0; i <= fltMealAndBag.Rows.Count - 1; i++)
                        {
                            TicketFormate += "<tr>";
                            TicketFormate += "<td style='font-size: 11px; width: 10%; text-align: right; vertical-align: top;'>";
                            TicketFormate += Convert.ToString(fltMealAndBag.Rows[i]["Name"]);
                            TicketFormate += "</td>";
                            TicketFormate += "<td style='font-size: 11px; width: 10%; text-align: right; vertical-align: top;'>";
                            TicketFormate += Convert.ToString(fltMealAndBag.Rows[i]["TripType"]);
                            TicketFormate += "</td>";
                            TicketFormate += "<td style='font-size: 11px; width: 10%; text-align: right; vertical-align: top;'>";
                            TicketFormate += Convert.ToString(fltMealAndBag.Rows[i]["MealCode"]);
                            TicketFormate += "</td>";
                            TicketFormate += "<td style='font-size: 11px; width: 10%; text-align: right; vertical-align: top;'>";
                            TicketFormate += Convert.ToString(fltMealAndBag.Rows[i]["MealPrice"]);
                            TicketFormate += "</td>";
                            TicketFormate += "<td style='font-size: 11px; width: 10%; text-align: right; vertical-align: top;'>";
                            TicketFormate += Convert.ToString(fltMealAndBag.Rows[i]["BaggageCode"]);
                            TicketFormate += "</td>";
                            TicketFormate += "<td style='font-size: 11px; width: 10%; text-align: right; vertical-align: top;'>";
                            TicketFormate += Convert.ToString(fltMealAndBag.Rows[i]["BaggagePrice"]);
                            TicketFormate += "</td>";
                            TicketFormate += "<td style='font-size: 11px; width: 10%; text-align: right; vertical-align: top;'>";
                            MealBagTotalPrice += Convert.ToDecimal(fltMealAndBag.Rows[i]["TotalPrice"]);
                            TicketFormate += Convert.ToString(fltMealAndBag.Rows[i]["TotalPrice"]);
                            TicketFormate += "</td>";
                            TicketFormate += "</tr>";
                        }
                        TicketFormate += "</tbody>";
                        TicketFormate += "</table>";
                        TicketFormate += "</td>";
                        TicketFormate += "</tr>";
                        fare = AdtTtlFare + ChdTtlFare + INFTtlFare + MealBagTotalPrice;
                    }
                    TicketFormate += "<table id='disfareinfoheader'  style='border: 1px solid #0b2759; font-size: 12px; padding: 0px !important; width: 100%;' >";
                    TicketFormate += "<tbody>";
                    TicketFormate += "<tr id='TR_FareInformation1'>";
                    TicketFormate += "<td class='pri' colspan='9' style='background-color: " + BackClor + "; color: #fff; font-size: 12px; font-weight: bold; padding: 5px;'>Redemption Details</td>";
                    TicketFormate += "</tr>";
                    TicketFormate += "</tbody>";
                    TicketFormate += "</table>";
                    TicketFormate += "<table id='disfareinfo' style='border: 1px solid #0b2759; font-size: 12px; padding: 0px !important; width: 100%;'>";
                    TicketFormate += "<tbody>";
                    TicketFormate += "<tr>";
                    TicketFormate += "<td colspan='9' style='font-size: 12px; padding: 5px; width: 30%' class='farehideshow'>";
                    TicketFormate += "<table style='font-size: 12px; padding: 5px; width: 100%;'>";
                    TicketFormate += "<tbody>";
                    TicketFormate += "<tr>";
                    TicketFormate += "<td style='font-size: 12px; color: #424242; text-align: left; padding: 5px; font-weight: bold;border-left: 1px solid #000 !important;border-top: 1px solid #000 !important'>Base fare</th>";
                    TicketFormate += "<td style='font-size: 12px; text-align: right;padding-right: 5px; vertical-align: top;font-weight: bold;border-right: 1px solid #000 !important;border-top: 1px solid #000 !important'>" + TBasefare.ToString() + "</td>";
                    TicketFormate += "</tr>";
                    TicketFormate += "<tr style='border: 1px solid #000;'>";
                    TicketFormate += "<td class='pri2' style='font-size: 12px; color: #424242; text-align: left; padding: 5px; font-weight: bold;border-left: 1px solid #000 !important;border-top: 1px solid #000 !important'>Fuel Surcharge</th>";
                    TicketFormate += "<td class='pri2' style='font-size: 12px; text-align: right; padding-right: 5px;vertical-align: top;font-weight: bold;border-right: 1px solid #000 !important;border-top: 1px solid #000 !important'>" + Tfuel.ToString() + "</td>"; // 'ttt
                    TicketFormate += "</tr>";
                    TicketFormate += "<tr style='border: 1px solid #000;'>";
                    TicketFormate += "<td style='font-size: 12px; color: #424242; text-align: left; padding: 5px; font-weight: bold;border-left: 1px solid #000 !important;border-top: 1px solid #000 !important'>Tax</th>";
                    TicketFormate += "<td style='font-size: 12px; text-align: right;padding-right: 5px; vertical-align: top;font-weight: bold;border-right: 1px solid #000 !important;border-top: 1px solid #000 !important' id='td_taxadt' class='taxclass'>" + Ttax.ToString() + "</td>"; // 'ttt
                    TicketFormate += "</tr>";
                    TicketFormate += "<tr style='border: 1px solid #000;'>";
                    TicketFormate += "<td class='pri2' style='font-size: 12px; color: #424242; text-align: left; padding: 5px; font-weight: bold;border-left: 1px solid #000 !important;border-top: 1px solid #000 !important'>Supp.SGST</th>";
                    TicketFormate += "<td class='pri2' style='font-size: 12px; text-align: right;padding-right: 5px; vertical-align: top;font-weight: bold;border-right: 1px solid #000 !important;border-top: 1px solid #000 !important'>" + Tgst.ToString() + "</td>"; // 'ttt
                    TicketFormate += "</tr>";
                    TicketFormate += "<tr style='border: 1px solid #000;'>";
                    TicketFormate += "<td style='font-size: 12px; color: #424242; text-align: left; padding: 5px; font-weight: bold;border-left: 1px solid #000 !important;border-top: 1px solid #000 !important'>Supp.CGST</th>";
                    TicketFormate += "<td style='font-size: 12px; text-align: right; padding-right: 5px; vertical-align: top;font-weight: bold;border-right: 1px solid #000 !important;border-top: 1px solid #000 !important'>0</td>"; // 'ttt
                    TicketFormate += "</tr>";
                    TicketFormate += "<tr style='border: 1px solid #000;'>";
                    TicketFormate += "<td class='pri2' style='font-size: 12px; color: #424242; text-align: left; padding: 5px; font-weight: bold;border-left: 1px solid #000 !important;border-top: 1px solid #000 !important'>Grand Total</th>";
                    TicketFormate += "<td class='pri2' style='font-size: 12px;padding-right: 5px; text-align: right; vertical-align: top;font-weight: bold;border-right: 1px solid #000 !important;border-top: 1px solid #000 !important' id='td_grandtot'><span style='font-weight:bold;color:green'>" + fare.ToString() + "</span</td>"; // 'ttt
                    TicketFormate += "</tr>";
                    TicketFormate += "</tbody>";
                    TicketFormate += "</table>";
                    TicketFormate += "</td>";
                    TicketFormate += "<td colspan='4' style='display:none;font-size: 12px; padding: 5px; width: 100%' class='farehideshow'>";
                    TicketFormate += " <table style='width:50%;margin-top: -82px;float: right;border: 1px solid #000;'>";
                    TicketFormate += " <tbody style='border: 1px solid #000;'>";
                    TicketFormate += " <tr style='border: 1px solid #000;'><td style='font-weight: bold;font-size: 12px;'>Passenger Information:</td><td style=''></td> </tr>";
                    TicketFormate += " <tr><td class='pri2' style='font-weight: bold;font-size: 12px;'>Email:</td><td class='pri2' style='font-weight: bold;font-size: 12px;'>" + FltHeaderList.Rows[0]["PgEmail"] + "</td></tr>";
                    TicketFormate += " <tr><td style='font-weight: bold;font-size: 12px;'>Contact No:</td><td style='font-weight: bold;font-size: 12px;'>" + FltHeaderList.Rows[0]["PgMobile"] + "</td></tr>";
                    TicketFormate += " </tbody>";
                    TicketFormate += " </table>";
                    TicketFormate += "</td>";
                    TicketFormate += "</tr >";
                    TicketFormate += " </tbody>";
                    TicketFormate += " </table>";
                    TicketFormate += "<table style='border: 1px solid #0b2759; font-size: 12px; padding: 0px !important; width: 100%;'>";
                    TicketFormate += "<tbody>";
                    TicketFormate += "<tr>";
                    TicketFormate += "<td colspan='8'>";
                    TicketFormate += "<ul>";
                    TicketFormate += "</ul>";
                    TicketFormate += "</td>";
                    TicketFormate += "</tr>";
                    TicketFormate += "<tr>";
                    TicketFormate += "<td class='pri' colspan='8' style='background-color: " + BackClor + "; color: #fff; font-size: 11px; font-weight: bold; padding: 5px;'>IMPORTANT INFORMATION :</td>";
                    TicketFormate += "</tr>";
                    TicketFormate += "<tr>";
                    TicketFormate += "<td colspan='8'>";
                    TicketFormate += "<ul>";
                    TicketFormate += "<li style='font-size: 10.5px;margin-left: 15px !important;list-style: disc !important;'>For any queries or communication with tripforo regarding this booking, please use the Booking ID as a reference.</li>";
                    TicketFormate += "<li style='font-size: 10.5px;margin-left: 15px !important;list-style: disc !important;'>Please note that for all domestic flights, check-in counters close 60 minutes prior to flight departure.</li>";
                    TicketFormate += "<li style='font-size: 10.5px;margin-left: 15px !important;list-style: disc !important;'>It is mandatory for the passenger to carry a valid photo ID proof in order to enter the airport and show at the time of check-in. Permissible ID proofs include - Aadhaar Card, Passport or any other government recognized ID proof. For infant travellers (0-2 yrs), it is mandatory to carry the birth certificate as a proof</li>";
                    TicketFormate += "<li style='font-size: 10.5px;margin-left: 15px !important;list-style: disc !important;'>Kindly carry a copy of your e-ticket on a tablet/ mobile/ laptop or a printed copy of the ticket to enter the airport and show at the time of check-in.</li>";
                    TicketFormate += "</ul>";
                    TicketFormate += " </td>";
                    TicketFormate += "</tr>";
                    TicketFormate += "</tbody>";
                    TicketFormate += "</table>";
                    TicketFormate += "</div>";

                    //TicketFormate += " <script type='text/javascript'>";
                    //TicketFormate += " decodeURI(window.location.search).substring(1).split('&');";
                    //TicketFormate += "  var btype = 'code128';";
                    //TicketFormate += "  var renderer = 'css';";
                    //TicketFormate += "  var quietZone = false;";
                    //TicketFormate += " if ($('#quietzone').is(':checked') || $('#quietzone').attr('checked'))";
                    //TicketFormate += "  { quietZone = true; }";
                    //TicketFormate += "  var settings = { output: renderer, bgColor: '#FFFFFF', color: '#000000', barWidth: '1', barHeight: '110', moduleSize: '5', posX: '10', posY: '20', addQuietZone: '1' };";
                    //TicketFormate += "   $('#canvasTarget').hide();";
                    //TicketFormate += "  $('#barcodeTarget').html('').show().barcode('" + OrderId + "', btype, settings);";
                    //TicketFormate += "  </script>";
                }

                return TicketFormate;
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return TicketFormate;
        }

        public static string InvoiceCopy(string orderId)
        {
            double adult = 0;
            double child = 0;
            double infant = 0;
            double totalfare = 0;
            double adulttax = 0;
            double childtax = 0;
            double infanttax = 0;
            double totalfaretax = 0;
            double total = 0;
            double GrandTotal = 0;
            double TotalST = 0;
            double TDS = 0;
            double CB = 0;
            string gdspnr = "";
            string result = "";
            DataSet ds = new DataSet();
            DataTable dt = new DataTable();

            try
            {
                if ((orderId != "" && orderId != null))
                {
                    string id = orderId; // HttpContext.Current.Request.QueryString("OrderId").ToString()
                    ds = GetInvoice(id);
                    dt = ds.Tables[0];
                    DataTable dtflt = new DataTable();
                    dtflt = ds.Tables[1];

                    string projID = "";
                    string bookedBy = "";
                    string billNo = "";
                    string ReissueId = "";
                    if (!string.IsNullOrEmpty(dt.Rows[0]["ProjectID"].ToString()))
                    {
                        projID = dt.Rows[0]["ProjectID"].ToString();
                    }
                    if (!string.IsNullOrEmpty(dt.Rows[0]["BookedBy"].ToString()))
                    {
                        bookedBy = dt.Rows[0]["BillNoCorp"].ToString();
                    }
                    if (!string.IsNullOrEmpty(dt.Rows[0]["BillNoCorp"].ToString()))
                    {
                        billNo = dt.Rows[0]["BillNoCorp"].ToString();
                    }
                    if (!string.IsNullOrEmpty(dt.Rows[0]["ResuId"].ToString()))
                    {
                        projID = dt.Rows[0]["ResuId"].ToString();
                    }
                    double mgtFee = 0;
                    DataTable dtAAdd;
                    dtAAdd = GetAgencyDetails(dt.Rows[0]["AgentId"].ToString()).Tables[0];
                    bool MgtFeeVisibleStatus = false;
                    bool IsCorp = false;
                    //if (dt.Rows[0]["IsCorp"] != System.DBNull.Value)
                    //{
                    //    if (Convert.ToBoolean(dtAAdd.Rows[0]["IsCorp"]))
                    //    {
                    //        MgtFeeVisibleStatus = true;
                    //        IsCorp = true;
                    //    }
                    //}
                    string my_table = "";

                    DataTable dtaddress = new DataTable();
                    if ((IsCorp == true))
                    {
                        dtaddress = GetCompanyAddress().Tables[0];
                        my_table += " <table width='100%' border='0' cellpadding='0' cellspacing='0' align='center' class='fullTable' bgcolor='#e1e1e1'>";
                        my_table += "<tr><td height='20'></td></tr>";
                        my_table += "<tr>";
                        my_table += "<td>";
                        my_table += "<table width='100%' border='0' cellpadding='0' cellspacing='0' align='center' class='fullTable' bgcolor='#ffffff' style='border-radius: 10px 10px 0 0;'>";
                        my_table += "<tr class='hiddenMobile'>";
                        my_table += "<td height='40'></td>";
                        my_table += "</tr>";
                        my_table += "<tr class='visibleMobile'>";
                        my_table += "<td height='30'></td>";
                        my_table += "</tr>";
                        my_table += "<tr>";
                        my_table += "<td>";
                        my_table += "<table width='100%' border='0' cellpadding='0' cellspacing='0' align='center' class='fullPadding'>";
                        my_table += "<tbody>";
                        my_table += "<tr>";
                        my_table += "<td>";
                        my_table += "<table width='220' border='0' cellpadding='0' cellspacing='0' align='left' class='col'>";
                        my_table += "<tbody>";
                        my_table += "<tr>";
                        my_table += "<td style='padding-left:10px;'> <img src='http://www.supah.it/dribbble/017/logo.png' width='32' height='32' alt='logo' border='0' /></td>";
                        my_table += "</tr>";
                        my_table += "<tr class='hiddenMobile'>";
                        my_table += "<td height='40'></td>";
                        my_table += "</tr>";
                        my_table += "<tr class='visibleMobile'>";
                        my_table += "<td height='20'></td>";
                        my_table += "</tr>";
                        my_table += "<tr>";
                        my_table += "<td style='font-size: 12px; color: #5b5b5b; line-height: 18px; vertical-align: top; text-align: left;'>Hello, Philip Brooks.<br> Thank you for shopping from our store and for your order.</td>";
                        my_table += "</tr>";
                        my_table += "</tbody>";
                        my_table += "</table>";
                        my_table += "<table width='220' border='0' cellpadding='0' cellspacing='0' align='right' class='col'>";
                        my_table += "<tbody>";
                        my_table += "<tr class='visibleMobile'>";
                        my_table += "<td height='20'></td>";
                        my_table += "</tr>";
                        my_table += "<tr>";
                        my_table += "<td height='5'></td>";
                        my_table += "</tr>";
                    }
                    else
                    {
                        dtaddress = GetCompanyAddress().Tables[0];
                        my_table += "<table width='100%' border='0' cellpadding='0' cellspacing='0' align='center' class='fullTable' bgcolor='#ffffff' style='border-radius: 10px 10px 0 0;'>";
                        my_table += "<tr class='hiddenMobile'>";
                        my_table += "<td height='20'></td>";
                        my_table += "</tr>";
                        my_table += "<tr>";
                        my_table += "<td>";
                        my_table += "<table width='100%' border='0' cellpadding='0' cellspacing='0' align='center' class='fullPadding'>";
                        my_table += "<tbody>";
                        my_table += "<tr>";
                        my_table += "<td>";
                        my_table += "<table width='220' border='0' cellpadding='0' cellspacing='0' align='left' class='col'>";
                        my_table += "<tbody>";
                        my_table += "<tr>";
                        my_table += "<td style='padding-left:10px;'> <img src='../Content/images/logo(ft).png' width='100' height='auto' alt='logo' border='0' /></td>";
                        my_table += "</tr>";
                        my_table += "<tr>";
                        my_table += "<td style='font-size: 12px;padding-left:10px;color: #5b5b5b; line-height: 18px; vertical-align: top; text-align: left;'>" + dtaddress.Rows[0]["COMPANYNAME"] + "<br> " + dtaddress.Rows[0]["COMPANYADDRESS"] + "<br>Phone : " + dtaddress.Rows[0]["PHONENO"] + "<br>Email : " + dtaddress.Rows[0]["EMAIL"] + "</td>";
                        my_table += "</tr>";
                        my_table += "</tbody>";
                        my_table += "</table>";
                    }
                    my_table += "</td>";
                    my_table += "</tr>";
                    my_table += "</table>";
                    my_table += "</td>";
                    my_table += "</tr>";
                    my_table += "</table>";
                    my_table += "<table width='100%' border='0' cellpadding='0' cellspacing='0' align='center' class='fullTable' bgcolor='#e1e1e1'>";
                    my_table += "<tbody>";
                    my_table += "<tr>";
                    my_table += "<td>";
                    my_table += "<table width='100%' border='0' cellpadding='0' cellspacing='0' align='center' class='fullTable' bgcolor='#ffffff'>";
                    my_table += "<tbody>";
                    my_table += "<tr>";
                    my_table += "<tr class='hiddenMobile'>";
                    my_table += "<td height='20'></td>";
                    my_table += "</tr>";
                    my_table += "<tr>";
                    my_table += "<td>";
                    my_table += "<table width='97%' border='0' cellpadding='0' cellspacing='8' align='center' class='fullPadding'>";
                    my_table += "<tbody>";



                    if ((IsCorp == true))
                    {
                        my_table += "<tr>";
                        my_table += "<th style='font-size: 12px; color: #5b5b5b; font-weight: normal; line-height: 1; vertical-align: top; padding: 0 10px 7px 0;' align='left'>CERATED DATE</th>";
                        my_table += "<th style='font-size: 12px; color: #5b5b5b; font-weight: normal; line-height: 1; vertical-align: top; padding: 0 0 7px;' align='left'><small>PAX NAME</small></th>";
                        my_table += "<th style='font-size: 12px; color: #5b5b5b; font-weight: normal; line-height: 1; vertical-align: top; padding: 0 0 7px;' align='center'>TICKET NO</th>";
                        my_table += "<th style='font-size: 12px; color: #1e2b33; font-weight: normal; line-height: 1; vertical-align: top; padding: 0 0 7px;' align='right'>Airline</th>";
                        my_table += "<th style='font-size: 12px; color: #5b5b5b; font-weight: normal; line-height: 1; vertical-align: top; padding: 0 0 7px;' align='center'>SECTORS</th>";
                        my_table += "<th style='font-size: 12px; color: #1e2b33; font-weight: normal; line-height: 1; vertical-align: top; padding: 0 0 7px;' align='right'>DEP DATE</th>";
                        my_table += "<th style='font-size: 12px; color: #1e2b33; font-weight: normal; line-height: 1; vertical-align: top; padding: 0 0 7px;' align='right'>AIRLINE</th>";
                        my_table += "<th style='font-size: 12px; color: #1e2b33; font-weight: normal; line-height: 1; vertical-align: top; padding: 0 0 7px;' align='right'>FLIGHT NO</th>";
                        my_table += "<th style='font-size: 12px; color: #1e2b33; font-weight: normal; line-height: 1; vertical-align: top; padding: 0 0 7px;' align='right'>FARE</th>";
                        my_table += "<th style='font-size: 12px; color: #1e2b33; font-weight: normal; line-height: 1; vertical-align: top; padding: 0 0 7px;' align='right'>TAX</th>";
                        my_table += "<th style='font-size: 12px; color: #1e2b33; font-weight: normal; line-height: 1; vertical-align: top; padding: 0 0 7px;' align='right'>TOTAL</th>";
                        my_table += "</tr>";
                    }
                    else
                    {
                        my_table += "<tr>";
                        my_table += "<th style='font-size: 12px; color: #5b5b5b; font-weight: normal; line-height: 1; vertical-align: top; padding: 0 10px 7px 0;'  align='left'>Pax</th>";
                        my_table += "<th style='font-size: 12px; color: #5b5b5b; font-weight: normal; line-height: 1; vertical-align: top; padding: 0 0 7px;' align='left'><small>Ticket No.</small></th>";
                        my_table += "<th style='font-size: 12px; color: #5b5b5b; font-weight: normal; line-height: 1; vertical-align: top; padding: 0 0 7px;' align='center'>Airline</th>";
                        my_table += "<th style='font-size: 12px; color: #1e2b33; font-weight: normal; line-height: 1; vertical-align: top; padding: 0 0 7px;' align='right'>PNR</th>";
                        my_table += "<th style='font-size: 12px; color: #5b5b5b; font-weight: normal; line-height: 1; vertical-align: top; padding: 0 0 7px;' align='center'>APNR</th>";
                        my_table += "<th style='font-size: 12px; color: #1e2b33; font-weight: normal; line-height: 1; vertical-align: top; padding: 0 0 7px;' align='center'>Sectors</th>";
                        my_table += "<th style='font-size: 12px; color: #1e2b33; font-weight: normal; line-height: 1; vertical-align: top; padding: 0 0 7px;' align='center'>Dep Date</th>";
                        my_table += "<th style='font-size: 12px; color: #1e2b33; font-weight: normal; line-height: 1; vertical-align: top; padding: 0 0 7px;' align='center'>Create Date</th>";
                        my_table += "<th style='font-size: 12px; color: #1e2b33; font-weight: normal; line-height: 1; vertical-align: top; padding: 0 0 7px;' align='center'>Fare</th>";
                        my_table += "<th style='font-size: 12px; color: #1e2b33; font-weight: normal; line-height: 1; vertical-align: top; padding: 0 0 7px;' align='center'>Tax</th>";
                        my_table += "<th style='font-size: 12px; color: #1e2b33; font-weight: normal; line-height: 1; vertical-align: top; padding: 0 0 7px;' align='center'>Total</th>";

                        my_table += "</tr>";
                    }
                    my_table += "<tr>";
                    my_table += "<td height='1' style='background: #bebebe;' colspan='11'></td>";
                    my_table += "</tr>";
                    my_table += "<tr>";
                    my_table += "<td height='10' colspan='11'></td>";
                    my_table += "</tr>";



                    my_table += "<tr>";
                    foreach (DataRow dr in dt.Rows)
                    {
                        my_table += "<tr class='InvoiceText' align='center'>";
                        if ((IsCorp == true))
                        {
                            if ((ReissueId == ""))
                            {
                                my_table += "<td>" + dr["Createdate"].ToString() + " </td>";
                                my_table += "<td>" + dr["title"].ToString() + " " + dr["fname"].ToString() + "" + dr["mname"] + " " + dr["lname"] + "</td>";
                                my_table += "<td>" + dr["TicketNumber"].ToString() + "</td>";


                                my_table += "<td style='font-size: 11px; width: 10%; text-align: left; font-weight: bold; vertical-align: top;'>";
                                my_table += "<img alt='' src='http://flywidus.co/AirLogo/sm" + dr["VC"] + ".gif' ></img>";
                                my_table += dr["VC"].ToString() + " " + dtflt.Rows[0]["FltNumber"].ToString();
                                my_table += "</td>";



                                my_table += "<td>" + dr["Sector"].ToString() + "</td>";
                                gdspnr = dr["GDSPnr"].ToString();


                                string strDepdt = Convert.ToString(dtflt.Rows[0]["DepDate"]);
                                try
                                {
                                    strDepdt = (strDepdt.Length == 8 ? Left(strDepdt, 4) + "-" + Mid(strDepdt, 4, 2) + "-" + Right(strDepdt, 2) : "20" + Right(strDepdt, 2) + "-" + Mid(strDepdt, 2, 2) + "-" + Left(strDepdt, 2));
                                    DateTime deptdt = Convert.ToDateTime(strDepdt);
                                    strDepdt = deptdt.ToString("dd/MMM/yy").Replace('-', '/');
                                }
                                catch (Exception ex)
                                {
                                    ex.ToString();
                                }
                                my_table += "<td>" + strDepdt + "</td>";
                                // my_table += "<td>" & dtflt.Rows[0]("DepDate").ToString() & "</td>"

                                my_table += "<td>" + dr["VC"].ToString() + "</td>";
                                my_table += "<td>" + dtflt.Rows[0]["FltNumber"].ToString() + "</td>";
                                if (dr["paxtype"].ToString() == "ADT")
                                {
                                    my_table += "<td>" + dr["BaseFare"].ToString() + " </td>";
                                    my_table += "<td>" + dr["TotalTax"].ToString() + " </td>";
                                    my_table += "<td>" + Convert.ToDouble(dr["BaseFare"].ToString()) + Convert.ToDouble(dr["TotalTax"].ToString()) + " </td>";
                                    adult += Convert.ToDouble(dr["basefare"].ToString());
                                    adulttax += Convert.ToDouble(dr["totaltax"].ToString());
                                }
                                if (dr["paxtype"].ToString() == "CHD")
                                {
                                    my_table += "<td>" + dr["BaseFare"].ToString() + " </td>";
                                    my_table += "<td>" + dr["TotalTax"].ToString() + " </td>";
                                    my_table += "<td>" + Convert.ToDouble(dr["BaseFare"].ToString()) + Convert.ToDouble(dr["TotalTax"].ToString()) + " </td>";
                                    child += Convert.ToDouble(dr["basefare"].ToString());
                                    childtax += Convert.ToDouble(dr["totaltax"].ToString());
                                }
                                if (dr["paxtype"].ToString() == "INF")
                                {
                                    my_table += "<td>" + dr["BaseFare"].ToString() + " </td>";
                                    my_table += "<td>" + dr["TotalTax"].ToString() + " </td>";
                                    my_table += "<td>" + Convert.ToDouble(dr["BaseFare"].ToString()) + Convert.ToDouble(dr["TotalTax"].ToString()) + " </td>";
                                    infant += Convert.ToDouble(dr["basefare"].ToString());
                                    infanttax += Convert.ToDouble(dr["totaltax"].ToString());
                                }
                            }
                            else
                            {
                                my_table += "<td>" + dr["Createdate"].ToString() + " </td>";
                                my_table += "<td>" + dr["title"].ToString() + " " + dr["fname"].ToString() + "" + dr["mname"] + " " + dr["lname"] + "</td>";
                                my_table += "<td>" + dr["TicketNumber"].ToString() + "</td>";
                                my_table += "<td>" + dr["Sector"].ToString() + "</td>";
                                gdspnr = dr["GDSPnr"].ToString();

                                string strDepdt = Convert.ToString(dtflt.Rows[0]["DepDate"]);
                                try
                                {
                                    strDepdt = (strDepdt.Length == 8 ? Left(strDepdt, 4) + "-" + Mid(strDepdt, 4, 2) + "-" + Right(strDepdt, 2) : "20" + Right(strDepdt, 2) + "-" + Mid(strDepdt, 2, 2) + "-" + Left(strDepdt, 2));
                                    DateTime deptdt = Convert.ToDateTime(strDepdt);
                                    strDepdt = deptdt.ToString("dd/MMM/yy").Replace('-', '/');
                                    string depDay = Convert.ToString(deptdt.DayOfWeek);
                                    strDepdt = strDepdt.Split('/')[0] + " " + strDepdt.Split('/')[1] + " " + strDepdt.Split('/')[2];

                                }
                                catch (Exception ex)
                                {
                                    ex.ToString();
                                }
                                my_table += "<td>" + strDepdt + "</td>";
                                my_table += "<td>" + dr["VC"].ToString() + "</td>";
                                my_table += "<td>" + dtflt.Rows[0]["FltNumber"].ToString() + "</td>";
                                if (dr["paxtype"].ToString() == "ADT")
                                {
                                    my_table += "<td>" + dr["ResuFareDiff"].ToString() + " </td>";
                                    my_table += "<td>0</td>";
                                    my_table += "<td>" + Convert.ToDouble(dr["ResuFareDiff"].ToString()) + "</td>";
                                    adult += Convert.ToDouble(dr["ResuFareDiff"].ToString());
                                    adulttax += 0;
                                }
                                if (dr["paxtype"].ToString() == "CHD")
                                {
                                    my_table += "<td>" + dr["ResuFareDiff"].ToString() + " </td>";
                                    my_table += "<td>0</td>";
                                    my_table += "<td>" + Convert.ToDouble(dr["ResuFareDiff"].ToString()) + "</td>";
                                    child += Convert.ToDouble(dr["ResuFareDiff"].ToString());
                                    childtax += 0;
                                }
                                if (dr["paxtype"].ToString() == "INF")
                                {
                                    my_table += "<td>" + dr["ResuFareDiff"].ToString() + " </td>";
                                    my_table += "<td>0</td>";
                                    my_table += "<td>" + Convert.ToDouble(dr["ResuFareDiff"].ToString()) + "</td>";
                                    infant += Convert.ToDouble(dr["ResuFareDiff"].ToString());
                                    infanttax += 0;
                                }
                            }
                        }
                        else if ((ReissueId == ""))
                        {
                            my_table += "<tr>";

                            my_table += "<td style='font-size: 12px; color: #ff0000;  line-height: 18px;  vertical-align: top; padding:10px 0;' class='article'>" + dr["title"].ToString() + " " + dr["fname"].ToString() + "" + dr["mname"] + " " + dr["lname"] + "</td>";
                            my_table += "<td style='font-size: 12px; color: #646a6e;  line-height: 18px;  vertical-align: top; padding:10px 0;'><small>" + dr["TicketNumber"].ToString() + ".</small></td>";
                            my_table += "<td style='font-size: 12px; color: #646a6e;  line-height: 18px;  vertical-align: top; padding:10px 0;'>";
                            my_table += dr["VC"].ToString() + " " + dtflt.Rows[0]["FltNumber"].ToString();
                            my_table += "</td>";
                            my_table += "<td style='font-size: 12px; color: #1e2b33;  line-height: 18px;  vertical-align: top; padding:10px 0;'>" + dr["GDSPnr"].ToString() + "</td>";
                            gdspnr = dr["GDSPnr"].ToString();
                            my_table += "<td style='font-size: 12px; color: #1e2b33;  line-height: 18px;  vertical-align: top; padding:10px 0;'>" + dr["AirlinePnr"].ToString() + "</td>";
                            my_table += "<td style='font-size: 12px; color: #1e2b33;  line-height: 18px;  vertical-align: top; padding:10px 0;'>" + dr["Sector"].ToString() + "</td>";
                            string strDepdt = Convert.ToString(dtflt.Rows[0]["DepDate"]);
                            try
                            {
                                strDepdt = (strDepdt.Length == 8 ? Left(strDepdt, 4) + "-" + Mid(strDepdt, 4, 2) + "-" + Right(strDepdt, 2) : "20" + Right(strDepdt, 2) + "-" + Mid(strDepdt, 2, 2) + "-" + Left(strDepdt, 2));
                                DateTime deptdt = Convert.ToDateTime(strDepdt);
                                strDepdt = deptdt.ToString("dd/MMM/yy").Replace('-', '/');
                                string depDay = Convert.ToString(deptdt.DayOfWeek);
                                strDepdt = strDepdt.Split('/')[0] + " " + strDepdt.Split('/')[1] + " " + strDepdt.Split('/')[2];

                            }
                            catch (Exception ex)
                            {
                                ex.ToString();
                            }
                            my_table += "<td style='font-size: 12px; color: #1e2b33;  line-height: 18px;  vertical-align: top; padding:10px 0;'>" + strDepdt + "</td>";
                            my_table += "<td style='font-size: 12px; color: #1e2b33;  line-height: 18px;  vertical-align: top; padding:10px 0;'>" + dr["Createdate"].ToString() + " </td>";
                            if (dr["paxtype"].ToString() == "ADT")
                            {
                                my_table += "<td style='font-size: 12px; color: #1e2b33;  line-height: 18px;  vertical-align: top; padding:10px 0;'>" + dr["BaseFare"].ToString() + " </td>";
                                my_table += "<td style='font-size: 12px; color: #1e2b33;  line-height: 18px;  vertical-align: top; padding:10px 0;'>" + dr["TotalTax"].ToString() + " </td>";
                                my_table += "<td style='font-size: 12px; color: #1e2b33;  line-height: 18px;  vertical-align: top; padding:10px 0;'>" + Convert.ToDouble(Convert.ToDouble(dr["BaseFare"]) + Convert.ToDouble(dr["TotalTax"])) + " </td>";
                                adult += Convert.ToDouble(dr["basefare"]);
                                adulttax += Convert.ToDouble(dr["totaltax"]);
                            }
                            if (dr["paxtype"].ToString() == "CHD")
                            {
                                my_table += "<td style='font-size: 12px; color: #1e2b33;  line-height: 18px;  vertical-align: top; padding:10px 0;'>" + dr["BaseFare"].ToString() + " </td>";
                                my_table += "<td style='font-size: 12px; color: #1e2b33;  line-height: 18px;  vertical-align: top; padding:10px 0;'>" + dr["TotalTax"].ToString() + " </td>";
                                my_table += "<td style='font-size: 12px; color: #1e2b33;  line-height: 18px;  vertical-align: top; padding:10px 0;'>" + Convert.ToDouble(Convert.ToDouble(dr["BaseFare"]) + Convert.ToDouble(dr["TotalTax"])) + " </td>";
                                child += Convert.ToDouble(dr["basefare"]);
                                childtax += Convert.ToDouble(dr["totaltax"]);
                            }
                            if (dr["paxtype"].ToString() == "INF")
                            {
                                my_table += "<td style='font-size: 12px; color: #1e2b33;  line-height: 18px;  vertical-align: top; padding:10px 0;'>" + dr["BaseFare"].ToString() + " </td>";
                                my_table += "<td style='font-size: 12px; color: #1e2b33;  line-height: 18px;  vertical-align: top; padding:10px 0;'>" + dr["TotalTax"].ToString() + " </td>";
                                my_table += "<td style='font-size: 12px; color: #1e2b33;  line-height: 18px;  vertical-align: top; padding:10px 0;'>" + Convert.ToDouble(Convert.ToDouble(dr["BaseFare"]) + Convert.ToDouble(dr["TotalTax"])) + " </td>";
                                infant += Convert.ToDouble(dr["basefare"]);
                                infanttax += Convert.ToDouble(dr["totaltax"]);
                            }
                        }
                        else
                        {
                            my_table += "<td style='font-size: 12px; color: #ff0000;  line-height: 18px;  vertical-align: top; padding:10px 0;' class='article'>" + dr["title"].ToString() + " " + dr["fname"].ToString() + "" + dr["mname"] + " " + dr["lname"] + "</td>";
                            my_table += "<td style='font-size: 12px; color: #646a6e;  line-height: 18px;  vertical-align: top; padding:10px 0;'><small>" + dr["TicketNumber"].ToString() + ".</small></td>";
                            my_table += "<td style='font-size: 12px; color: #646a6e;  line-height: 18px;  vertical-align: top; padding:10px 0;'>";
                            my_table += dr["VC"].ToString() + " " + dtflt.Rows[0]["FltNumber"].ToString();
                            my_table += "</td>";
                            my_table += "<td style='font-size: 12px; color: #1e2b33;  line-height: 18px;  vertical-align: top; padding:10px 0;' >" + dr["GDSPnr"].ToString() + "</td>";
                            gdspnr = dr["GDSPnr"].ToString();
                            my_table += "<td style='font-size: 12px; color: #1e2b33;  line-height: 18px;  vertical-align: top; padding:10px 0;'>" + dr["AirlinePnr"].ToString() + "</td>";
                            my_table += "<td style='font-size: 12px; color: #1e2b33;  line-height: 18px;  vertical-align: top; padding:10px 0;>" + dr["Sector"].ToString() + "</td>";
                            string strDepdt = Convert.ToString(dtflt.Rows[0]["DepDate"]);
                            try
                            {
                                strDepdt = (strDepdt.Length == 8 ? Left(strDepdt, 4) + "-" + Mid(strDepdt, 4, 2) + "-" + Right(strDepdt, 2) : "20" + Right(strDepdt, 2) + "-" + Mid(strDepdt, 2, 2) + "-" + Left(strDepdt, 2));
                                DateTime deptdt = Convert.ToDateTime(strDepdt);
                                strDepdt = deptdt.ToString("dd/MMM/yy").Replace('-', '/');
                                string depDay = Convert.ToString(deptdt.DayOfWeek);
                                strDepdt = strDepdt.Split('/')[0] + " " + strDepdt.Split('/')[1] + " " + strDepdt.Split('/')[2];

                            }
                            catch (Exception ex)
                            {
                                ex.ToString();
                            }
                            my_table += "<td style='font-size: 12px; color: #1e2b33;  line-height: 18px;  vertical-align: top; padding:10px 0;'>" + strDepdt + "</td>";
                            my_table += "<td style='font-size: 12px; color: #1e2b33;  line-height: 18px;  vertical-align: top; padding:10px 0;'>" + dr["Createdate"].ToString() + " </td>";
                            if (dr["paxtype"].ToString() == "ADT")
                            {
                                my_table += "<td style='font-size: 12px; color: #1e2b33;  line-height: 18px;  vertical-align: top; padding:10px 0;''>" + dr["ResuFareDiff"].ToString() + " </td>";
                                my_table += "<td style='font-size: 12px; color: #1e2b33;  line-height: 18px;  vertical-align: top; padding:10px 0;'>0</td>";
                                my_table += "<td style='font-size: 12px; color: #1e2b33;  line-height: 18px;  vertical-align: top; padding:10px 0;' align='right'>" + dr["ResuFareDiff"].ToString() + "</td>";
                                adult += Convert.ToDouble(dr["ResuFareDiff"]);
                                adulttax += 0;
                            }
                            if (dr["paxtype"].ToString() == "CHD")
                            {
                                my_table += "<td style='font-size: 12px; color: #1e2b33;  line-height: 18px;  vertical-align: top; padding:10px 0;'>" + dr["ResuFareDiff"].ToString() + " </td>";
                                my_table += "<td style='font-size: 12px; color: #1e2b33;  line-height: 18px;  vertical-align: top; padding:10px 0;'>0</td>";
                                my_table += "<td style='font-size: 12px; color: #1e2b33;  line-height: 18px;  vertical-align: top; padding:10px 0;'>" + dr["ResuFareDiff"].ToString() + "</td>";
                                child += Convert.ToDouble(dr["ResuFareDiff"]);
                                childtax += 0;
                            }
                            if (dr["paxtype"].ToString() == "INF")
                            {
                                my_table += "<td style='font-size: 12px; color: #1e2b33;  line-height: 18px;  vertical-align: top; padding:10px 0;'>" + dr["ResuFareDiff"].ToString() + " </td>";
                                my_table += "<td style='font-size: 12px; color: #1e2b33;  line-height: 18px;  vertical-align: top; padding:10px 0;'>0</td>";
                                my_table += "<td style='font-size: 12px; color: #1e2b33;  line-height: 18px;  vertical-align: top; padding:10px 0;'>" + dr["ResuFareDiff"].ToString() + "</td>";
                                infant += Convert.ToDouble(dr["ResuFareDiff"]);
                                infanttax += 0;
                            }
                        }
                        if ((ReissueId == ""))
                            total = (Convert.ToDouble(dr["basefare"]) + Convert.ToDouble(dr["totaltax"]));
                        else
                            total = (Convert.ToDouble(dr["ResuFareDiff"]));
                    }

                    totalfare = adult + child + infant;
                    totalfaretax = adulttax + childtax + infanttax;
                    total = totalfare + totalfaretax;
                    my_table += "<tr>";
                    my_table += "<td height='1' colspan='11' style='border-bottom:1px solid #e4e4e4'></td>";
                    my_table += "</tr>";
                    my_table += "<tr>";
                    my_table += "<td style='font-size: 12px; color: #ff0000;  line-height: 18px;  vertical-align: top; padding:10px 0;' class='article'></td>";
                    my_table += "<td style='font-size: 12px; color: #646a6e;  line-height: 18px;  vertical-align: top; padding:10px 0;'><small></small></td>";
                    my_table += "<td style='font-size: 12px; color: #646a6e;  line-height: 18px;  vertical-align: top; padding:10px 0;'></td>";
                    my_table += "<td style='font-size: 12px; color: #1e2b33;  line-height: 18px;  vertical-align: top; padding:10px 0;''></td>";
                    my_table += "<td style='font-size: 12px; color: #1e2b33;  line-height: 18px;  vertical-align: top; padding:10px 0;' ></td>";
                    my_table += "<td style='font-size: 12px; color: #1e2b33;  line-height: 18px;  vertical-align: top; padding:10px 0;' ></td>";
                    my_table += "<td style='font-size: 12px; color: #1e2b33;  line-height: 18px;  vertical-align: top; padding:10px 0;' ></td>";
                    my_table += "<td style='font-size: 12px; color: #1e2b33;  line-height: 18px;  vertical-align: top; padding:10px 0;' ><strong>Total</strong></td>";
                    my_table += "<td style='font-size: 12px; color: #1e2b33;  line-height: 18px;  vertical-align: top; padding:10px 0;' ><strong>" + totalfare + "</strong></td>";
                    my_table += "<td style='font-size: 12px; color: #1e2b33;  line-height: 18px;  vertical-align: top; padding:10px 0;' ><strong>" + totalfaretax + "</strong></td>";
                    my_table += "<td style='font-size: 12px; color: #1e2b33;  line-height: 18px;  vertical-align: top; padding:10px 0;' ><strong>" + total + "</strong></td>";
                    my_table += "</tr>";
                    my_table += "<tr>";
                    my_table += "<td height='1' colspan='11' style='border-bottom:1px solid #e4e4e4'></td>";
                    my_table += "</tr>";
                    my_table += "</tbody>";
                    my_table += "</table>";
                    my_table += "</td>";
                    my_table += " </tr>";
                    my_table += "<tr>";
                    my_table += "<td height='20'></td>";
                    my_table += "</tr>";
                    my_table += "</tbody>";
                    my_table += "</table>";
                    my_table += "</td>";
                    my_table += "</tr>";
                    my_table += "</tbody>";
                    my_table += "</table>";
                    my_table += "<table width='100%' border='0' cellpadding='0' cellspacing='0' align='center' class='fullTable' bgcolor='#e1e1e1'>";
                    my_table += "<tbody>";
                    my_table += "<tr>";
                    my_table += "<td>";
                    my_table += "<table width='100%' border='0' cellpadding='0' cellspacing='0' align='center' class='fullTable' bgcolor='#ffffff'>";
                    my_table += "<tbody>";
                    my_table += "<tr>";
                    my_table += "<td style='position: relative;right: 10%;'>";
                    double srvTax = 0;
                    double tf = 0;
                    double admMrk = 0;
                    double agMrk = 0;
                    double totDis = 0;
                    for (int j = 0; j <= dt.Rows.Count - 1; j++)
                    {
                        srvTax = srvTax + Convert.ToDouble(dt.Rows[j]["ServiceTax"]);
                        tf = tf + Convert.ToDouble(dt.Rows[j]["TranFee"]);
                        admMrk = admMrk + Convert.ToDouble(dt.Rows[j]["adminmrk"]);
                        agMrk = agMrk + Convert.ToDouble(dt.Rows[j]["AgentMrk"]);
                        totDis = totDis + Convert.ToDouble(dt.Rows[j]["TotalDiscount"]);
                        TDS = TDS + Convert.ToDouble(dt.Rows[j]["Tds"]);
                        CB = CB + Convert.ToDouble(dt.Rows[j]["CashBack"]);

                        if (!string.IsNullOrEmpty(dtAAdd.Rows[0]["IsCorp"].ToString()))
                        {
                            if (Convert.ToBoolean(dtAAdd.Rows[0]["IsCorp"]))
                                mgtFee = mgtFee + Convert.ToDouble(dt.Rows[j]["MgtFee"]);
                        }
                    }
                    if ((ReissueId == ""))
                    {
                        my_table += "<table width='300' border='0' cellpadding='5' cellspacing='3' align='right' class='fullPadding'>";
                        my_table += "<tbody>";

                        if ((IsCorp == true))
                        {
                            my_table += "<tr class='TransInvoice'>";
                            my_table += "<td style='font-size: 12px; color: #646a6e; line-height: 22px; vertical-align: top; text-align:right; '>Management Fee</td>";
                            my_table += "<td style='font-size: 12px; color: #646a6e; line-height: 22px; vertical-align: top; text-align:right; white-space:nowrap;' width='80'>" + mgtFee.ToString() + "</td>";
                            my_table += "</tr>";
                            my_table += "<tr>";
                            my_table += "<td height='1' colspan='11' style='border-bottom:1px solid #e4e4e4'></td>";
                            my_table += "</tr>";
                            my_table += "<tr class='TransInvoice'>";
                            my_table += "<td style='font-size: 12px; color: #646a6e; line-height: 22px; vertical-align: top; text-align:right; '>Service Tax</td>";
                            my_table += "<td style='font-size: 12px; color: #646a6e; line-height: 22px; vertical-align: top; text-align:right; '>" + srvTax.ToString() + "</td>";
                            my_table += "</tr>";
                            my_table += "<tr>";
                            my_table += "<td height='1' colspan='11' style='border-bottom:1px solid #e4e4e4'></td>";
                            my_table += "</tr>";
                            TotalST = total + srvTax; // + admMrk '+ tf + agentmrk
                        }
                        else
                        {
                            my_table += "<tr class='TransInvoice'>";
                            my_table += "<td style='font-size: 12px; color: #646a6e; line-height: 22px; vertical-align: top; text-align:right; '>Service Tax</td>";
                            my_table += "<td style='font-size: 12px; color: #646a6e; line-height: 22px; vertical-align: top; text-align:right; '>" + srvTax.ToString() + "</td>";
                            my_table += "</tr>";
                            my_table += "<tr>";
                            my_table += "<td height='1' colspan='11' style='border-bottom:1px solid #e4e4e4'></td>";
                            my_table += "</tr>";
                            my_table += "<tr class='TransInvoice'>";
                            my_table += "<td style='font-size: 12px; color: #646a6e; line-height: 22px; vertical-align: top; text-align:right; '>Transaction Fee</td>";
                            my_table += "<td style='font-size: 12px; color: #646a6e; line-height: 22px; vertical-align: top; text-align:right; '>0</td>"; // 'tf.ToString
                            my_table += "</tr>";
                            my_table += "<tr>";
                            my_table += "<td height='1' colspan='11' style='border-bottom:1px solid #e4e4e4'></td>";
                            my_table += "</tr>";

                            my_table += "<tr class='TransInvoice'>";
                            my_table += "<td style='font-size: 12px; color: #646a6e; line-height: 22px; vertical-align: top; text-align:right; '>Transaction Charge</td>";
                            my_table += "<td style='font-size: 12px; color: #646a6e; line-height: 22px; vertical-align: top; text-align:right; '>" + admMrk.ToString() + "</td>";

                            TotalST = total + srvTax + admMrk; // + tf + agentmrk
                            my_table += "</tr>";
                            my_table += "<tr>";
                            my_table += "<td height='1' colspan='11' style='border-bottom:1px solid #e4e4e4'></td>";
                            my_table += "</tr>";

                            my_table += "<tr>";
                            my_table += "<td style='font-size: 12px; color: #000; line-height: 22px; vertical-align: top; text-align:right; '><strong>Total(Inc. S.Tax & T.F.)</strong></td>";
                            my_table += "<td style='font-size: 12px; color: #000; line-height: 22px; vertical-align: top; text-align:right; '><strong>" + TotalST + "</td>";
                            my_table += "</tr>";
                            my_table += "<tr>";
                            my_table += "<td height='1' colspan='11' style='border-bottom:1px solid #e4e4e4'></td>";
                            my_table += "</tr>";
                            my_table += "<tr class='TransInvoice'>";
                            my_table += "<td style='font-size: 12px; color: #646a6e; line-height: 22px; vertical-align: top; text-align:right; '>Less Discount</td>";
                            my_table += "<td style='font-size: 12px; color: #646a6e; line-height: 22px; vertical-align: top; text-align:right; '>" + (totDis - (CB + tf)).ToString() + "</td>";
                            my_table += "</tr>";
                            my_table += "<tr>";
                            my_table += "<td height='1' colspan='11' style='border-bottom:1px solid #e4e4e4'></td>";
                            my_table += "</tr>";
                            my_table += "<tr class='TransInvoice'>";
                            my_table += "<td style='font-size: 12px; color: #646a6e; line-height: 22px; vertical-align: top; text-align:right; '>Less Cash Back</td>";
                            my_table += "<td style='font-size: 12px; color: #646a6e; line-height: 22px; vertical-align: top; text-align:right; '>" + CB.ToString() + "</td>";
                            my_table += "</tr>";
                            my_table += "<tr>";
                            my_table += "<td height='1' colspan='11' style='border-bottom:1px solid #e4e4e4'></td>";
                            my_table += "</tr>";
                            my_table += "<tr class='TransInvoice' >";
                            my_table += "<td style='font-size: 12px; color: #646a6e; line-height: 22px; vertical-align: top; text-align:right; '>Add TDS</td>";
                            my_table += "<td style='font-size: 12px; color: #646a6e; line-height: 22px; vertical-align: top; text-align:right; '>" + TDS.ToString() + "</td>";
                            my_table += "</tr>";
                            my_table += "<tr>";
                            my_table += "<td height='1' colspan='11' style='border-bottom:1px solid #e4e4e4'></td>";
                            my_table += "</tr>";
                        }
                    }
                    else if ((IsCorp == true))
                    {
                        my_table += "<tr class='TransInvoice'>";
                        my_table += "<td style='font-size: 12px; color: #646a6e; line-height: 22px; vertical-align: top; text-align:right; '>Reissue Charge</td>";
                        my_table += "<td style='font-size: 12px; color: #646a6e; line-height: 22px; vertical-align: top; text-align:right; '>" + Convert.ToDouble(dt.Rows[0]["ResuCharge"]) + Convert.ToDouble(dt.Rows[0]["ResuServiseCharge"]) + "</td>";
                        my_table += "</tr>";
                        my_table += "<tr>";
                        my_table += "<td height='1' colspan='11' style='border-bottom:1px solid #e4e4e4'></td>";
                        my_table += "</tr>";
                        my_table += "<tr class='TransInvoice'>";
                        my_table += "<td style='font-size: 12px; color: #646a6e; line-height: 22px; vertical-align: top; text-align:right; '></td>";
                        my_table += "<td style='font-size: 12px; color: #646a6e; line-height: 22px; vertical-align: top; text-align:right; '></td>"; // 'tf.ToString
                        my_table += "</tr>";
                        my_table += "<tr>";
                        my_table += "<td height='1' colspan='11' style='border-bottom:1px solid #e4e4e4'></td>";
                        my_table += "</tr>";
                        TotalST = total + Convert.ToDouble(dt.Rows[0]["ResuCharge"]) + Convert.ToDouble(dt.Rows[0]["ResuServiseCharge"]); // + admMrk '+ tf + agentmrk
                    }
                    else
                    {
                        my_table += "<tr class='TransInvoice'>";
                        my_table += "<td style='font-size: 12px; color: #646a6e; line-height: 22px; vertical-align: top; text-align:right; '>Reissue Charge</td>";
                        my_table += "<td style='font-size: 12px; color: #646a6e; line-height: 22px; vertical-align: top; text-align:right; '>" + dt.Rows[0]["ResuCharge"].ToString() + "</td>";
                        my_table += "</tr>";
                        my_table += "<tr>";
                        my_table += "<td height='1' colspan='11' style='border-bottom:1px solid #e4e4e4'></td>";
                        my_table += "</tr>";
                        my_table += "<tr class='TransInvoice'>";
                        my_table += "<td style='font-size: 12px; color: #646a6e; line-height: 22px; vertical-align: top; text-align:right; '>Service Charge</td>";
                        my_table += "<td style='font-size: 12px; color: #646a6e; line-height: 22px; vertical-align: top; text-align:right; '>" + dt.Rows[0]["ResuServiseCharge"].ToString() + "</td>"; // 'tf.ToString
                        my_table += "</tr>";
                        my_table += "<tr>";
                        my_table += "<td height='1' colspan='11' style='border-bottom:1px solid #e4e4e4'></td>";
                        my_table += "</tr>";
                        TotalST = total + Convert.ToDouble(dt.Rows[0]["ResuCharge"]) + Convert.ToDouble(dt.Rows[0]["ResuServiseCharge"]);
                    }






                    if ((ReissueId == ""))
                        // GrandTotal = (TotalST + TDS + mgtFee) - (totDis - tf) + debitBal - creditBal
                        GrandTotal = (TotalST + TDS + mgtFee) - (totDis - tf);
                    else
                        GrandTotal = TotalST;
                    if ((IsCorp == true))
                        my_table += "<td style='font-size: 12px; color: #000; line-height: 22px; vertical-align: top; text-align:right; '><strong>Grand Total</strong></td>";
                    else
                        my_table += "<td style='font-size: 12px; color: #000; line-height: 22px; vertical-align: top; text-align:right; '><strong>Grand Total</strong></td>";

                    my_table += "<td style='font-size: 12px; color: #000; line-height: 22px; vertical-align: top; text-align:right; '><strong>" + GrandTotal + "</strong></td>";

                    my_table += "</tbody>";
                    my_table += "</table>";

                    my_table += "</td>";
                    my_table += "</tr>";
                    my_table += "</tbody>";
                    my_table += "</table>";
                    my_table += "</td>";
                    my_table += "</tr>";
                    my_table += "</tbody>";
                    my_table += "</table>";
                    DataTable dtAgent = GetAgencyDetails(dt.Rows[0]["AgentId"].ToString()).Tables[0];
                    my_table += "<table width='100%' border='0' cellpadding='0' cellspacing='0' align='center' class='fullTable' bgcolor='#e1e1e1'>";
                    my_table += "<tbody>";
                    my_table += "<tr>";
                    my_table += "<td>";
                    my_table += "<table width='100%' border='0' cellpadding='0' cellspacing='0' align='center' class='fullTable' bgcolor='#ffffff'>";
                    my_table += "<tbody>";
                    my_table += "<tr>";
                    my_table += "<td>";
                    my_table += "<table width='100%' border='0' cellpadding='0' cellspacing='0' align='center' class='fullPadding'>";
                    my_table += "<tbody>";
                    my_table += "<tr>";
                    my_table += "<td>";
                    my_table += "<table width='220' border='0' cellpadding='0' cellspacing='0' align='left' class='col'>";

                    my_table += "<tbody>";
                    my_table += "<tr>";
                    my_table += "<td style='font-size: 11px; color: #5b5b5b; line-height: 1; vertical-align: top;padding-left: 10px;'>";
                    my_table += "<strong>" + dtAgent.Rows[0]["Agency_Name"].ToString() + "</strong>";
                    my_table += "</td>";
                    my_table += "</tr>";
                    my_table += "<tr>";
                    my_table += "<td width='100%' height='10'></td>";
                    my_table += "</tr>";
                    my_table += "<tr>";
                    my_table += "<td style='font-size: 12px;padding-left: 10px; color: #5b5b5b; line-height: 20px; vertical-align: top;'>";
                    my_table += "" + dtAgent.Rows[0]["Address"].ToString() + "<br> " + dtAgent.Rows[0]["City"].ToString() + ", " + dtAgent.Rows[0]["Zipcode"].ToString() + ",<br> " + dtAgent.Rows[0]["State"].ToString() + ",<br>  " + dtAgent.Rows[0]["Country"].ToString() + "";
                    my_table += "</td>";
                    my_table += "</tr>";
                    my_table += "</tbody>";
                    my_table += "</table>";
                    my_table += "</td>";
                    my_table += "</tr>";
                    my_table += "</tbody>";
                    my_table += "</table>";
                    my_table += "</td>";
                    my_table += "</tr>";
                    my_table += "<tr>";
                    my_table += "<td>";
                    my_table += "<table width='480' border='0' cellpadding='0' cellspacing='0' align='center' class='fullPadding'>";
                    my_table += "<tbody>";
                    my_table += "<tr>";
                    my_table += "<td>";
                    my_table += "</td>";
                    my_table += "</tr>";
                    my_table += "<tbody>";
                    my_table += "</table>";
                    my_table += "</td>";
                    my_table += "</tr>";
                    my_table += "</tbody>";
                    my_table += "</table>";
                    my_table += "</td>";
                    my_table += "</tr>";
                    my_table += "</tbody>";
                    my_table += "</table>";
                    my_table += "<table width='100%' border='0' cellpadding='0' cellspacing='0' align='center' class='fullTable' bgcolor='#e1e1e1'>";
                    my_table += "<tr>";
                    my_table += "<td>";
                    my_table += "<table width='100%' border='0' cellpadding='0' cellspacing='0' align='center' class='fullTable' bgcolor='#ffffff' style='border-radius: 0 0 10px 10px;'>";
                    my_table += "<tr>";
                    my_table += "<td>";
                    my_table += "<table width='100%' border='0' cellpadding='0' cellspacing='0' align='center' class='fullPadding'>";
                    my_table += "<tbody>";
                    my_table += "<tr>";
                    my_table += "<td style='font-size: 12px; color: #5b5b5b; line-height: 18px; vertical-align: top; text-align: left;'>This is Computer generated invoice,hence no signature required.</td>";
                    my_table += "</tr>";
                    my_table += "</tbody>";
                    my_table += "</table>";
                    my_table += "</td>";
                    my_table += "</tr>";
                    my_table += "</table>";
                    my_table += "</td>";
                    my_table += "</tr>";
                    my_table += "</table>";


                    result = my_table;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return result;
        }

        public static DataTable SelectHeaderDetail(string OrderId)
        {
            SqlDataAdapter adap;
            DataTable dt = new DataTable();
            try
            {
                adap = new SqlDataAdapter("SELECT isnull(ImportCharge,0) as Import_Charge,*  FROM  FltHeader WHERE OrderId = '" + OrderId + "' ", ConnectToDataBase.MyAmdDBConnection);
                adap.Fill(dt);
            }
            catch (Exception ex)
            {
                ex.ToString();
            }


            return dt;
        }

        public static DataTable SelectFlightDetail(string OrderId)
        {
            SqlDataAdapter adap;
            DataTable dt = new DataTable();
            adap = new SqlDataAdapter("SELECT FltId, OrderId, DepCityOrAirportCode AS DFrom, DepCityOrAirportName as DepAirName, ArrCityOrAirportCode AS ATo, ArrCityOrAirportName as ArrAirName, DepDate, DepTime, ArrDate, ArrTime, AirlineCode, AirlineName, FltNumber, AirCraft, CreateDate, UpdateDate,ISNULL(AdtRbd,'') as AdtRbd,ISNULL(ChdRbd,'') as ChdRbd FROM FltDetails  WHERE OrderId = '" + OrderId + "' Order by FltId ", ConnectToDataBase.MyAmdDBConnection);
            adap.Fill(dt);
            return dt;
        }


        public static string GetBagInfo(string Provider, string Remark)
        {
            string baginfo = "";
            if (Provider == "TB")
            {
                if (Remark.Contains("Hand"))
                    baginfo = Remark;
            }
            else if (Provider == "YA")
            {
                if (Remark.Contains("Hand"))
                    baginfo = Remark;
                else if (!string.IsNullOrEmpty(Remark))
                    baginfo = Remark + " Baggage allowance";
            }
            else if (Provider == "1G")
            {
                if (Remark.Contains("PC"))
                    baginfo = Remark.Replace("PC", " Piece(s) Baggage allowance");
                else if (Remark.Contains("K"))
                    baginfo = Remark.Replace("K", " Kg Baggage allowance");
            }
            return baginfo;
        }

        public static DataTable AgentIDInfo(string AgentID)
        {
            SqlDataAdapter adap;
            DataTable dt = new DataTable();
            adap = new SqlDataAdapter("select * from agent_register where user_id='" + AgentID + "'", ConnectToDataBase.MyAmdDBConnection);
            adap.Fill(dt);
            return dt;
        }

        public static DataTable SelectPaxDetail(string OrderId, string TID)
        {
            SqlDataAdapter adap = new SqlDataAdapter();
            if (string.IsNullOrEmpty(TID))
            {
                DataTable dt = new DataTable();

                adap = new SqlDataAdapter("SELECT PaxId, OrderId, Title + '  ' + FName + '  ' + MName + '  ' + LName AS Name, PaxType, TicketNumber,DOB,FFNumber,FFAirline,MealType,SeatType FROM   FltPaxDetails WHERE OrderId = '" + OrderId + "' ", ConnectToDataBase.MyAmdDBConnection);
                adap.Fill(dt);

                return dt;
            }
            else
            {
                DataTable dt = new DataTable();
                adap = new SqlDataAdapter("SELECT PaxId, OrderId, PaxId, Title + '  ' + FName + '  ' + MName + '  ' + LName AS Name, PaxType, TicketNumber,DOB,FFNumber,FFAirline,MealType,SeatType FROM   FltPaxDetails WHERE OrderId = '" + OrderId + "' and PaxId= '" + TID + "' ", ConnectToDataBase.MyAmdDBConnection);
                adap.Fill(dt);
                return dt;
            }
        }

        public static DataTable TerminalDetails(string OrderID, string DepCity, string ArrvlCity)
        {
            SqlDataAdapter adap = new SqlDataAdapter("USP_TERMINAL_INFO", ConnectToDataBase.MyAmdDBConnection);
            adap.SelectCommand.CommandType = CommandType.StoredProcedure;
            adap.SelectCommand.Parameters.AddWithValue("@DEPARTURECITY", DepCity);
            adap.SelectCommand.Parameters.AddWithValue("@ARRIVALCITY", ArrvlCity);
            adap.SelectCommand.Parameters.AddWithValue("@ORDERID", OrderID);
            DataTable dt1 = new DataTable();
            ConnectToDataBase.MyAmdDBConnection.Open();
            adap.Fill(dt1);
            ConnectToDataBase.MyAmdDBConnection.Close();
            return dt1;
        }
        public static DataSet GetInvoice(string OrderID)
        {
            SqlDataAdapter adap = new SqlDataAdapter("GetInvoiceDetails", ConnectToDataBase.MyAmdDBConnection);
            adap.SelectCommand.CommandType = CommandType.StoredProcedure;
            adap.SelectCommand.Parameters.AddWithValue("@orderid", OrderID);
            DataSet ds = new DataSet();
            ConnectToDataBase.MyAmdDBConnection.Open();
            adap.Fill(ds);
            ConnectToDataBase.MyAmdDBConnection.Close();
            return ds;
        }

        public static DataSet GetAgencyDetails(string UserId)
        {
            SqlDataAdapter adap = new SqlDataAdapter("AgencyDetails", ConnectToDataBase.MyAmdDBConnection);
            adap.SelectCommand.CommandType = CommandType.StoredProcedure;
            adap.SelectCommand.Parameters.AddWithValue("@UserId", UserId);
            DataSet ds = new DataSet();
            ConnectToDataBase.MyAmdDBConnection.Open();
            adap.Fill(ds);
            ConnectToDataBase.MyAmdDBConnection.Close();
            return ds;
        }

        public static DataSet GetCompanyAddress()
        {
            SqlDataAdapter adap = new SqlDataAdapter("SP_GETADDRESS", ConnectToDataBase.MyAmdDBConnection);
            adap.SelectCommand.CommandType = CommandType.StoredProcedure;
            adap.SelectCommand.Parameters.AddWithValue("@TYPE", "HEADOFFICE");
            DataSet ds = new DataSet();
            ConnectToDataBase.MyAmdDBConnection.Open();
            adap.Fill(ds);
            ConnectToDataBase.MyAmdDBConnection.Close();
            return ds;
        }
        public static DataTable ProxyChargeMth(string OrderID)
        {
            SqlDataAdapter adap = new SqlDataAdapter("Proxy_chargeOnTicket", ConnectToDataBase.MyAmdDBConnection);
            adap.SelectCommand.CommandType = CommandType.StoredProcedure;
            adap.SelectCommand.Parameters.AddWithValue("@OrderID", OrderID);
            DataTable dt1 = new DataTable();
            ConnectToDataBase.MyAmdDBConnection.Open();
            adap.Fill(dt1);
            ConnectToDataBase.MyAmdDBConnection.Close();
            return dt1;
        }

        public static DataTable SelectAgent(string OrderId)
        {
            SqlDataAdapter adap;
            DataTable dt = new DataTable();
            adap = new SqlDataAdapter("SELECT AgentId FROM   FltHeader WHERE OrderId = '" + OrderId + "' ", ConnectToDataBase.MyAmdDBConnection);
            adap.Fill(dt);
            return dt;
        }

        public static DataSet GetFltDtls(string orderid, string agentid)
        {
            DataSet dataSet = new DataSet();
            DataTable dataTable = new DataTable();
            try
            {

                SqlDataAdapter sqlDataAdapter;
                SqlCommand sqlCommand = new SqlCommand("GetSelectedFltDtls_Gal", ConnectToDataBase.MyAmdDBConnection);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Parameters.Add(new SqlParameter("@TrackId", orderid));
                sqlCommand.Parameters.Add(new SqlParameter("@UserId", agentid));
                sqlDataAdapter = new SqlDataAdapter();
                sqlDataAdapter.SelectCommand = sqlCommand;
                dataSet = new DataSet();
                sqlDataAdapter.Fill(dataSet, "GetSelectedFltDtls");
                dataTable = dataSet.Tables["GetSelectedFltDtls"];
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return dataSet;
        }

        public static DataSet GetMailingDetails(string department, string UserID)
        {
            DataSet dataSet = new DataSet();
            DataTable dataTable = new DataTable();
            try
            {
                SqlDataAdapter sqlDataAdapter;
                SqlCommand sqlCommand = new SqlCommand("SP_GETMAILINGCREDENTIAL_ITZ", ConnectToDataBase.MyAmdDBConnection);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Parameters.Add(new SqlParameter("@Department", department));
                sqlCommand.Parameters.Add(new SqlParameter("@UserID", UserID));
                sqlDataAdapter = new SqlDataAdapter();
                sqlDataAdapter.SelectCommand = sqlCommand;
                dataSet = new DataSet();
                sqlDataAdapter.Fill(dataSet, "SP_GETMAILINGCREDENTIAL");
                dataTable = dataSet.Tables["SP_GETMAILINGCREDENTIAL"];
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return dataSet;
        }

        public static DataSet GetPaxDetails(string trackid)
        {
            DataSet dataSet = new DataSet();
            try
            {
                SqlDataAdapter sqlDataAdapter;
                SqlCommand sqlCommand = new SqlCommand("GetPaxDetails", ConnectToDataBase.MyAmdDBConnection);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Parameters.Add(new SqlParameter("@TrackId", trackid));
                sqlDataAdapter = new SqlDataAdapter();
                sqlDataAdapter.SelectCommand = sqlCommand;
                dataSet = new DataSet();
                sqlDataAdapter.Fill(dataSet, "SP_GetPaxDetails");
                dataTable = dataSet.Tables["SP_GetPaxDetails"];
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return dataSet;
        }

        public static DataTable SelectAgencyDetail(string AgentID)
        {
            SqlDataAdapter adap;
            DataTable dt = new DataTable();
            adap = new SqlDataAdapter("SELECT Agency_Name,Email,Mobile,Address,Address,(City+' - '+State+' - '+Country) as Address1,IsCorp,ag_logo from agent_register  WHERE User_Id = '" + AgentID + "' ", ConnectToDataBase.MyAmdDBConnection);
            adap.Fill(dt);
            return dt;
        }

        public static DataTable SelectFareDetail(string OrderId, string TID)
        {
            SqlDataAdapter adap;
            if (string.IsNullOrEmpty(TID))
            {
                DataTable dt = new DataTable();
                adap = new SqlDataAdapter("select PaxType, BaseFare, YQ as Fuel,  (YR+WO+OT+K3) as Tax,ServiceTax,TranFee as TFee, (AdminMrk+AgentMrk+DistrMrk) as TCharge,(BaseFare+YQ+YR+WO+OT+ServiceTax+AdminMrk+AgentMrk+DistrMrk+TranFee+K3) as Total,TotalAfterDis , ISNULL(MgtFee,0) as MgtFee,ISNULL(FareType,'') as FareType,ISNULL(TotalDiscount,0) AS Discount,isnull(Tds,0) as tds,ticketcopymarkupforTax,ticketcopymarkupforTC  FROM FltFareDetails where OrderId='" + OrderId + "' ", ConnectToDataBase.MyAmdDBConnection);
                adap.Fill(dt);
                return dt;
            }
            else
            {
                DataTable dt = new DataTable();
                adap = new SqlDataAdapter("SELECT  FltFareDetails.BaseFare as BaseFare, FltFareDetails.YQ as Fuel,  (FltFareDetails.YR+FltFareDetails.WO+FltFareDetails.OT+FltFareDetails.K3) as Tax,FltFareDetails.ServiceTax as ServiceTax,FltFareDetails.TranFee as TFee, FltFareDetails.AdminMrk+FltFareDetails.AgentMrk+FltFareDetails.DistrMrk as TCharge,TotalAfterDis , ISNULL(MgtFee,0) as MgtFee,ISNULL(FareType,'') as FareType,ISNULL(FltFareDetails.TotalDiscount,0) AS Discount,isnull(FltFareDetails.Tds,0) as tds  FROM FltPaxDetails INNER JOIN FltFareDetails ON FltPaxDetails.OrderId = FltFareDetails.OrderId AND FltPaxDetails.PaxType = FltFareDetails.PaxType WHERE FltPaxDetails.PaxId = '" + TID + "' ", ConnectToDataBase.MyAmdDBConnection);
                adap.Fill(dt);
                return dt;
            }
        }

        public static string Left(string param, int length)
        {
            string result = param.Substring(0, length);
            return result;
        }
        public static string Right(string param, int length)
        {
            string result = param.Substring(param.Length - length, length);
            return result;
        }

        public static string Mid(string param, int startIndex, int endIndex)
        {
            string result = param.Substring(startIndex, endIndex);
            return result;
        }

    }
}
