﻿using ModelLibrary;
using System;
using System.Data;
using System.Data.SqlClient;

namespace DataBaseLibrary.BusDB
{
    public static class BusDataBase
    {
        private static SqlCommand sqlCommand { get; set; }
        private static SqlDataAdapter sqlDataAdapter { get; set; }
        private static DataSet dataSet { get; set; }
        private static DataTable dataTable { get; set; }
        public static DataTable GetBusReport(BusFilter filter)
        {
            try
            {
                ConnectToDataBase.BusOpenConnection();
                sqlCommand = new SqlCommand("SP_BUS_REPORT", ConnectToDataBase.ConnectionStrBus);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Parameters.Add(new SqlParameter("@FromDate", (!string.IsNullOrEmpty(filter.FilterFromDate) ? filter.FilterFromDate : "")));
                sqlCommand.Parameters.Add(new SqlParameter("@ToDate", (!string.IsNullOrEmpty(filter.FilterToDate) ? filter.FilterToDate : "")));
                sqlCommand.Parameters.Add(new SqlParameter("@ORDERID", (!string.IsNullOrEmpty(filter.FilterOrderId) ? filter.FilterOrderId : "")));
                sqlCommand.Parameters.Add(new SqlParameter("@SOURCE", (!string.IsNullOrEmpty(filter.FilterSource) ? filter.FilterSource : "")));
                sqlCommand.Parameters.Add(new SqlParameter("@DESTINATION", (!string.IsNullOrEmpty(filter.FilterDestination) ? filter.FilterDestination : "")));
                sqlCommand.Parameters.Add(new SqlParameter("@BUSOPERATOR", (!string.IsNullOrEmpty(filter.FilterBusOperator) ? filter.FilterBusOperator : "")));
                sqlCommand.Parameters.Add(new SqlParameter("@TICKETNO", (!string.IsNullOrEmpty(filter.FilterTicketNo) ? filter.FilterTicketNo : "")));
                sqlCommand.Parameters.Add(new SqlParameter("@PNR", (!string.IsNullOrEmpty(filter.FilterPnr) ? filter.FilterPnr : "")));
                sqlCommand.Parameters.Add(new SqlParameter("@Status", (!string.IsNullOrEmpty(filter.FilterStatus) ? filter.FilterStatus : "")));
                sqlCommand.Parameters.Add(new SqlParameter("@AgentID", (!string.IsNullOrEmpty(filter.FilterAgentId) ? filter.FilterAgentId : "")));
                sqlCommand.Parameters.Add(new SqlParameter("@usertype", (!string.IsNullOrEmpty(filter.FilterUserType) ? filter.FilterUserType : "")));
                sqlCommand.Parameters.Add(new SqlParameter("@LoginID", (!string.IsNullOrEmpty(filter.FilterLoginId) ? filter.FilterLoginId : "")));
                sqlCommand.Parameters.Add(new SqlParameter("@PaymentStatus", (!string.IsNullOrEmpty(filter.FilterPaymentStatus) ? filter.FilterPaymentStatus : "")));

                sqlDataAdapter = new SqlDataAdapter();
                sqlDataAdapter.SelectCommand = sqlCommand;
                dataSet = new DataSet();
                sqlDataAdapter.Fill(dataSet, "BusReport");
                dataTable = dataSet.Tables["BusReport"];
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            finally
            {
                ConnectToDataBase.BusCloseConnection();
            }

            return dataTable;
        }
        public static DataTable GetBusRefundReport(BusFilter filter)
        {
            try
            {
                ConnectToDataBase.BusOpenConnection();
                sqlCommand = new SqlCommand("SP_BUSCancellationReport", ConnectToDataBase.ConnectionStrBus);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Parameters.Add(new SqlParameter("@usertype", (!string.IsNullOrEmpty(filter.FilterUserType) ? filter.FilterUserType : "")));
                sqlCommand.Parameters.Add(new SqlParameter("@LoginID", (!string.IsNullOrEmpty(filter.FilterLoginId) ? filter.FilterLoginId : "")));
                sqlCommand.Parameters.Add(new SqlParameter("@FormDate", (!string.IsNullOrEmpty(filter.FilterFromDate) ? filter.FilterFromDate : "")));
                sqlCommand.Parameters.Add(new SqlParameter("@ToDate", (!string.IsNullOrEmpty(filter.FilterToDate) ? filter.FilterToDate : "")));
                sqlCommand.Parameters.Add(new SqlParameter("@OrderID", (!string.IsNullOrEmpty(filter.FilterOrderId) ? filter.FilterOrderId : "")));
                sqlCommand.Parameters.Add(new SqlParameter("@AgentId", (!string.IsNullOrEmpty(filter.FilterAgentId) ? filter.FilterAgentId : "")));
                sqlCommand.Parameters.Add(new SqlParameter("@source", (!string.IsNullOrEmpty(filter.FilterSource) ? filter.FilterSource : "")));
                sqlCommand.Parameters.Add(new SqlParameter("@destination", (!string.IsNullOrEmpty(filter.FilterDestination) ? filter.FilterDestination : "")));
                sqlCommand.Parameters.Add(new SqlParameter("@pnr", (!string.IsNullOrEmpty(filter.FilterPnr) ? filter.FilterPnr : "")));
                sqlCommand.Parameters.Add(new SqlParameter("@providername", (!string.IsNullOrEmpty(filter.FilterProviderName) ? filter.FilterProviderName : "")));
                sqlCommand.Parameters.Add(new SqlParameter("@PaymentStatus", (!string.IsNullOrEmpty(filter.FilterPaymentStatus) ? filter.FilterPaymentStatus : "")));
                sqlCommand.Parameters.Add(new SqlParameter("@BUSOPERATOR", (!string.IsNullOrEmpty(filter.FilterBusOperator) ? filter.FilterBusOperator : "")));
                sqlCommand.Parameters.Add(new SqlParameter("@TICKETNO", (!string.IsNullOrEmpty(filter.FilterTicketNo) ? filter.FilterTicketNo : "")));

                sqlDataAdapter = new SqlDataAdapter();
                sqlDataAdapter.SelectCommand = sqlCommand;
                dataSet = new DataSet();
                sqlDataAdapter.Fill(dataSet, "BUSCancellationReport");
                dataTable = dataSet.Tables["BUSCancellationReport"];
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            finally
            {
                ConnectToDataBase.BusCloseConnection();
            }

            return dataTable;
        }
        public static DataTable GetBusRejectReport(BusFilter filter)
        {
            try
            {
                ConnectToDataBase.BusOpenConnection();
                sqlCommand = new SqlCommand("SP_BUSRejectReport", ConnectToDataBase.ConnectionStrBus);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Parameters.Add(new SqlParameter("@usertype", (!string.IsNullOrEmpty(filter.FilterUserType) ? filter.FilterUserType : "")));
                sqlCommand.Parameters.Add(new SqlParameter("@LoginID", (!string.IsNullOrEmpty(filter.FilterLoginId) ? filter.FilterLoginId : "")));
                sqlCommand.Parameters.Add(new SqlParameter("@FormDate", (!string.IsNullOrEmpty(filter.FilterFromDate) ? filter.FilterFromDate : "")));
                sqlCommand.Parameters.Add(new SqlParameter("@ToDate", (!string.IsNullOrEmpty(filter.FilterToDate) ? filter.FilterToDate : "")));
                sqlCommand.Parameters.Add(new SqlParameter("@OrderID", (!string.IsNullOrEmpty(filter.FilterOrderId) ? filter.FilterOrderId : "")));
                sqlCommand.Parameters.Add(new SqlParameter("@AgentId", (!string.IsNullOrEmpty(filter.FilterAgentId) ? filter.FilterAgentId : "")));
                sqlCommand.Parameters.Add(new SqlParameter("@source", (!string.IsNullOrEmpty(filter.FilterSource) ? filter.FilterSource : "")));
                sqlCommand.Parameters.Add(new SqlParameter("@destination", (!string.IsNullOrEmpty(filter.FilterDestination) ? filter.FilterDestination : "")));
                sqlCommand.Parameters.Add(new SqlParameter("@pnr", (!string.IsNullOrEmpty(filter.FilterPnr) ? filter.FilterPnr : "")));
                sqlCommand.Parameters.Add(new SqlParameter("@providername", (!string.IsNullOrEmpty(filter.FilterProviderName) ? filter.FilterProviderName : "")));
                sqlCommand.Parameters.Add(new SqlParameter("@PaymentStatus", (!string.IsNullOrEmpty(filter.FilterPaymentStatus) ? filter.FilterPaymentStatus : "")));
                sqlCommand.Parameters.Add(new SqlParameter("@BUSOPERATOR", (!string.IsNullOrEmpty(filter.FilterBusOperator) ? filter.FilterBusOperator : "")));
                sqlCommand.Parameters.Add(new SqlParameter("@TICKETNO", (!string.IsNullOrEmpty(filter.FilterTicketNo) ? filter.FilterTicketNo : "")));

                sqlDataAdapter = new SqlDataAdapter();
                sqlDataAdapter.SelectCommand = sqlCommand;
                dataSet = new DataSet();
                sqlDataAdapter.Fill(dataSet, "BUSRejectReport");
                dataTable = dataSet.Tables["BUSRejectReport"];
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            finally
            {
                ConnectToDataBase.BusCloseConnection();
            }

            return dataTable;
        }
        public static DataTable GetBusHoldPnrReport(BusFilter filter)
        {
            try
            {
                ConnectToDataBase.BusOpenConnection();
                sqlCommand = new SqlCommand("SP_BUSHoldReport", ConnectToDataBase.ConnectionStrBus);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Parameters.Add(new SqlParameter("@usertype", (!string.IsNullOrEmpty(filter.FilterUserType) ? filter.FilterUserType : "")));
                sqlCommand.Parameters.Add(new SqlParameter("@LoginID", (!string.IsNullOrEmpty(filter.FilterLoginId) ? filter.FilterLoginId : "")));
                sqlCommand.Parameters.Add(new SqlParameter("@FormDate", (!string.IsNullOrEmpty(filter.FilterFromDate) ? filter.FilterFromDate : "")));
                sqlCommand.Parameters.Add(new SqlParameter("@ToDate", (!string.IsNullOrEmpty(filter.FilterToDate) ? filter.FilterToDate : "")));
                sqlCommand.Parameters.Add(new SqlParameter("@OrderID", (!string.IsNullOrEmpty(filter.FilterOrderId) ? filter.FilterOrderId : "")));
                sqlCommand.Parameters.Add(new SqlParameter("@AgentId", (!string.IsNullOrEmpty(filter.FilterAgentId) ? filter.FilterAgentId : "")));
                sqlCommand.Parameters.Add(new SqlParameter("@source", (!string.IsNullOrEmpty(filter.FilterSource) ? filter.FilterSource : "")));
                sqlCommand.Parameters.Add(new SqlParameter("@destination", (!string.IsNullOrEmpty(filter.FilterDestination) ? filter.FilterDestination : "")));
                sqlCommand.Parameters.Add(new SqlParameter("@pnr", (!string.IsNullOrEmpty(filter.FilterPnr) ? filter.FilterPnr : "")));
                sqlCommand.Parameters.Add(new SqlParameter("@providername", (!string.IsNullOrEmpty(filter.FilterProviderName) ? filter.FilterProviderName : "")));
                sqlCommand.Parameters.Add(new SqlParameter("@PaymentStatus", (!string.IsNullOrEmpty(filter.FilterPaymentStatus) ? filter.FilterPaymentStatus : "")));
                sqlCommand.Parameters.Add(new SqlParameter("@BUSOPERATOR", (!string.IsNullOrEmpty(filter.FilterBusOperator) ? filter.FilterBusOperator : "")));
                sqlCommand.Parameters.Add(new SqlParameter("@TICKETNO", (!string.IsNullOrEmpty(filter.FilterTicketNo) ? filter.FilterTicketNo : "")));
                sqlCommand.Parameters.Add(new SqlParameter("@execid", (!string.IsNullOrEmpty(filter.FilterExecid) ? filter.FilterExecid : "")));

                sqlDataAdapter = new SqlDataAdapter();
                sqlDataAdapter.SelectCommand = sqlCommand;
                dataSet = new DataSet();
                sqlDataAdapter.Fill(dataSet, "BusHoldPnr");
                dataTable = dataSet.Tables["BusHoldPnr"];
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            finally
            {
                ConnectToDataBase.BusCloseConnection();
            }

            return dataTable;
        }
        public static DataTable GetStatusExecutiveID()
        {
            try
            {
                ConnectToDataBase.MyAmdOpenConnection();
                sqlCommand = new SqlCommand("Select Distinct ExecutiveID  from fltheader where ExecutiveID<>'' AND ExecutiveID IS NOT NULL AND ([Status]='Confirm' OR [Status]='InProcess')", ConnectToDataBase.MyAmdDBConnection);

                sqlDataAdapter = new SqlDataAdapter();
                sqlDataAdapter.SelectCommand = sqlCommand;
                dataSet = new DataSet();
                sqlDataAdapter.Fill(dataSet, "ExecutiveID");
                dataTable = dataSet.Tables["ExecutiveID"];
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            finally
            {
                ConnectToDataBase.MyAmdCloseConnection();
            }

            return dataTable;
        }
        public static DataTable GetBusSelectedSeatDetails(string orderid, string agentid, string tinno = "")
        {
            try
            {
                ConnectToDataBase.BusOpenConnection();
                sqlCommand = new SqlCommand("SP_BUS_SELECTED_SEATDETAILS", ConnectToDataBase.ConnectionStrBus);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Parameters.Add(new SqlParameter("@orderid", orderid));
                sqlCommand.Parameters.Add(new SqlParameter("@tripid", ""));
                sqlCommand.Parameters.Add(new SqlParameter("@src", ""));
                sqlCommand.Parameters.Add(new SqlParameter("@dest", ""));
                sqlCommand.Parameters.Add(new SqlParameter("@jrdate", ""));
                sqlCommand.Parameters.Add(new SqlParameter("@busoperator", ""));
                sqlCommand.Parameters.Add(new SqlParameter("@seatno", ""));
                sqlCommand.Parameters.Add(new SqlParameter("@noofpax", ""));
                sqlCommand.Parameters.Add(new SqlParameter("@bustype", ""));
                sqlCommand.Parameters.Add(new SqlParameter("@ladiesseat", ""));
                sqlCommand.Parameters.Add(new SqlParameter("@fare", ""));
                sqlCommand.Parameters.Add(new SqlParameter("@originalfare", ""));
                sqlCommand.Parameters.Add(new SqlParameter("@bdpoint", ""));
                sqlCommand.Parameters.Add(new SqlParameter("@dppoint", ""));
                sqlCommand.Parameters.Add(new SqlParameter("@agentid", agentid));
                sqlCommand.Parameters.Add(new SqlParameter("@bookreq", ""));
                sqlCommand.Parameters.Add(new SqlParameter("@bookres", ""));
                sqlCommand.Parameters.Add(new SqlParameter("@blockkey", ""));
                sqlCommand.Parameters.Add(new SqlParameter("@tinno", tinno));
                sqlCommand.Parameters.Add(new SqlParameter("@status", ""));
                sqlCommand.Parameters.Add(new SqlParameter("@updateddate", ""));
                sqlCommand.Parameters.Add(new SqlParameter("@admrkup", 0));
                sqlCommand.Parameters.Add(new SqlParameter("@agmrkup", 0));
                sqlCommand.Parameters.Add(new SqlParameter("@adcomm", 0));
                sqlCommand.Parameters.Add(new SqlParameter("@taTds", 0));
                sqlCommand.Parameters.Add(new SqlParameter("@totfare", 0));
                sqlCommand.Parameters.Add(new SqlParameter("@taTotfare", 0));
                sqlCommand.Parameters.Add(new SqlParameter("@taNetfare", 0));
                sqlCommand.Parameters.Add(new SqlParameter("@execID", ""));
                sqlCommand.Parameters.Add(new SqlParameter("@operation", "Select"));
                sqlCommand.Parameters.Add(new SqlParameter("@tableref", "BOOKREQUEST"));
                sqlCommand.Parameters.Add(new SqlParameter("@idproof", ""));
                sqlCommand.Parameters.Add(new SqlParameter("@usertype", "TA"));
                sqlCommand.Parameters.Add(new SqlParameter("@providername", ""));
                sqlCommand.Parameters.Add(new SqlParameter("@canpolicy", ""));
                sqlCommand.Parameters.Add(new SqlParameter("@partialcan", ""));
                sqlCommand.Parameters.Add(new SqlParameter("@Arr_Time", ""));
                sqlCommand.Parameters.Add(new SqlParameter("@Dept_Time", ""));
                sqlCommand.Parameters.Add(new SqlParameter("@Dur_Time", ""));
                sqlCommand.Parameters.Add(new SqlParameter("@AC_NONAC", ""));
                sqlCommand.Parameters.Add(new SqlParameter("@SEAT_TYPE", ""));
                sqlCommand.Parameters.Add(new SqlParameter("@totalFareWithTaxes", ""));
                sqlCommand.Parameters.Add(new SqlParameter("@serviceTaxAmount", ""));

                try
                {
                    sqlDataAdapter = new SqlDataAdapter();
                    sqlDataAdapter.SelectCommand = sqlCommand;
                    dataSet = new DataSet();
                    sqlDataAdapter.Fill(dataSet, "Selected_SeatDetail");
                    dataTable = dataSet.Tables["Selected_SeatDetail"];
                }
                catch (Exception ex)
                {
                    string error = ex.ToString();
                }
            }
            catch (Exception ex)
            {
                string error= ex.ToString(); 
            }
            finally
            {
                ConnectToDataBase.BusCloseConnection();
            }

            return dataTable;
        }
    }
}
