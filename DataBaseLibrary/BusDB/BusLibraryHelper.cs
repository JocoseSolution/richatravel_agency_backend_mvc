﻿using DataBaseLibrary.UtilityLibrary;
using ModelLibrary;
using System;
using System.Collections.Generic;
using System.Data;

namespace DataBaseLibrary.BusDB
{
    public static class BusLibraryHelper
    {
        #region [Export To Excel]   
        public static DataTable ExportBusTicketReport(BusFilter filter)
        {
            return BusDataBase.GetBusReport(filter);
        }
        public static DataTable BusRefundReportExportToExcel(BusFilter filter)
        {
            return BusDataBase.GetBusRefundReport(filter);
        }
        public static DataTable BusRejectReportExportToExcel(BusFilter filter)
        {
            return BusDataBase.GetBusRejectReport(filter);
        }
        public static DataTable BusHoldPnrReportExportToExcel(BusFilter filter)
        {
            return BusDataBase.GetBusHoldPnrReport(filter);
        }
        #endregion
        public static BusModel GetBusReport(BusFilter filter)
        {
            BusModel model = new BusModel();
            try
            {
                DataTable dtBus = BusDataBase.GetBusReport(filter);

                if (dtBus != null && dtBus.Rows.Count > 0)
                {
                    List<BusModel> busReportList = new List<BusModel>();
                    for (int i = 0; i < dtBus.Rows.Count; i++)
                    {
                        BusModel bus = new BusModel();
                        bus.AgentId = !string.IsNullOrEmpty(dtBus.Rows[i]["AgentId"].ToString()) ? dtBus.Rows[i]["AgentId"].ToString() : "- - -";
                        bus.Agency_Name = !string.IsNullOrEmpty(dtBus.Rows[i]["Agency_Name"].ToString()) ? dtBus.Rows[i]["Agency_Name"].ToString() : "- - -";
                        bus.Agent_Type = !string.IsNullOrEmpty(dtBus.Rows[i]["Agent_Type"].ToString()) ? dtBus.Rows[i]["Agent_Type"].ToString() : "- - -";
                        bus.ORDERID = !string.IsNullOrEmpty(dtBus.Rows[i]["ORDERID"].ToString()) ? dtBus.Rows[i]["ORDERID"].ToString() : "- - -";
                        bus.TRIPID = !string.IsNullOrEmpty(dtBus.Rows[i]["TRIPID"].ToString()) ? dtBus.Rows[i]["TRIPID"].ToString() : "- - -";
                        bus.SOURCE = !string.IsNullOrEmpty(dtBus.Rows[i]["SOURCE"].ToString()) ? dtBus.Rows[i]["SOURCE"].ToString() : "- - -";
                        bus.DESTINATION = !string.IsNullOrEmpty(dtBus.Rows[i]["DESTINATION"].ToString()) ? dtBus.Rows[i]["DESTINATION"].ToString() : "- - -";
                        bus.Sector = !string.IsNullOrEmpty(dtBus.Rows[i]["Sector"].ToString()) ? dtBus.Rows[i]["Sector"].ToString() : "- - -";
                        bus.BUSOPERATOR = !string.IsNullOrEmpty(dtBus.Rows[i]["BUSOPERATOR"].ToString()) ? dtBus.Rows[i]["BUSOPERATOR"].ToString() : "- - -";
                        bus.SEATNO = !string.IsNullOrEmpty(dtBus.Rows[i]["SEATNO"].ToString()) ? dtBus.Rows[i]["SEATNO"].ToString() : "- - -";
                        bus.LADIESSEAT = !string.IsNullOrEmpty(dtBus.Rows[i]["LADIESSEAT"].ToString()) ? dtBus.Rows[i]["LADIESSEAT"].ToString() : "- - -";
                        bus.FARE = !string.IsNullOrEmpty(dtBus.Rows[i]["FARE"].ToString()) ? dtBus.Rows[i]["FARE"].ToString() : "- - -";
                        bus.TA_NET_FARE = !string.IsNullOrEmpty(dtBus.Rows[i]["TA_NET_FARE"].ToString()) ? dtBus.Rows[i]["TA_NET_FARE"].ToString() : "- - -";
                        bus.BOARDINGPOINT = !string.IsNullOrEmpty(dtBus.Rows[i]["BOARDINGPOINT"].ToString()) ? dtBus.Rows[i]["BOARDINGPOINT"].ToString() : "- - -";
                        bus.DROPPINGPOINT = !string.IsNullOrEmpty(dtBus.Rows[i]["DROPPINGPOINT"].ToString()) ? dtBus.Rows[i]["DROPPINGPOINT"].ToString() : "- - -";
                        bus.PAXNAME = !string.IsNullOrEmpty(dtBus.Rows[i]["PAXNAME"].ToString()) ? dtBus.Rows[i]["PAXNAME"].ToString() : "- - -";
                        bus.GENDER = !string.IsNullOrEmpty(dtBus.Rows[i]["GENDER"].ToString()) ? dtBus.Rows[i]["GENDER"].ToString() : "- - -";
                        bus.PRIMARY_PAX_IDTYPE = !string.IsNullOrEmpty(dtBus.Rows[i]["PRIMARY_PAX_IDTYPE"].ToString()) ? dtBus.Rows[i]["PRIMARY_PAX_IDTYPE"].ToString() : "- - -";
                        bus.PRIMARY_PAX_IDNUMBER = !string.IsNullOrEmpty(dtBus.Rows[i]["PRIMARY_PAX_IDNUMBER"].ToString()) ? dtBus.Rows[i]["PRIMARY_PAX_IDNUMBER"].ToString() : "- - -";
                        bus.PRIMARY_PAX_PAXMOB = !string.IsNullOrEmpty(dtBus.Rows[i]["PRIMARY_PAX_PAXMOB"].ToString()) ? dtBus.Rows[i]["PRIMARY_PAX_PAXMOB"].ToString() : "- - -";
                        bus.PRIMARY_PAX_PAXEMAIL = !string.IsNullOrEmpty(dtBus.Rows[i]["PRIMARY_PAX_PAXEMAIL"].ToString()) ? dtBus.Rows[i]["PRIMARY_PAX_PAXEMAIL"].ToString() : "- - -";
                        bus.BOOKINGSTATUS = !string.IsNullOrEmpty(dtBus.Rows[i]["BOOKINGSTATUS"].ToString()) ? dtBus.Rows[i]["BOOKINGSTATUS"].ToString() : "- - -";
                        bus.TICKETNO = !string.IsNullOrEmpty(dtBus.Rows[i]["TICKETNO"].ToString()) ? dtBus.Rows[i]["TICKETNO"].ToString() : "- - -";
                        bus.PNR = !string.IsNullOrEmpty(dtBus.Rows[i]["PNR"].ToString()) ? dtBus.Rows[i]["PNR"].ToString() : "- - -";
                        bus.CANCELLATIONCHARGE = !string.IsNullOrEmpty(dtBus.Rows[i]["CANCELLATIONCHARGE"].ToString()) ? dtBus.Rows[i]["CANCELLATIONCHARGE"].ToString() : "- - -";
                        bus.REFUNDEDAMOUNT = !string.IsNullOrEmpty(dtBus.Rows[i]["REFUNDEDAMOUNT"].ToString()) ? dtBus.Rows[i]["REFUNDEDAMOUNT"].ToString() : "- - -";
                        bus.PARTIALCANCEL = !string.IsNullOrEmpty(dtBus.Rows[i]["PARTIALCANCEL"].ToString()) ? dtBus.Rows[i]["PARTIALCANCEL"].ToString() : "- - -";
                        bus.JOURNEYDATE = !string.IsNullOrEmpty(dtBus.Rows[i]["JOURNEYDATE"].ToString()) ? dtBus.Rows[i]["JOURNEYDATE"].ToString() : "- - -";
                        bus.CREATEDDATE = !string.IsNullOrEmpty(dtBus.Rows[i]["CREATEDDATE"].ToString()) ? UtilityLib.DateFormateDDMMMYYYY(dtBus.Rows[i]["CREATEDDATE"].ToString()) : "- - -";
                        bus.TA_TOT_FARE = !string.IsNullOrEmpty(dtBus.Rows[i]["TA_TOT_FARE"].ToString()) ? dtBus.Rows[i]["TA_TOT_FARE"].ToString() : "- - -";
                        bus.ADMIN_COMM = !string.IsNullOrEmpty(dtBus.Rows[i]["ADMIN_COMM"].ToString()) ? dtBus.Rows[i]["ADMIN_COMM"].ToString() : "- - -";
                        bus.TA_TDS = !string.IsNullOrEmpty(dtBus.Rows[i]["TA_TDS"].ToString()) ? dtBus.Rows[i]["TA_TDS"].ToString() : "- - -";
                        bus.YATRA_PNR = !string.IsNullOrEmpty(dtBus.Rows[i]["YATRA_PNR"].ToString()) ? dtBus.Rows[i]["YATRA_PNR"].ToString() : "- - -";
                        bus.PROVIDER_NAME = !string.IsNullOrEmpty(dtBus.Rows[i]["PROVIDER_NAME"].ToString()) ? dtBus.Rows[i]["PROVIDER_NAME"].ToString() : "- - -";
                        bus.PaymentMode = !string.IsNullOrEmpty(dtBus.Rows[i]["PaymentMode"].ToString()) ? dtBus.Rows[i]["PaymentMode"].ToString() : "- - -";

                        busReportList.Add(bus);
                    }
                    model.BusList = busReportList;
                    model.TotalCount = busReportList.Count;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return model;
        }
        public static List<BusRefund> GetBusRefundReport(BusFilter filter)
        {
            List<BusRefund> busRefundList = new List<BusRefund>();
            try
            {
                DataTable dtBusRefund = BusDataBase.GetBusRefundReport(filter);
                if (dtBusRefund != null && dtBusRefund.Rows.Count > 0)
                {
                    for (int i = 0; i < dtBusRefund.Rows.Count; i++)
                    {
                        BusRefund busRefund = new BusRefund();
                        busRefund.OrderId = !string.IsNullOrEmpty(dtBusRefund.Rows[i]["ORDERID"].ToString()) ? dtBusRefund.Rows[i]["ORDERID"].ToString() : "- - -";
                        busRefund.TripId = !string.IsNullOrEmpty(dtBusRefund.Rows[i]["TRIPID"].ToString()) ? dtBusRefund.Rows[i]["TRIPID"].ToString() : "- - -";
                        busRefund.Source = !string.IsNullOrEmpty(dtBusRefund.Rows[i]["SOURCE"].ToString()) ? dtBusRefund.Rows[i]["SOURCE"].ToString() : "- - -";
                        busRefund.Destination = !string.IsNullOrEmpty(dtBusRefund.Rows[i]["DESTINATION"].ToString()) ? dtBusRefund.Rows[i]["DESTINATION"].ToString() : "- - -";
                        busRefund.BusOperator = !string.IsNullOrEmpty(dtBusRefund.Rows[i]["BUSOPERATOR"].ToString()) ? dtBusRefund.Rows[i]["BUSOPERATOR"].ToString() : "- - -";
                        busRefund.SeatNo = !string.IsNullOrEmpty(dtBusRefund.Rows[i]["SEATNO"].ToString()) ? dtBusRefund.Rows[i]["SEATNO"].ToString() : "- - -";
                        busRefund.PaxName = !string.IsNullOrEmpty(dtBusRefund.Rows[i]["PAXNAME"].ToString()) ? dtBusRefund.Rows[i]["PAXNAME"].ToString() : "- - -";
                        busRefund.Gender = !string.IsNullOrEmpty(dtBusRefund.Rows[i]["GENDER"].ToString()) ? dtBusRefund.Rows[i]["GENDER"].ToString() : "- - -";
                        busRefund.TicketNo = !string.IsNullOrEmpty(dtBusRefund.Rows[i]["TICKETNO"].ToString()) ? dtBusRefund.Rows[i]["TICKETNO"].ToString() : "- - -";
                        busRefund.Pnr = !string.IsNullOrEmpty(dtBusRefund.Rows[i]["PNR"].ToString()) ? dtBusRefund.Rows[i]["PNR"].ToString() : "- - -";
                        busRefund.TaNetFare = !string.IsNullOrEmpty(dtBusRefund.Rows[i]["TA_NET_FARE"].ToString()) ? dtBusRefund.Rows[i]["TA_NET_FARE"].ToString() : "- - -";
                        busRefund.BookingStatus = !string.IsNullOrEmpty(dtBusRefund.Rows[i]["BOOKINGSTATUS"].ToString()) ? dtBusRefund.Rows[i]["BOOKINGSTATUS"].ToString() : "- - -";
                        busRefund.JourneyDate = !string.IsNullOrEmpty(dtBusRefund.Rows[i]["JOURNEYDATE"].ToString()) ? dtBusRefund.Rows[i]["JOURNEYDATE"].ToString() : "- - -";
                        busRefund.CreatedDate = !string.IsNullOrEmpty(dtBusRefund.Rows[i]["CREATEDDATE"].ToString()) ? UtilityLib.DateFormateDDMMMYYYY(dtBusRefund.Rows[i]["CREATEDDATE"].ToString()) : "- - -";
                        busRefund.RefundServicechrg = !string.IsNullOrEmpty(dtBusRefund.Rows[i]["REFUND_SERVICECHRG"].ToString()) ? dtBusRefund.Rows[i]["REFUND_SERVICECHRG"].ToString() : "- - -";
                        busRefund.PaymentMode = !string.IsNullOrEmpty(dtBusRefund.Rows[i]["PaymentMode"].ToString()) ? dtBusRefund.Rows[i]["PaymentMode"].ToString() : "- - -";
                        busRefund.CancelCharge = !string.IsNullOrEmpty(dtBusRefund.Rows[i]["CancelCharge"].ToString()) ? dtBusRefund.Rows[i]["CancelCharge"].ToString() : "- - -";
                        busRefund.RefundAmount = !string.IsNullOrEmpty(dtBusRefund.Rows[i]["REFUND_AMT"].ToString()) ? dtBusRefund.Rows[i]["REFUND_AMT"].ToString() : "- - -";
                        busRefund.PublishFare = !string.IsNullOrEmpty(dtBusRefund.Rows[i]["PublishFare"].ToString()) ? dtBusRefund.Rows[i]["PublishFare"].ToString() : "- - -";
                        busRefundList.Add(busRefund);
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return busRefundList;
        }
        public static List<BusReject> GetBusRejectReport(BusFilter filter)
        {
            List<BusReject> busRejectList = new List<BusReject>();
            try
            {
                DataTable dtBusReject = BusDataBase.GetBusRejectReport(filter);
                if (dtBusReject != null && dtBusReject.Rows.Count > 0)
                {
                    for (int i = 0; i < dtBusReject.Rows.Count; i++)
                    {
                        BusReject busReject = new BusReject();
                        busReject.Agency_Name = !string.IsNullOrEmpty(dtBusReject.Rows[i]["Agency_Name"].ToString()) ? dtBusReject.Rows[i]["Agency_Name"].ToString() : "- - -";
                        busReject.EASY_ORDERID_ITZ = !string.IsNullOrEmpty(dtBusReject.Rows[i]["EASY_ORDERID_ITZ"].ToString()) ? dtBusReject.Rows[i]["EASY_ORDERID_ITZ"].ToString() : "- - -";
                        busReject.EASY_TRAN_CODE_ITZ = !string.IsNullOrEmpty(dtBusReject.Rows[i]["EASY_TRAN_CODE_ITZ"].ToString()) ? dtBusReject.Rows[i]["EASY_TRAN_CODE_ITZ"].ToString() : "- - -";
                        busReject.ORDERID = !string.IsNullOrEmpty(dtBusReject.Rows[i]["ORDERID"].ToString()) ? dtBusReject.Rows[i]["ORDERID"].ToString() : "- - -";
                        busReject.TRIPID = !string.IsNullOrEmpty(dtBusReject.Rows[i]["TRIPID"].ToString()) ? dtBusReject.Rows[i]["TRIPID"].ToString() : "- - -";
                        busReject.SEATNO = !string.IsNullOrEmpty(dtBusReject.Rows[i]["SEATNO"].ToString()) ? dtBusReject.Rows[i]["SEATNO"].ToString() : "- - -";
                        busReject.SOURCE = !string.IsNullOrEmpty(dtBusReject.Rows[i]["SOURCE"].ToString()) ? dtBusReject.Rows[i]["SOURCE"].ToString() : "- - -";
                        busReject.DESTINATION = !string.IsNullOrEmpty(dtBusReject.Rows[i]["DESTINATION"].ToString()) ? dtBusReject.Rows[i]["DESTINATION"].ToString() : "- - -";
                        busReject.BOARDINGPOINT = !string.IsNullOrEmpty(dtBusReject.Rows[i]["BOARDINGPOINT"].ToString()) ? dtBusReject.Rows[i]["BOARDINGPOINT"].ToString() : "- - -";
                        busReject.DROPPINGPOINT = !string.IsNullOrEmpty(dtBusReject.Rows[i]["DROPPINGPOINT"].ToString()) ? dtBusReject.Rows[i]["DROPPINGPOINT"].ToString() : "- - -";
                        busReject.LADIESSEAT = !string.IsNullOrEmpty(dtBusReject.Rows[i]["LADIESSEAT"].ToString()) ? dtBusReject.Rows[i]["LADIESSEAT"].ToString() : "- - -";
                        busReject.BUSOPERATOR = !string.IsNullOrEmpty(dtBusReject.Rows[i]["BUSOPERATOR"].ToString()) ? dtBusReject.Rows[i]["BUSOPERATOR"].ToString() : "- - -";
                        busReject.GENDER = !string.IsNullOrEmpty(dtBusReject.Rows[i]["GENDER"].ToString()) ? dtBusReject.Rows[i]["GENDER"].ToString() : "- - -";
                        busReject.PAXNAME = !string.IsNullOrEmpty(dtBusReject.Rows[i]["PAXNAME"].ToString()) ? dtBusReject.Rows[i]["PAXNAME"].ToString() : "- - -";
                        busReject.AGENTID = !string.IsNullOrEmpty(dtBusReject.Rows[i]["AGENTID"].ToString()) ? dtBusReject.Rows[i]["AGENTID"].ToString() : "- - -";
                        busReject.ADMIN_COMM = !string.IsNullOrEmpty(dtBusReject.Rows[i]["ADMIN_COMM"].ToString()) ? dtBusReject.Rows[i]["ADMIN_COMM"].ToString() : "- - -";
                        busReject.ADMIN_MARKUP = !string.IsNullOrEmpty(dtBusReject.Rows[i]["ADMIN_MARKUP"].ToString()) ? dtBusReject.Rows[i]["ADMIN_MARKUP"].ToString() : "- - -";
                        busReject.TA_TDS = !string.IsNullOrEmpty(dtBusReject.Rows[i]["TA_TDS"].ToString()) ? dtBusReject.Rows[i]["TA_TDS"].ToString() : "- - -";
                        busReject.TA_NET_FARE = !string.IsNullOrEmpty(dtBusReject.Rows[i]["TA_NET_FARE"].ToString()) ? dtBusReject.Rows[i]["TA_NET_FARE"].ToString() : "- - -";
                        busReject.TA_TOT_FARE = !string.IsNullOrEmpty(dtBusReject.Rows[i]["TA_TOT_FARE"].ToString()) ? dtBusReject.Rows[i]["TA_TOT_FARE"].ToString() : "- - -";
                        busReject.TOTAL_FARE = !string.IsNullOrEmpty(dtBusReject.Rows[i]["TOTAL_FARE"].ToString()) ? dtBusReject.Rows[i]["TOTAL_FARE"].ToString() : "- - -";
                        busReject.BOOKINGSTATUS = !string.IsNullOrEmpty(dtBusReject.Rows[i]["BOOKINGSTATUS"].ToString()) ? dtBusReject.Rows[i]["BOOKINGSTATUS"].ToString() : "- - -";
                        busReject.PARTIALCANCEL = !string.IsNullOrEmpty(dtBusReject.Rows[i]["PARTIALCANCEL"].ToString()) ? dtBusReject.Rows[i]["PARTIALCANCEL"].ToString() : "- - -";
                        busReject.CREATEDDATE = !string.IsNullOrEmpty(dtBusReject.Rows[i]["CREATEDDATE"].ToString()) ? UtilityLib.DateFormateDDMMMYYYY(dtBusReject.Rows[i]["CREATEDDATE"].ToString()) : "- - -";
                        busReject.PNR = !string.IsNullOrEmpty(dtBusReject.Rows[i]["PNR"].ToString()) ? dtBusReject.Rows[i]["PNR"].ToString() : "- - -";
                        busReject.PROVIDER_NAME = !string.IsNullOrEmpty(dtBusReject.Rows[i]["PROVIDER_NAME"].ToString()) ? dtBusReject.Rows[i]["PROVIDER_NAME"].ToString() : "- - -";
                        busReject.ORIGINAL_FARE = !string.IsNullOrEmpty(dtBusReject.Rows[i]["ORIGINAL_FARE"].ToString()) ? dtBusReject.Rows[i]["ORIGINAL_FARE"].ToString() : "- - -";
                        busReject.PaymentMode = !string.IsNullOrEmpty(dtBusReject.Rows[i]["PaymentMode"].ToString()) ? dtBusReject.Rows[i]["PaymentMode"].ToString() : "- - -";
                        busReject.USERID = !string.IsNullOrEmpty(dtBusReject.Rows[i]["USERID"].ToString()) ? dtBusReject.Rows[i]["USERID"].ToString() : "- - -";
                        busReject.JOURNEYDATE = !string.IsNullOrEmpty(dtBusReject.Rows[i]["JOURNEYDATE"].ToString()) ? dtBusReject.Rows[i]["JOURNEYDATE"].ToString() : "- - -";
                        busRejectList.Add(busReject);
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return busRejectList;
        }
        public static List<BusHoldPnr> GetBusHoldPnrReport(BusFilter filter)
        {
            List<BusHoldPnr> busHoldPnrList = new List<BusHoldPnr>();
            try
            {
                DataTable dtBusReject = BusDataBase.GetBusHoldPnrReport(filter);
                if (dtBusReject != null && dtBusReject.Rows.Count > 0)
                {
                    for (int i = 0; i < dtBusReject.Rows.Count; i++)
                    {
                        BusHoldPnr busHold = new BusHoldPnr();
                        busHold.Agency_Name = !string.IsNullOrEmpty(dtBusReject.Rows[i]["Agency_Name"].ToString()) ? dtBusReject.Rows[i]["Agency_Name"].ToString() : "- - -";
                        busHold.ORDERID = !string.IsNullOrEmpty(dtBusReject.Rows[i]["ORDERID"].ToString()) ? dtBusReject.Rows[i]["ORDERID"].ToString() : "- - -";
                        busHold.TRIPID = !string.IsNullOrEmpty(dtBusReject.Rows[i]["TRIPID"].ToString()) ? dtBusReject.Rows[i]["TRIPID"].ToString() : "- - -";
                        busHold.SEATNO = !string.IsNullOrEmpty(dtBusReject.Rows[i]["SEATNO"].ToString()) ? dtBusReject.Rows[i]["SEATNO"].ToString() : "- - -";
                        busHold.SOURCE = !string.IsNullOrEmpty(dtBusReject.Rows[i]["SOURCE"].ToString()) ? dtBusReject.Rows[i]["SOURCE"].ToString() : "- - -";
                        busHold.DESTINATION = !string.IsNullOrEmpty(dtBusReject.Rows[i]["DESTINATION"].ToString()) ? dtBusReject.Rows[i]["DESTINATION"].ToString() : "- - -";
                        busHold.BOARDINGPOINT = !string.IsNullOrEmpty(dtBusReject.Rows[i]["BOARDINGPOINT"].ToString()) ? dtBusReject.Rows[i]["BOARDINGPOINT"].ToString() : "- - -";
                        busHold.DROPPINGPOINT = !string.IsNullOrEmpty(dtBusReject.Rows[i]["DROPPINGPOINT"].ToString()) ? dtBusReject.Rows[i]["DROPPINGPOINT"].ToString() : "- - -";
                        busHold.LADIESSEAT = !string.IsNullOrEmpty(dtBusReject.Rows[i]["LADIESSEAT"].ToString()) ? dtBusReject.Rows[i]["LADIESSEAT"].ToString() : "- - -";
                        busHold.BUSOPERATOR = !string.IsNullOrEmpty(dtBusReject.Rows[i]["BUSOPERATOR"].ToString()) ? dtBusReject.Rows[i]["BUSOPERATOR"].ToString() : "- - -";
                        busHold.GENDER = !string.IsNullOrEmpty(dtBusReject.Rows[i]["GENDER"].ToString()) ? dtBusReject.Rows[i]["GENDER"].ToString() : "- - -";
                        busHold.PAXNAME = !string.IsNullOrEmpty(dtBusReject.Rows[i]["PAXNAME"].ToString()) ? dtBusReject.Rows[i]["PAXNAME"].ToString() : "- - -";
                        busHold.AGENTID = !string.IsNullOrEmpty(dtBusReject.Rows[i]["AGENTID"].ToString()) ? dtBusReject.Rows[i]["AGENTID"].ToString() : "- - -";
                        busHold.ADMIN_COMM = !string.IsNullOrEmpty(dtBusReject.Rows[i]["ADMIN_COMM"].ToString()) ? dtBusReject.Rows[i]["ADMIN_COMM"].ToString() : "- - -";
                        busHold.ADMIN_MARKUP = !string.IsNullOrEmpty(dtBusReject.Rows[i]["ADMIN_MARKUP"].ToString()) ? dtBusReject.Rows[i]["ADMIN_MARKUP"].ToString() : "- - -";
                        busHold.TA_TDS = !string.IsNullOrEmpty(dtBusReject.Rows[i]["TA_TDS"].ToString()) ? dtBusReject.Rows[i]["TA_TDS"].ToString() : "- - -";
                        busHold.TA_NET_FARE = !string.IsNullOrEmpty(dtBusReject.Rows[i]["TA_NET_FARE"].ToString()) ? dtBusReject.Rows[i]["TA_NET_FARE"].ToString() : "- - -";
                        busHold.TA_TOT_FARE = !string.IsNullOrEmpty(dtBusReject.Rows[i]["TA_TOT_FARE"].ToString()) ? dtBusReject.Rows[i]["TA_TOT_FARE"].ToString() : "- - -";
                        busHold.TOTAL_FARE = !string.IsNullOrEmpty(dtBusReject.Rows[i]["TOTAL_FARE"].ToString()) ? dtBusReject.Rows[i]["TOTAL_FARE"].ToString() : "- - -";
                        busHold.BOOKINGSTATUS = !string.IsNullOrEmpty(dtBusReject.Rows[i]["BOOKINGSTATUS"].ToString()) ? dtBusReject.Rows[i]["BOOKINGSTATUS"].ToString() : "- - -";
                        busHold.PARTIALCANCEL = !string.IsNullOrEmpty(dtBusReject.Rows[i]["PARTIALCANCEL"].ToString()) ? dtBusReject.Rows[i]["PARTIALCANCEL"].ToString() : "- - -";
                        busHold.CREATEDDATE = !string.IsNullOrEmpty(dtBusReject.Rows[i]["CREATEDDATE"].ToString()) ? UtilityLib.DateFormateDDMMMYYYY(dtBusReject.Rows[i]["CREATEDDATE"].ToString()) : "- - -";
                        busHold.PNR = !string.IsNullOrEmpty(dtBusReject.Rows[i]["PNR"].ToString()) ? dtBusReject.Rows[i]["PNR"].ToString() : "- - -";
                        busHold.PROVIDER_NAME = !string.IsNullOrEmpty(dtBusReject.Rows[i]["PROVIDER_NAME"].ToString()) ? dtBusReject.Rows[i]["PROVIDER_NAME"].ToString() : "- - -";
                        busHold.ORIGINAL_FARE = !string.IsNullOrEmpty(dtBusReject.Rows[i]["ORIGINAL_FARE"].ToString()) ? dtBusReject.Rows[i]["ORIGINAL_FARE"].ToString() : "- - -";
                        busHold.PaymentMode = !string.IsNullOrEmpty(dtBusReject.Rows[i]["PaymentMode"].ToString()) ? dtBusReject.Rows[i]["PaymentMode"].ToString() : "- - -";
                        busHold.USERID = !string.IsNullOrEmpty(dtBusReject.Rows[i]["USERID"].ToString()) ? dtBusReject.Rows[i]["USERID"].ToString() : "- - -";
                        busHold.JOURNEYDATE = !string.IsNullOrEmpty(dtBusReject.Rows[i]["JOURNEYDATE"].ToString()) ? dtBusReject.Rows[i]["JOURNEYDATE"].ToString() : "- - -";
                        busHold.TICKETNO = !string.IsNullOrEmpty(dtBusReject.Rows[i]["TICKETNO"].ToString()) ? dtBusReject.Rows[i]["TICKETNO"].ToString() : "- - -";

                        busHoldPnrList.Add(busHold);
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return busHoldPnrList;
        }
        public static List<CommonSelectListItem> ExecutiveList()
        {
            List<CommonSelectListItem> result = new List<CommonSelectListItem>();
            try
            {
                DataTable dtExecutive = BusDataBase.GetStatusExecutiveID();
                if (dtExecutive != null && dtExecutive.Rows.Count > 0)
                {
                    for (int i = 0; i < dtExecutive.Rows.Count; i++)
                    {
                        CommonSelectListItem item = new CommonSelectListItem();
                        item.Text = !string.IsNullOrEmpty(dtExecutive.Rows[i]["ExecutiveID"].ToString()) ? dtExecutive.Rows[i]["ExecutiveID"].ToString() : "- - -";
                        item.Value = !string.IsNullOrEmpty(dtExecutive.Rows[i]["ExecutiveID"].ToString()) ? dtExecutive.Rows[i]["ExecutiveID"].ToString() : "- - -";
                        result.Add(item);
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return result;
        }
        public static List<BusSelectedSeatDetail> GetBusSelectedSeatDetails(string orderid, string agentid, string tinno = "")
        {
            List<BusSelectedSeatDetail> objList = new List<BusSelectedSeatDetail>();
            try
            {
                DataTable dtBusSelected = BusDataBase.GetBusSelectedSeatDetails(orderid, agentid, tinno);
                if (dtBusSelected != null && dtBusSelected.Rows.Count > 0)
                {
                    for (int i = 0; i < dtBusSelected.Rows.Count; i++)
                    {
                        BusSelectedSeatDetail busSelect = new BusSelectedSeatDetail();
                        busSelect.BOOK_RESPONSE = !string.IsNullOrEmpty(dtBusSelected.Rows[i]["BOOK_RESPONSE"].ToString()) ? dtBusSelected.Rows[i]["BOOK_RESPONSE"].ToString() : "- - -";
                        busSelect.source = !string.IsNullOrEmpty(dtBusSelected.Rows[i]["source"].ToString()) ? dtBusSelected.Rows[i]["source"].ToString() : "- - -";
                        busSelect.destination = !string.IsNullOrEmpty(dtBusSelected.Rows[i]["destination"].ToString()) ? dtBusSelected.Rows[i]["destination"].ToString() : "- - -";
                        busSelect.journeydate = !string.IsNullOrEmpty(dtBusSelected.Rows[i]["journeydate"].ToString()) ? dtBusSelected.Rows[i]["journeydate"].ToString() : "- - -";
                        busSelect.SEATNO = !string.IsNullOrEmpty(dtBusSelected.Rows[i]["SEATNO"].ToString()) ? dtBusSelected.Rows[i]["SEATNO"].ToString() : "- - -";
                        busSelect.AGENTID = !string.IsNullOrEmpty(dtBusSelected.Rows[i]["AGENTID"].ToString()) ? dtBusSelected.Rows[i]["AGENTID"].ToString() : "- - -";
                        busSelect.Agency_Name = !string.IsNullOrEmpty(dtBusSelected.Rows[i]["Agency_Name"].ToString()) ? dtBusSelected.Rows[i]["Agency_Name"].ToString() : "- - -";
                        busSelect.Address = !string.IsNullOrEmpty(dtBusSelected.Rows[i]["Address"].ToString()) ? dtBusSelected.Rows[i]["Address"].ToString() : "- - -";
                        busSelect.City = !string.IsNullOrEmpty(dtBusSelected.Rows[i]["City"].ToString()) ? dtBusSelected.Rows[i]["City"].ToString() : "- - -";
                        busSelect.State = !string.IsNullOrEmpty(dtBusSelected.Rows[i]["State"].ToString()) ? dtBusSelected.Rows[i]["State"].ToString() : "- - -";
                        busSelect.Country = !string.IsNullOrEmpty(dtBusSelected.Rows[i]["Country"].ToString()) ? dtBusSelected.Rows[i]["Country"].ToString() : "- - -";
                        busSelect.Phone = !string.IsNullOrEmpty(dtBusSelected.Rows[i]["Phone"].ToString()) ? dtBusSelected.Rows[i]["Phone"].ToString() : "- - -";
                        busSelect.Mobile = !string.IsNullOrEmpty(dtBusSelected.Rows[i]["Mobile"].ToString()) ? dtBusSelected.Rows[i]["Mobile"].ToString() : "- - -";
                        busSelect.FARE = !string.IsNullOrEmpty(dtBusSelected.Rows[i]["FARE"].ToString()) ? dtBusSelected.Rows[i]["FARE"].ToString() : "- - -";
                        busSelect.TA_NET_FARE = !string.IsNullOrEmpty(dtBusSelected.Rows[i]["TA_NET_FARE"].ToString()) ? dtBusSelected.Rows[i]["TA_NET_FARE"].ToString() : "- - -";
                        busSelect.TA_TOT_FARE = !string.IsNullOrEmpty(dtBusSelected.Rows[i]["TA_TOT_FARE"].ToString()) ? dtBusSelected.Rows[i]["TA_TOT_FARE"].ToString() : "- - -";
                        busSelect.PROVIDER_NAME = !string.IsNullOrEmpty(dtBusSelected.Rows[i]["PROVIDER_NAME"].ToString()) ? dtBusSelected.Rows[i]["PROVIDER_NAME"].ToString() : "- - -";
                        busSelect.BOARDINGPOINT = !string.IsNullOrEmpty(dtBusSelected.Rows[i]["BOARDINGPOINT"].ToString()) ? dtBusSelected.Rows[i]["BOARDINGPOINT"].ToString() : "- - -";
                        busSelect.BUSOPERATOR = !string.IsNullOrEmpty(dtBusSelected.Rows[i]["BUSOPERATOR"].ToString()) ? dtBusSelected.Rows[i]["BUSOPERATOR"].ToString() : "- - -";
                        busSelect.PNR = !string.IsNullOrEmpty(dtBusSelected.Rows[i]["PNR"].ToString()) ? dtBusSelected.Rows[i]["PNR"].ToString() : "- - -";
                        busSelect.PAXNAME = !string.IsNullOrEmpty(dtBusSelected.Rows[i]["PAXNAME"].ToString()) ? dtBusSelected.Rows[i]["PAXNAME"].ToString() : "- - -";
                        busSelect.PRIMARY_PAX_PAXMOB = !string.IsNullOrEmpty(dtBusSelected.Rows[i]["PRIMARY_PAX_PAXMOB"].ToString()) ? dtBusSelected.Rows[i]["PRIMARY_PAX_PAXMOB"].ToString() : "- - -";
                        busSelect.CANCEL_POLICY = !string.IsNullOrEmpty(dtBusSelected.Rows[i]["CANCEL_POLICY"].ToString()) ? dtBusSelected.Rows[i]["CANCEL_POLICY"].ToString() : "- - -";
                        busSelect.ISPRIMARY = !string.IsNullOrEmpty(dtBusSelected.Rows[i]["ISPRIMARY"].ToString()) ? dtBusSelected.Rows[i]["ISPRIMARY"].ToString() : "- - -";
                        busSelect.TICKETNO = !string.IsNullOrEmpty(dtBusSelected.Rows[i]["TICKETNO"].ToString()) ? dtBusSelected.Rows[i]["TICKETNO"].ToString() : "- - -";
                        busSelect.NEWTICKETNO = !string.IsNullOrEmpty(dtBusSelected.Rows[i]["NEWTICKETNO"].ToString()) ? dtBusSelected.Rows[i]["NEWTICKETNO"].ToString() : "- - -";
                        busSelect.BUSTYPE = !string.IsNullOrEmpty(dtBusSelected.Rows[i]["BUSTYPE"].ToString()) ? dtBusSelected.Rows[i]["BUSTYPE"].ToString() : "- - -";
                        busSelect.GENDER = !string.IsNullOrEmpty(dtBusSelected.Rows[i]["GENDER"].ToString()) ? dtBusSelected.Rows[i]["GENDER"].ToString() : "- - -";
                        busSelect.PAX_TITLE = !string.IsNullOrEmpty(dtBusSelected.Rows[i]["PAX_TITLE"].ToString()) ? dtBusSelected.Rows[i]["PAX_TITLE"].ToString() : "- - -";
                        busSelect.BOOKINGSTATUS = !string.IsNullOrEmpty(dtBusSelected.Rows[i]["BOOKINGSTATUS"].ToString()) ? dtBusSelected.Rows[i]["BOOKINGSTATUS"].ToString() : "- - -";
                        objList.Add(busSelect);
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return objList;
        }
    }
}
