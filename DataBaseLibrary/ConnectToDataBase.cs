﻿using ModelLibrary;
using System.Data;
using System.Data.SqlClient;

namespace DataBaseLibrary
{
    public static class ConnectToDataBase
    {
        #region [Connection Strings]
        public static SqlConnection MyAmdDBConnection = new SqlConnection(Config.MyAmdDBConnectionString);

        public static SqlConnection ConnectionStrBus = new SqlConnection(Config.ConnectionStrBus);
        #endregion

        #region [Connection Open and Close]
        public static void MyAmdOpenConnection()
        {
            if (MyAmdDBConnection.State == ConnectionState.Closed)
            {
                MyAmdDBConnection.Open();
            }
        }
        public static void BusOpenConnection()
        {
            if (ConnectionStrBus.State == ConnectionState.Closed)
            {
                ConnectionStrBus.Open();
            }
        }
        public static void MyAmdCloseConnection()
        {
            if (MyAmdDBConnection.State == ConnectionState.Open)
            {
                MyAmdDBConnection.Close();
            }
        }
        public static void BusCloseConnection()
        {
            if (ConnectionStrBus.State == ConnectionState.Open)
            {
                ConnectionStrBus.Close();
            }
        }
        #endregion
    }
}
