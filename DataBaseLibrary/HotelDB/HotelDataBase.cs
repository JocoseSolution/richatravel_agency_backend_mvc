﻿using ModelLibrary;
using System;
using System.Data;
using System.Data.SqlClient;

namespace DataBaseLibrary.HotelDB
{
    public static class HotelDataBase
    {
        private static SqlCommand sqlCommand { get; set; }
        private static SqlDataAdapter sqlDataAdapter { get; set; }
        private static DataSet dataSet { get; set; }
        private static DataTable dataTable { get; set; }

        //public static DataTable GetBusReport(HotelFilter filter)
        //{
        //    try
        //    {
        //        ConnectToDataBase.BusOpenConnection();
        //        sqlCommand = new SqlCommand("SP_BUS_REPORT", ConnectToDataBase.ConnectionStrBus);
        //        sqlCommand.CommandType = CommandType.StoredProcedure;
        //        sqlCommand.Parameters.Add(new SqlParameter("@FromDate", (!string.IsNullOrEmpty(filter.FilterFromDate) ? filter.FilterFromDate : "")));
        //        sqlCommand.Parameters.Add(new SqlParameter("@ToDate", (!string.IsNullOrEmpty(filter.FilterToDate) ? filter.FilterToDate : "")));
        //        sqlCommand.Parameters.Add(new SqlParameter("@ORDERID", (!string.IsNullOrEmpty(filter.FilterOrderId) ? filter.FilterOrderId : "")));
        //        sqlCommand.Parameters.Add(new SqlParameter("@SOURCE", (!string.IsNullOrEmpty(filter.FilterSource) ? filter.FilterSource : "")));
        //        sqlCommand.Parameters.Add(new SqlParameter("@DESTINATION", (!string.IsNullOrEmpty(filter.FilterDestination) ? filter.FilterDestination : "")));
        //        sqlCommand.Parameters.Add(new SqlParameter("@BUSOPERATOR", (!string.IsNullOrEmpty(filter.FilterBusOperator) ? filter.FilterBusOperator : "")));
        //        sqlCommand.Parameters.Add(new SqlParameter("@TICKETNO", (!string.IsNullOrEmpty(filter.FilterTicketNo) ? filter.FilterTicketNo : "")));
        //        sqlCommand.Parameters.Add(new SqlParameter("@PNR", (!string.IsNullOrEmpty(filter.FilterPnr) ? filter.FilterPnr : "")));
        //        sqlCommand.Parameters.Add(new SqlParameter("@Status", (!string.IsNullOrEmpty(filter.FilterStatus) ? filter.FilterStatus : "")));
        //        sqlCommand.Parameters.Add(new SqlParameter("@AgentID", (!string.IsNullOrEmpty(filter.FilterAgentId) ? filter.FilterAgentId : "")));
        //        sqlCommand.Parameters.Add(new SqlParameter("@usertype", (!string.IsNullOrEmpty(filter.FilterUserType) ? filter.FilterUserType : "")));
        //        sqlCommand.Parameters.Add(new SqlParameter("@LoginID", (!string.IsNullOrEmpty(filter.FilterLoginId) ? filter.FilterLoginId : "")));
        //        sqlCommand.Parameters.Add(new SqlParameter("@PaymentStatus", (!string.IsNullOrEmpty(filter.FilterPaymentStatus) ? filter.FilterPaymentStatus : "")));

        //        sqlDataAdapter = new SqlDataAdapter();
        //        sqlDataAdapter.SelectCommand = sqlCommand;
        //        dataSet = new DataSet();
        //        sqlDataAdapter.Fill(dataSet, "BusReport");
        //        dataTable = dataSet.Tables["BusReport"];
        //    }
        //    catch (Exception ex)
        //    {
        //        ex.ToString();
        //    }
        //    finally
        //    {
        //        ConnectToDataBase.BusCloseConnection();
        //    }

        //    return dataTable;
        //}
    }
}
