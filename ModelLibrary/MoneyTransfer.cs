﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelLibrary
{
    public class MoneyTransferFilter
    {
        public string FromDate { get; set; }
        public string ToDate { get; set; }
        public string OrderId { get; set; }
        public string TrackId { get; set; }
        public string Status { get; set; }
    }
    public class MoneyTransfer
    {
        public MoneyTransferFilter Filter { get; set; }
    }
}
