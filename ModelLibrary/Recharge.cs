﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelLibrary
{
    public class RechargeFilter
    {
        public string FromDate { get; set; }
        public string ToDate { get; set; }
        public string OrderId { get; set; }
        public string TrackId { get; set; }
        public string TransType { get; set; }
        public string Status { get; set; }
    }
    public class Recharge
    {
        public RechargeFilter Filter { get; set; }
    }
}
