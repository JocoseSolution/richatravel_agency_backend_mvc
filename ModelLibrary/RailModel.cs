﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelLibrary
{
    public class RailModel
    {
        public string FromDate { get; set; }
        public string ToDate { get; set; }
        public string AgencyName { get; set; }
        public string AgencyId { get; set; }
        public string LedgerTrackId { get; set; }
        public string ReservationId { get; set; }
        public string PNR_NO { get; set; }
        public string Class { get; set; }
        public string ReferenceNo { get; set; }
        public string LedDebit { get; set; }
        public string BookingStatus { get; set; }
        public string BookingDate { get; set; }
        public int Totalcount { get; set; }
        public List<RailModel> RailReportList { get; set; }
    }
    public class RailModelFilter
    {
        public string FromDate { get; set; }
        public string ToDate { get; set; }
        public string UserType { get; set; }
        public string Loginid { get; set; }
       
    }
}

