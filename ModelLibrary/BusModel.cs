﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace ModelLibrary
{
    public class BusFilter
    {
        public string AgencyName { get; set; }
        public string AgentId { get; set; }
        public string FilterFromDate { get; set; }
        public string FilterToDate { get; set; }
        public string FilterOrderId { get; set; }
        public string FilterSource { get; set; }
        public string FilterDestination { get; set; }
        public string FilterBusOperator { get; set; }
        public string FilterTicketNo { get; set; }
        public string FilterPnr { get; set; }
        public string FilterStatus { get; set; }
        public string FilterAgentId { get; set; }
        public string FilterUserType { get; set; } = "AGENT";
        public string FilterLoginId { get; set; }
        public string FilterPaymentStatus { get; set; }
        public string FilterProviderName { get; set; }
        public string FilterExecid { get; set; }
    }
    public class BusModel
    {
        public string AgentId { get; set; }
        public string Agency_Name { get; set; }
        public string Agent_Type { get; set; }
        public string ORDERID { get; set; }
        public string TRIPID { get; set; }
        public string SOURCE { get; set; }
        public string DESTINATION { get; set; }
        public string Sector { get; set; }
        public string BUSOPERATOR { get; set; }
        public string SEATNO { get; set; }
        public string LADIESSEAT { get; set; }
        public string FARE { get; set; }
        public string TA_NET_FARE { get; set; }
        public string BOARDINGPOINT { get; set; }
        public string DROPPINGPOINT { get; set; }
        public string PAXNAME { get; set; }
        public string GENDER { get; set; }
        public string PRIMARY_PAX_IDTYPE { get; set; }
        public string PRIMARY_PAX_IDNUMBER { get; set; }
        public string PRIMARY_PAX_PAXMOB { get; set; }
        public string PRIMARY_PAX_PAXEMAIL { get; set; }
        public string BOOKINGSTATUS { get; set; }
        public string TICKETNO { get; set; }
        public string PNR { get; set; }
        public string CANCELLATIONCHARGE { get; set; }
        public string REFUNDEDAMOUNT { get; set; }
        public string PARTIALCANCEL { get; set; }
        public string JOURNEYDATE { get; set; }
        public string CREATEDDATE { get; set; }
        public string TA_TOT_FARE { get; set; }
        public string ADMIN_COMM { get; set; }
        public string TA_TDS { get; set; }
        public string YATRA_PNR { get; set; }
        public string PROVIDER_NAME { get; set; }
        public string PaymentMode { get; set; }
        public int TotalCount { get; set; }
        public List<BusModel> BusList { get; set; }
        public BusFilter Filter { get; set; }
    }
    public class BusRefund
    {
        public string OrderId { get; set; }
        public string TripId { get; set; }
        public string Source { get; set; }
        public string Destination { get; set; }
        public string BusOperator { get; set; }
        public string SeatNo { get; set; }
        public string PaxName { get; set; }
        public string Gender { get; set; }
        public string TicketNo { get; set; }
        public string Pnr { get; set; }
        public string TaNetFare { get; set; }
        public string BookingStatus { get; set; }
        public string JourneyDate { get; set; }
        public string CreatedDate { get; set; }
        public string RefundServicechrg { get; set; }
        public string PaymentMode { get; set; }
        public string CancelCharge { get; set; }
        public string RefundAmount { get; set; }
        public string PublishFare { get; set; }
        public List<BusRefund> BusRefundList { get; set; }
        public BusFilter Filter { get; set; }
        public int TotalCount { get; set; }
    }
    public class BusReject
    {
        public string Agency_Name { get; set; }
        public string EASY_ORDERID_ITZ { get; set; }
        public string EASY_TRAN_CODE_ITZ { get; set; }
        public string ORDERID { get; set; }
        public string TRIPID { get; set; }
        public string SEATNO { get; set; }
        public string SOURCE { get; set; }
        public string DESTINATION { get; set; }
        public string BOARDINGPOINT { get; set; }
        public string DROPPINGPOINT { get; set; }
        public string LADIESSEAT { get; set; }
        public string BUSOPERATOR { get; set; }
        public string GENDER { get; set; }
        public string PAXNAME { get; set; }
        public string AGENTID { get; set; }
        public string ADMIN_COMM { get; set; }
        public string ADMIN_MARKUP { get; set; }
        public string TA_TDS { get; set; }
        public string TA_NET_FARE { get; set; }
        public string TA_TOT_FARE { get; set; }
        public string TOTAL_FARE { get; set; }
        public string BOOKINGSTATUS { get; set; }
        public string PARTIALCANCEL { get; set; }
        public string CREATEDDATE { get; set; }
        public string JOURNEYDATE { get; set; }
        public string PNR { get; set; }
        public string PROVIDER_NAME { get; set; }
        public string ORIGINAL_FARE { get; set; }
        public string PaymentMode { get; set; }
        public string USERID { get; set; }
        public List<BusReject> BusRejectList { get; set; }
        public BusFilter Filter { get; set; }
        public int TotalCount { get; set; }
    }
    public class BusHoldPnr
    {
        public string Agency_Name { get; set; }

        public string ORDERID { get; set; }
        public string TRIPID { get; set; }
        public string SEATNO { get; set; }
        public string SOURCE { get; set; }
        public string DESTINATION { get; set; }
        public string BOARDINGPOINT { get; set; }
        public string DROPPINGPOINT { get; set; }
        public string LADIESSEAT { get; set; }
        public string BUSOPERATOR { get; set; }
        public string GENDER { get; set; }
        public string PAXNAME { get; set; }
        public string AGENTID { get; set; }
        public string ADMIN_COMM { get; set; }
        public string ADMIN_MARKUP { get; set; }
        public string TA_TDS { get; set; }
        public string TA_NET_FARE { get; set; }
        public string TA_TOT_FARE { get; set; }
        public string TOTAL_FARE { get; set; }
        public string BOOKINGSTATUS { get; set; }
        public string PARTIALCANCEL { get; set; }
        public string CREATEDDATE { get; set; }
        public string JOURNEYDATE { get; set; }
        public string PNR { get; set; }
        public string PROVIDER_NAME { get; set; }
        public string ORIGINAL_FARE { get; set; }
        public string PaymentMode { get; set; }
        public string TICKETNO { get; set; }
        public string USERID { get; set; }
        public List<BusHoldPnr> BusHoldPnrList { get; set; }
        public BusFilter Filter { get; set; }
        public int TotalCount { get; set; }
        public List<SelectListItem> ExecutiveList { get; set; }
    }
    public class BusSelectedSeatDetail
    {
        public string BOOK_RESPONSE { get; set; }
        public string source { get; set; }
        public string destination { get; set; }
        public string journeydate { get; set; }
        public string SEATNO { get; set; }
        public string AGENTID { get; set; }
        public string Agency_Name { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Country { get; set; }
        public string Phone { get; set; }
        public string Mobile { get; set; }
        public string FARE { get; set; }
        public string TA_NET_FARE { get; set; }
        public string TA_TOT_FARE { get; set; }
        public string PROVIDER_NAME { get; set; }
        public string BOARDINGPOINT { get; set; }
        public string BUSOPERATOR { get; set; }
        public string PNR { get; set; }
        public string PAXNAME { get; set; }
        public string PRIMARY_PAX_PAXMOB { get; set; }
        public string CANCEL_POLICY { get; set; }
        public string ISPRIMARY { get; set; }
        public string TICKETNO { get; set; }
        public string NEWTICKETNO { get; set; }
        public string GENDER { get; set; }
        public string BUSTYPE { get; set; }
        public string PAX_TITLE { get; set; }
        public string BOOKINGSTATUS { get; set; }
    }

    #region [Single Bus Summary]
    // Root myDeserializedClass = JsonConvert.DeserializeObject<Root>(myJsonResponse); 
    public class CustomerPriceBreakUp
    {
        public string cancellationHandling { get; set; }
        public string componentName { get; set; }
        public string refundableValue { get; set; }
        public string type { get; set; }
        public string value { get; set; }
    }

    public class FareBreakup
    {
        public List<CustomerPriceBreakUp> customerPriceBreakUp { get; set; }
        public string seatName { get; set; }
    }

    public class Passenger
    {
        public string address { get; set; }
        public string age { get; set; }
        public string email { get; set; }
        public string gender { get; set; }
        public string idNumber { get; set; }
        public string idType { get; set; }
        public string mobile { get; set; }
        public string name { get; set; }
        public string primary { get; set; }
        public string singleLadies { get; set; }
        public string title { get; set; }
    }

    public class InventoryItems
    {
        public string fare { get; set; }
        public FareBreakup fareBreakup { get; set; }
        public string ladiesSeat { get; set; }
        public string malesSeat { get; set; }
        public string operatorServiceCharge { get; set; }
        public Passenger passenger { get; set; }
        public string seatName { get; set; }
        public string serviceTax { get; set; }
    }

    public class ReschedulingPolicy
    {
        public string reschedulingCharge { get; set; }
        public string windowTime { get; set; }
    }

    public class BusSingleRoot
    {
        public string bookingFee { get; set; }
        public string busType { get; set; }
        public string cancellationCalculationTimestamp { get; set; }
        public string cancellationMessage { get; set; }
        public string cancellationPolicy { get; set; }
        public DateTime dateOfIssue { get; set; }
        public string destinationCity { get; set; }
        public string destinationCityId { get; set; }
        public DateTime doj { get; set; }
        public string dropLocation { get; set; }
        public string dropLocationId { get; set; }
        public string dropTime { get; set; }
        public string FIRST_BOARDING_POINT_TIMESTAMP { get; set; }
        public string firstBoardingPointTime { get; set; }
        public string hasRTCBreakup { get; set; }
        public string hasSpecialTemplate { get; set; }
        public string inventoryId { get; set; }
        public InventoryItems inventoryItems { get; set; }
        public string MTicketEnabled { get; set; }
        public string partialCancellationAllowed { get; set; }
        public string pickUpContactNo { get; set; }
        public string pickUpLocationAddress { get; set; }
        public string pickupLocation { get; set; }
        public string pickupLocationId { get; set; }
        public string pickupLocationLandmark { get; set; }
        public string pickupTime { get; set; }
        public string pnr { get; set; }
        public string primeDepartureTime { get; set; }
        public string primoBooking { get; set; }
        public ReschedulingPolicy reschedulingPolicy { get; set; }
        public string serviceCharge { get; set; }
        public string sourceCity { get; set; }
        public string sourceCityId { get; set; }
        public string status { get; set; }
        public string tin { get; set; }
        public string travels { get; set; }
        public string tripCode { get; set; }
        public string vaccinatedBus { get; set; }
        public string vaccinatedStaff { get; set; }
        public string vendorPnr { get; set; }
    }


    #endregion
}
